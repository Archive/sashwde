Building SashXB Weblication Development Enviroment for Linux
=========================

Contents:

* Setting up for compilation
* Building the SashXB WDE on Linux
* Installing the SashXB WDE



Setting up for compilation
==========================
The following components are required for building the SashXB WDE:
    -Mozilla (tested with build 0809, should work with >= M18)     
     http://www.mozilla.org
    -Xerces  (tested with 1.2.0, should work with >= 1.2.0)  
     http://xml.apache.org
    -GTK, GNOME
    -UUID library from Theodore Ts'o, included with the ext2 filesystem
    -The SashXB source distribution (availble on this server)
    -GtkHTML (tested with 0.5, should work, but might have conflicts 
	with >= 0.5 due to GtkHTML's incomplete C++ support.)
    -All the packages GtkHTML requires, including (but not limited to):
          - gnome-print - 0.20
          - gdk-pixbuf - 0.7.0 or later

GNU tools such as Autoconf, libtool, and GNU Make are required to
build the SashXB WDE. These tools are available from the Free Software
Foundation.

You must first set your environment variables identifying the paths of
SashXB, SashXB WDE, and Xerces. Set your paths as follows:

	export XERCESCROOT=<full path to Xerces's C src>
	export MOZILLA=<full path to Mozilla toplevel source directory>
	export SASHXB=<full path to SashXB's src>
	export MOZILLA_FIVE_HOME=<full path to where the mozilla libraries 
			live, usually something like $HOME/mozilla/dist/bin>

Building does not require the Mozilla sources, and can be done with a
binary package, but you must set up symlinks properly. (TODO: document
how to do this)

Building the SashXB WDE on Linux
========================
Run:
	./autogen.sh --prefix=<desired install location> --with-mozilla-libs=<path to mozilla library directory> --with-mozilla-headers=<path to mozilla includes>
	make

These commands should configure the build and compile the source tree.

Instead of setting the SashMo and Xerces enviroment variables, you can
specify them on the autogen/configure commandline using:

--with-sashmo-dir=<top SashXB directory, i.e. SASHXB>
--with-xerces-dir=<top Xerces directory, i.e. XERCESCROOT>

The usual desired install location for --prefix is $HOME/.sash, as
that is where SashXB is usually installed. However, you can install
the SashXB WDE in any other directory (or system-wide) as well.

Installing the SashXB WDE
=================

Simply type from your SashXB WDE directory:

make install

The appropriate binaries will be placed in your specified --prefix,
and a link to the SashWDE installed under Internet applications on
your GNOME menus.

