All about the SashWDE (Weblication Development Environment)
Jordi Albornoz
September 2000

For information on compiling the SashWDE or SashXB 
(the runtime for weblications) see the appropriate README.make files.
SashXB is in the SashMo directory in the Gnome CVS server.

This file gives an overview of the structure of the WDE.
It would useful to read the SashMo/README.overview file
before reading this one.  It will define some terms in more detail.

What is it?
The WDE is a GUI application that makes use of the GTK+ and
Gnome libraries.  It is basically a visual integrated development
enviroment that is geared towards writing HTML files that use
Sashscript (the superset of Javascript implemented by SashXB).
Aside from being an editor and managing projects as all IDEs do, the SashWDE
is made to "Publish" weblications.  This is the closest thing to compiling
a weblication and amounts to creating its WDF file (Weblication Definition
Format).

What is a WDF file?
A weblication is completely specified by a WDF file and its HTML/Javascript
source files.  The WDF file is an XML file that decribes aspects of the
weblication such as who wrote it, what runtime components it needs, where
to fetch its source and data files, and what security level the weblication
requests.  When a weblication is installed, the installer basically reads
this file and uses it to do the right thing.

Structure of the WDE:
The WDF is written in C++ and takes advantage of libglade for much of its
user interface (we had to do some magic to tie signals to C++ functions).
It is seperated into a few components that communicate using "events".
The WDE is designed with a modified Document/View architecture.

The data or document in the WDE is encompassed by the WeblicationProject
class.  This class contains as members a WDFDocument class a FilesManager
class and a Registry class.  These classes together encompass the data that
the user is working upon.  The rest of the WDE deals simply with the user
interface.

The WeblicationProject and its related classes export functions that allow
the data they contain to be read and modified.  Whenever the data is modified
by any part of the program an "event" is trigerred.  To listen to events
a class simply needs to derive from the EventHandler class and implement the
pure virtual function HandleEvent().  Then one can register as a listener with
the WeblicationProject::AddListener() function.  The HandleEvent function
ussually consists of a switch statement on the different events that the
listener might care about.

Events are defined in the events.h file.  They are simply integer constants.
Notice the funky RegisterEvent() macros that are used to define events.  This
is one of the magic tricks we use to take advantage of glade/libglade for the
user interface.  Using this macro, gmodule, and other stuff, we are able to
link gtk signals directly to events using glade.  This is mostly used in the
menus in the main WDE window.  Use glade to open SashWDE/glade/MDImanager.glade
to see examples of this.

The WDE event system has 4 types of events:
Proj - These events are triggered by changes in data and are meant to tell views
    of this data to update.  This is how data components communicate.
State - These events are sent to let the views know about some UI action that
    the user has done such as bring an editor to the foreground or request that
    the actions pane be shown.  This is how UI components communicate.
WDE - These are utility events that are used to prevent code duplication.  They
    are handled by a class called the WDEProjectManager.  Ex: WDE_NewProject
Editor- These events are like state events but they have the special
    characteristic that they are only sent to a specific editor window, not
    broadcast like the above event types.

Why do this?!
1. To seperate data from UI basically using the document/view model but with
more detail than a single UpdateAllViews() call as is common.

2. Biggest reason is to allow customizability to be possible using sash script.
The WDE will be a "location" in sash and will export to sashscript an API that
will allow authors to customise the IDE using Sash itself.  This API will map
very closely to these events we have designed and make this customizability
somewhat as simply as having the sashscript interpreter send these events
whenever the apporopriate sashscript function is called.

3. We wanted to get away from the "everything has a pointer to everything
else" problem that happens with UI programming in C++ often.  Notice that
the UI components basically have a pointer to the WeblicationProject class
and the WDEProjectManager class and ussually nothing more.  If something occurs
that might be of interest to someone else, a state event is sent.  If something
is commonly used by many aspects of the interface such as code to open files
or create new projects, then it is written once and triggered using a WDE
event.

Request/Style Guideline: Use the userdata field in events as little as possible.

Scintilla:
This is a wonderful cross platform source editor component that we have
chosen to use for the editor in the WDE.  Check out www.scintilla.org.

Some stuff left to be done:
- Enable more of scintilla's features including syntax completion.
- Make a Project Files pane
- WDF generation (publish) code is incomplete.
- Add archive creation capabilities (.jar files)
- Add customizability features (implement a SashXB location for the WDE)
