#!/usr/bin/env python
import re
import sys

reg = re.compile(r'(.{8})-(.{4})-(.{4})-(.{2})(.{2})-' + r'(.{2})' * 6)

def conv(uuid):
    m = reg.match(uuid)
    if not m:
        print "Didn't match regular expression"
        sys.exit(1)
    res = '{0x%s, 0x%s, 0x%s, {0x%s, 0x%s, 0x%s, 0x%s, 0x%s, 0x%s, 0x%s, 0x%s}}' % m.groups()
    print res

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Usage: %s <uuid>" % (sys.argv[0])
        sys.exit(1)
    conv(sys.argv[1])
