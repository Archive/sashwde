dnl - Determine where and which version of mozilla is installed on the system
dnl - Author: Andrew Chatham

AC_DEFUN([CHECK_MOZILLA_PATH],[
	has_mozilla=false

	CFLAGS=${CFLAGS--O}
	AC_ARG_WITH(mozilla-dist,
	  [  --with-mozilla-dist=dir      Specify the Mozilla dist directory],[
	    #we have a source disribution
	    MOZILLA_INCLUDE_DIR="$withval/include"
	    MOZILLA_LIB_DIR="$withval/lib"
	    MOZILLA_BIN_DIR="$withval/bin"
	    MOZILLA_IDL_DIR="$withval/idl"
	], [
	    #we're using whatever comes from the rpms
	    MOZILLA_INCLUDE_DIR="/usr/include/mozilla"
	    MOZILLA_LIB_DIR="/usr/lib"
	    MOZILLA_BIN_DIR="/usr/bin"
	    MOZILLA_IDL_DIR="/usr/include/mozilla/idl"
	    if ! test -f $MOZILLA_IDL_DIR/nsISupports.idl; then
		MOZILLA_IDL_DIR="/usr/share/idl/mozilla"
	    fi
            if ! test -f $MOZILLA_IDL_DIR/nsISupports.idl; then
		MOZILLA_IDL_DIR="/usr/include/mozilla/idl/"
	    fi
	])

	AC_ARG_WITH(mozilla-include,
	 [  --with-mozilla-include=dir The mozilla include directory],[
	 if test x$withval != x; then
	    MOZILLA_INCLUDE_DIR="$withval"
	 fi
	])

	AC_ARG_WITH(mozilla-lib,
	 [  --with-mozilla-lib=dir     The mozilla library directory],[
	 if test x$withval != x; then
	    MOZILLA_LIB_DIR="$withval"
	 fi
	])

	AC_ARG_WITH(mozilla-idl,
	 [  --with-mozilla-idl=dir     The mozilla idl directory],[
	 if test x$withval != x; then
	    MOZILLA_IDL_DIR="$withval"
	 fi
	])

	AC_MSG_CHECKING(for mozilla)

	if ! test -f $MOZILLA_INCLUDE_DIR/nsBuildID.h; then
		AC_MSG_ERROR("Could not find header nsBuildID.h in includes dir $MOZILLA_INCLUDE_DIR")
	fi

	if ! test -f $MOZILLA_LIB_DIR/libgtkembedmoz.so; then
		AC_MSG_ERROR("Could not find library libgtkembedmoz.so in lib dir $MOZILLA_LIB_DIR")
	fi

	if ! test -f $MOZILLA_IDL_DIR/nsISupports.idl; then
		AC_MSG_ERROR("Could not find nsISupports.idl in idl dir $MOZILLA_IDL_DIR")
	fi

	if test -f $MOZILLA_LIB_DIR/defaults/pref/all.js; then
		ALLJSLOC=$MOZILLA_LIB_DIR/defaults/pref/all.js
	elif test -f $MOZILLA_LIB_DIR/mozilla/defaults/pref/all.js; then
		ALLJSLOC=$MOZILLA_LIB_DIR/mozilla/defaults/pref/all.js
	elif test -f $MOZILLA_BIN_DIR/defaults/pref/all.js; then
		ALLJSLOC=$MOZILLA_BIN_DIR/defaults/pref/all.js
	elif test -f `echo $MOZILLA_LIB_DIR/mozilla-*/defaults/pref/all.js`; then
		ALLJSLOC=`echo $MOZILLA_LIB_DIR/mozilla-*/defaults/pref/all.js`
	else
		ALLJSLOC=$MOZILLA_FIVE_HOME/defaults/pref/all.js
	fi

	MOZILLA_UTIL_BIN_DIR=""
	
	if test -f $MOZILLA_LIB_DIR/xpidl; then
		MOZILLA_UTIL_BIN_DIR=$MOZILLA_LIB_DIR
	elif test -f $MOZILLA_LIB_DIR/mozilla/xpidl; then
		MOZILLA_UTIL_BIN_DIR=$MOZILLA_LIB_DIR/mozilla
	elif test -f $MOZILLA_BIN_DIR/xpidl; then
		MOZILLA_UTIL_BIN_DIR=$MOZILLA_BIN_DIR
	elif test -f $MOZILLA_BIN_DIR/mozilla/xpidl; then
		MOZILLA_UTIL_BIN_DIR=$MOZILLA_BIN_DIR/mozilla
	else
		AC_MSG_ERROR("Could not find xpidl and the xpt utilities in $MOZILLA_LIB_DIR or $MOZILLA_BIN_DIR");
	fi

	# nasty stuff to check turn the Mozilla version into a number.
	# no doubt this won't work for some future Mozilla versions,
	# because I am _that_ talented - Andrew

	# a version 1.2.3 turns into 10203
	# removed bc requirement and changed so it would handly nightly versions -- AJ

	VERSION_LINE=$(grep useragent.misc $ALLJSLOC)
	MOZILLA_VER=`echo $VERSION_LINE | perl -n -e '/(\d+)\.(\d+)\.(\d+)/; print (\${1}*10000 + \${2}	*100 + \${3});' `
	if test "x$MOZILLA_VER" == "x"; then 
		AC_MSG_ERROR("Could not determine mozilla version")
	fi

	if test "$MOZILLA_VER" -lt "907"; then
		AC_MSG_ERROR("Mozilla version must be at least 0.9.5")
	else
		AC_MSG_RESULT(found)
	fi

	AC_MSG_CHECKING($MOZILLA_UTIL_BIN_DIR)

	AC_SUBST(MOZILLA_BIN_DIR)
	AC_SUBST(MOZILLA_LIB_DIR)
	AC_SUBST(MOZILLA_INCLUDE_DIR)
	AC_SUBST(MOZILLA_IDL_DIR)
	AC_SUBST(MOZILLA_VER)
	AC_SUBST(MOZILLA_UTIL_BIN_DIR)
])


