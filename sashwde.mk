#include ./$(DEPTH)/config/autoconf.mk

##### standard includes. anything used by all Makefile.am's should be here

SASHINCLUDES = \
	-Wall \
	-DJS_THREADSAFE \
	-DG_LOG_DOMAIN=\"$(PACKAGE)\" \
	-DGNOMELOCALEDIR=\""$(datadir)/locale"\" \
	-DDEBUG \
	-DTRACING \
	-DOSTYPE=\"$(uname -s)$(uname -r)\" \
	-DOJI \
	-D_DEBUG \
	-DXP_UNIX \
	-DXP_UNIX \
	-DMOZILLA_CLIENT \
	-DIBMBIDI \
	-fexceptions \
	-fno-rtti \
	-fPIC \
	-I$(top_srcdir) \
	-I$(top_srcdir)/include \
	-I$(top_builddir)/include \
	$(GNOME_INCLUDEDIR) \
	-I$(top_srcdir)/intl \
	-I$(MOZILLA_INCLUDE_DIR)/string \
	-I$(MOZILLA_INCLUDE_DIR)/browser \
	-I$(MOZILLA_INCLUDE_DIR)/js \
	-I$(MOZILLA_INCLUDE_DIR)/necko \
	-I$(MOZILLA_INCLUDE_DIR)/xpcom \
	-I$(MOZILLA_INCLUDE_DIR)/layout \
	-I$(MOZILLA_INCLUDE_DIR)/xpconnect \
	-I$(MOZILLA_INCLUDE_DIR)/embed_base \
	-I$(MOZILLA_INCLUDE_DIR)/widget \
	-I$(MOZILLA_INCLUDE_DIR)/dom \
	-I$(MOZILLA_INCLUDE_DIR)/gfx \
	-I$(MOZILLA_INCLUDE_DIR)/nspr \
	-I$(MOZILLA_INCLUDE_DIR)/gtkembedmoz \
	-I$(MOZILLA_INCLUDE_DIR)/docshell \
	-I$(MOZILLA_INCLUDE_DIR)/pref \
	-I$(MOZILLA_INCLUDE_DIR)/caps \
	-I$(MOZILLA_INCLUDE_DIR)/webbrwsr \
	-I$(MOZILLA_INCLUDE_DIR)/content \
	-I/usr/include/nspr \
	-I$(SASHXB_INCLUDE_DIR) \
	-I$(top_srcdir)/mozinclude \
	-I/usr/include/orbit-1.0 \
	-D_REENTRANT  # for gthreads

#	-I$(top_srcdir)/tools 
#	-I$(top_srcdir)/wdf 
#	-I$(top_srcdir)/extensiontools 
#	-I$(top_srcdir)/net 
#	-I$(top_srcdir)/runtime 
#	-I$(top_srcdir)/instmanager 
#	-I$(top_srcdir)/datamanager 
#	-I$(top_srcdir)/filesystem 
#	-I$(top_srcdir)/ezxml 
#	-I$(top_srcdir)/registry 
#	-I$(top_srcdir)/protocolhandler 
#	-I$(top_srcdir)/cachemanager 
#	-I$(top_srcdir)/security 

top_srcdirLDADD = \
	-L$(SASHXB_LIB_DIR)

WDEINCLUDES = \
	$(SASHINCLUDES) \
	-I$(top_builddir)/src/idl \
	-I$(top_srcdir)/src/gui \
	-I$(top_srcdir)/src/app \
	-I$(top_srcdir)/src/wde \
	-I$(top_srcdir)/src/project \
	-I$(top_srcdir)/src/event \
	-I$(top_srcdir)/src/extensions/ExtensionDKLocation \
	-I$(top_srcdir)/src/extensions/LocationDKLocation \
	-I$(top_srcdir)/src/extensions/WDEAddinLocation \
	-I$(top_builddir)/src/extensions/ExtensionDKLocation \
	-I$(top_builddir)/src/extensions/LocationDKLocation \
	-I$(top_builddir)/src/extensions/WDEAddinLocation \
	-I$(top_srcdir)/scintilla/include -DGTK 

MOZILLA_LIBS = \
	-L$(MOZILLA_LIB_DIR) \
	-L$(MOZILLA_LIB_DIR)/components \
	-lgkgfx \
	-lgtkembedmoz \
	-lgtksuperwin \
	-lgtkxtbin \
	-ljsj \
	-lmozjs \
	-lnspr4 \
	-lplc4 \
	-lplds4 \
	-lxpcom \
	-lxpistub

##### lib macros -- you should only have to change library locations *here*

SASHTOOLSLDFLAGS = \
	-L$(top_builddir)/tools \
	-lsashtools

SASHSECURITYLDFLAGS = \
	-L$(top_builddir)/security \
	-lsashsecurity

SASHNETLDFLAGS = \
	-L$(top_builddir)/net \
	-lsashnet

SASHLDFLAGS = \
	-L$(top_builddir)/sash \
	-lsash

FILESYSTEMLDFLAGS = \
	-L$(top_builddir)/filesystem \
	-lfilesystem

INSTMANAGERLDFLAGS = \
	-L$(top_builddir)/instmanager \
	-linstmanager \
	-L/usr/lib/nspr \
	-L$(MOZILLA_LIB_DIR) \
	-lnspr4

CACHEMANAGERLDFLAGS = \
	-L$(top_builddir)/cachemanager \
	-lcachemanager

DATAMANAGERLDFLAGS = \
	-L$(top_builddir)/datamanager \
	-ldatamanager

SASHWDFLDFLAGS = \
	-L$(top_builddir)/wdf \
	-lsashwdf

EXTENSIONTOOLSLDFLAGS = \
	-L$(top_builddir)/extensiontools \
	-lsashextensiontools

EZXMLLDFLAGS = \
	-L$(top_builddir)/ezxml \
	-lezxml

SASHRUNTIMETOOLS = \
	-L$(top_builddir)/runtime \
	-lruntimetools
	
SASHACTIONRUNTIME = \
	-L$(top_builddir)/runtime \
	-lsashactionruntime \
	-lsashruntime

######### XPIDL macros. used by extensions, or anything that uses XPCOM ####

IDL = $(MOZILLA_UTIL_BIN_DIR)/xpidl
XPTLINK = $(MOZILLA_UTIL_BIN_DIR)/xpt_link

# you can replace IDLFLAGS or redefine it below if you need
# more idl include dirs.
IDLFLAGS = \
	-w \
	-I $(MOZILLA_IDL_DIR) \
	-I $(SASHXB_IDL_DIR) \
	-I $(top_srcdir)/src/eventable \
	-I $(top_srcdir)/src/project\
	-I $(top_srcdir)/src/wde \
	-I $(top_srcdir)/src/idl \
	-I $(top_srcdir)/src/extensions/LocationDKLocation \
	-I $(top_srcdir)/src/extensions/WDEAddinLocation \
	-I $(top_srcdir)/src/extensions/ExtensionDKLocation \
	-I .

%.h: %.idl
	$(IDL) $(IDLFLAGS) -m header -o `basename $@ .h` $<

%.xpt: %.idl
	$(IDL) $(IDLFLAGS) -m typelib -o `basename $@ .xpt` $<

%.html: %.idl
	$(IDL) $(IDLFLAGS) -m doc -o `basename $@ .html` $<

PYTHON = `which python`

componentsdir = $(pkglibdir)/components

idldir = $(datadir)/idl/$(PACKAGE)

includedir = $(prefix)/include
pkgincludedir = $(includedir)/$(PACKAGE)

IDL_FILES =

skeldir = $(pkgdatadir)/sash-skel

defaultextsdir = $(pkgdatadir)/default-exts

############### WDE stuff ##################

WDEWDF_LIBS = \
	$(SASHWDFLDFLAGS) \
	-L$(top_builddir)/wdf/ -lwdfactioncollectionmod -lwdfactionmod -lwdfdocumentmod

WDEPROJECT_LIBS = \
	-L$(top_builddir)/src/project -lwdeproject

WDEWDE_LIBS = \
	-L$(top_builddir)/src/wde -lwdewde -luuid

WDEAPP_LIBS = \
	-L$(top_builddir)/src/app -lwdeapp

WDEGUI_LIBS = \
	-L$(top_builddir)/src/gui -lwdegui \
	-L$(top_builddir)/src/event -lwdeevent

SASHWDELDADD = \
	-L$(SASHXB_LIB_DIR)

WDE_GLADE_DIR = "$(pkgdatadir)/glade"
WDE_PIXMAPS_DIR = "$(datadir)/pixmaps/$(PACKAGE)"
#GENERATED_TYPELIBS = $(IDL_FILES:.idl=.xpt)
