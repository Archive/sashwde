/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/

// WDEEditorPane.cpp
// Editor pane class for the Sash WDE. Encapsulates event and gtk callback
// functionality.
// Written by kag, 6/29/00

#include "wdeconstants.h"
#include "WDEEditorPane.h"
#include "WDEProjectManager.h"
#include "WDEglobal.h"
#include <gnome.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "Platform.h"
#include "Scintilla.h"
#include "ScintillaWidget.h"
#include "SciLexer.h"
#include "PropSet.h"
#include "WDEProps.h"
//#include "prenv.h"


#include "gtkmozembed.h"

#include "events.h"
#include "sash_error.h"
#include <strstream>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

#include <sys/types.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
//#include <libgnomeprint/gnome-print.h>
#include <libgnome/gnome-defs.h>
#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <regex.h>
#include <string.h>




// Local functions
static int IntFromHexDigit(const char ch);
static Colour ColourFromString(const char *val);


WDEEditorPane::WDEEditorPane(WDEProjectManager *manager, string filename,
			     string title) : m_ProjManager(manager)
{
  // Create GenericMDIChild and setup callbacks properly. Initialize any
  // other editor-specific data.

  m_Filename = filename;

  m_EditorChild = GTK_OBJECT(gnome_mdi_generic_child_new(title.c_str()));
  gtk_object_set_user_data(m_EditorChild, this);

  gnome_mdi_generic_child_set_view_creator(GNOME_MDI_GENERIC_CHILD(m_EditorChild),
					   WDEEditorPane_ExternCreateView,
					   this);
  gnome_mdi_generic_child_set_label_func(GNOME_MDI_GENERIC_CHILD(m_EditorChild),
					 WDEEditorPane_ExternLabelFunc,
					 this);
  m_JustBorn = false;
  

  m_Props.superPS = &m_PropsBase;
  
  // Note : very, very easy to implement user-level settings file at this
  // point. Do it when you have some time.
  char curDir[PATH_MAX];
  getcwd(curDir, PATH_MAX);

  // Enables syntax highlighting
  m_PropsBase.Read(GetWDESharedFile(WDEEDITORPANE_CONFIG_FILENAME), curDir);

  m_TextBuffer = GTK_OBJECT(scintilla_new());
  // Perhaps this should be set to some legitimate value?
  scintilla_set_id(SCINTILLA(m_TextBuffer), 0);
  gtk_signal_connect(GTK_OBJECT(m_TextBuffer), "notify",
		     GtkSignalFunc(WDEEditorPane_NotifySignal), this);

  SendEditor(SCI_SETLEXER, SCLEX_HTML);
  SendEditor(SCI_SETSTYLEBITS, 7);

  string fileNameForExtension = WEBLICATIONPROJECT_HTML_EXTENSION;

  SString kw0 = m_Props.GetNewExpand("keywords.", fileNameForExtension.c_str());
  SendEditorString(SCI_SETKEYWORDS, 0, kw0.c_str());
  SString kw1 = m_Props.GetNewExpand("keywords2.", fileNameForExtension.c_str());
  SendEditorString(SCI_SETKEYWORDS, 1, kw1.c_str());
  SString kw2 = m_Props.GetNewExpand("keywords3.", fileNameForExtension.c_str());
  SendEditorString(SCI_SETKEYWORDS, 2, kw2.c_str());
  SString kw3 = m_Props.GetNewExpand("keywords4.", fileNameForExtension.c_str());
  SendEditorString(SCI_SETKEYWORDS, 3, kw3.c_str());

  SendEditor(SCI_SETINDENTATIONGUIDES, true);
  
  Window wEditor;
  wEditor = GTK_WIDGET(m_TextBuffer);
  SetStyleFor(wEditor, "*");
  SetStyleFor(wEditor, "hypertext");

  SendEditor(SCI_COLOURISE, 0, -1);
  wEditor.InvalidateAll();

  SetDirty(FALSE);
}

void WDEEditorPane::LoadFromFile() {

  // load in given text file
  ifstream editorFile(m_Filename.c_str());
  string editorText, tempString;
  while (!editorFile.eof()) {
    getline(editorFile, tempString);
    editorText.append(tempString);
    editorText.append("\n");
  }



  SendEditorString(SCI_ADDTEXT, editorText.length(), editorText.c_str());
  SetDirty(FALSE);

}


WDEEditorPane::~WDEEditorPane() {
  // Do any memory freeing and cleanup here
  // Note: actually do this

  m_ProjManager->UnmapEditorFilename(m_Filename);

  //gtk_object_unref(GTK_OBJECT(m_TextBuffer));
  //gtk_object_destroy(GTK_OBJECT(m_TextBuffer));

  //if (m_Views.size() > 0)
  //gnome_mdi_remove_child(GNOME_MDI(GNOME_MDI_CHILD(m_EditorChild)->parent), GNOME_MDI_CHILD(m_EditorChild), true);

  //m_EditorChild = NULL;

}


void WDEEditorPane::SetName(string inName) {
  // sets the name of the editor pane. the name is what is displayed in
  // the titlebar, as opposed to the filename.
  
  gnome_mdi_child_set_name(GNOME_MDI_CHILD(m_EditorChild), inName.c_str());
}


string WDEEditorPane::GetName() {
  return string(GNOME_MDI_CHILD(m_EditorChild)->name);
}


bool WDEEditorPane::HandleEvent(EventType event, void *data) {

  bool wasHandled = false;

  guint32 time = GDK_CURRENT_TIME;

  switch (event) {
    /*
  case EVENT_Editor_Cut:
    gtk_editable_cut_clipboard(GTK_EDITABLE(m_Editors[0]));
    wasHandled = true;
    break;

  case EVENT_Editor_Copy:
    gtk_editable_copy_clipboard(GTK_EDITABLE(m_Editors[0]));
    wasHandled = true;
    break;

  case EVENT_Editor_Paste:
    gtk_editable_paste_clipboard(GTK_EDITABLE(m_Editors[0]));
    wasHandled = true;
    break;
    
  case EVENT_Editor_Clear:
    gtk_editable_delete_selection(GTK_EDITABLE(m_Editors[0]));
    wasHandled = true;
    break;
    */


  case EVENT_Editor_Cut:
    SendEditor(SCI_CUT);
    wasHandled = true;
    break;

  case EVENT_Editor_Copy:
    SendEditor(SCI_COPY);
    wasHandled = true;
    break;

  case EVENT_Editor_Paste:
    SendEditor(SCI_PASTE);
    wasHandled = true;
    break;
    
  case EVENT_Editor_Clear:
    SendEditor(SCI_CLEAR);
    wasHandled = true;
    break; 

  case EVENT_Editor_Close:
    gnome_mdi_remove_child(GNOME_MDI(GNOME_MDI_CHILD(m_EditorChild)->parent), GNOME_MDI_CHILD(m_EditorChild), FALSE);
   
    wasHandled = true;
    break;
    
  case EVENT_Editor_Save:
    Save();
    wasHandled = true;
    break;

  case EVENT_Editor_SaveAs:
    SaveAs();
    wasHandled = true;
    break;

  case EVENT_Editor_Undo:
  case EVENT_Editor_Redo:
  case EVENT_Editor_FindReplace:
  case EVENT_Editor_GotoLine:
  case EVENT_Editor_ToolbarToggle:

    // These events are not currently handled, but should be. Output an
    // error message.

    OutputMessage("Editor event not handled at WDEEditorPane::HandleEvent()\nEvent ID is: %d\n", event);
    
    wasHandled = false;  // Normally should be set to true
    break;

  default:
    // do nothing
    break;
  }

  return wasHandled;
}

GtkWidget *WDEEditorPane::CreateView(GnomeMDIChild *child, gpointer data)
{
  // Create and return the editor view widget tied to this window. 
  // Note: actually do this

/*
  GtkText *text = GTK_TEXT(gtk_text_new(NULL, NULL));
  gtk_text_set_word_wrap(text, true);
  gtk_text_set_editable(text, true);
*/


  GtkWidget *text = GTK_WIDGET(m_TextBuffer);
  GtkWidget *scroll = GTK_WIDGET(m_TextBuffer);

  m_Editors.push_back(GTK_WIDGET(text));

  GtkNotebook *book = GTK_NOTEBOOK(gtk_notebook_new());
  gtk_notebook_set_tab_pos(book, GTK_POS_BOTTOM);
  gtk_notebook_set_scrollable(book, true);
  gtk_notebook_set_tab_border(book, false);
  gtk_notebook_set_tab_hborder(book, false);
  gtk_notebook_set_tab_vborder(book, false);
  gtk_notebook_popup_enable(book);

  GtkWidget *source = gtk_hbox_new(FALSE, 3);
  GtkWidget *design = gtk_hbox_new(FALSE, 3);
  GtkWidget *source_icon = gnome_stock_pixmap_widget_at_size(NULL,
					      GNOME_STOCK_PIXMAP_ALIGN_LEFT, 18, 18);
  int width, height;
  gdk_window_get_size((GdkWindow*)(GNOME_PIXMAP(source_icon)->pixmap),
		      &width, &height);

  GtkWidget *source_label = gtk_label_new("Source");

  GtkWidget *design_icon = gnome_stock_pixmap_widget_at_size(NULL,
					      GNOME_STOCK_PIXMAP_INDEX,
							     18, 18);
  GtkWidget *design_label = gtk_label_new("Design");
  
  gtk_widget_show(source_icon);
  gtk_widget_show(source_label);
  gtk_widget_show(design_icon);
  gtk_widget_show(design_label);

  gtk_box_pack_start(GTK_BOX(source), source_icon, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(source), source_label, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(design), design_icon, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(design), design_label, FALSE, FALSE, 0);

  gtk_widget_show(source);

  gtk_notebook_append_page_menu(book, GTK_WIDGET(scroll), 
				GTK_WIDGET(source),
				gtk_label_new("Source"));

  
  // Create the design view, add it to the notebook

  //OutputMessage("chars %d", gtk_text_buffer_get_char_count(GTK_TEXT_BUFFER(m_TextBuffer)));
  
  GtkWidget *browser = gtk_moz_embed_new();

  gtk_signal_connect_after(GTK_OBJECT(browser), "realize",
			   GTK_SIGNAL_FUNC(WDEEditorPane_ExternMozRealize), m_TextBuffer);

  // set the chrome type so it's stored in the object
  //gtk_moz_embed_set_chrome_mask(GTK_MOZ_EMBED(browser), GTK_MOZ_EMBED_FLAG_DEFAULTCHROME);

  //gtk_moz_embed_load_url(GTK_MOZ_EMBED(browser), m_Filename.c_str());

  /*
  gtk_moz_embed_render_data(GTK_MOZ_EMBED(browser), 
			    gtk_text_buffer_get_text_chars(GTK_TEXT_BUFFER(m_TextBuffer),
							   0,
							   gtk_text_buffer_get_char_count(GTK_TEXT_BUFFER(m_TextBuffer)),
							   FALSE),
			    gtk_text_buffer_get_char_count(GTK_TEXT_BUFFER(m_TextBuffer)),
			    "file://",
			    "text/html");
  */


  gtk_widget_set_name(GTK_WIDGET(browser), "GtkMozEmbed Design View");
  gtk_widget_set_name(GTK_WIDGET(book), "EditorPane Source/Design Notebook");
  gtk_widget_set_name(GTK_WIDGET(scroll), "ScrolledWindow Source");
  gtk_widget_set_name(GTK_WIDGET(text), "GtkTextView Source Editor");



  m_Designs.push_back(browser);
  
  
  gtk_notebook_append_page_menu(book, browser, GTK_WIDGET(design),
				gtk_label_new("Design"));
  
  gtk_signal_connect(GTK_OBJECT(book),"switch_page",
		     GTK_SIGNAL_FUNC(WDEEditorPane_ExternNotebookPageSwitch),this);

  gtk_widget_show(GTK_WIDGET(source));
  gtk_widget_show(GTK_WIDGET(text));
  gtk_widget_show_all(GTK_WIDGET(scroll));
  gtk_widget_show(GTK_WIDGET(browser));

  gtk_widget_show(GTK_WIDGET(book));

  GtkWidget *newView = GTK_WIDGET(book); 

  // Add to internal view list. There should only be one view, but it may
  // change. Then set user data pointer to this object.
  m_Views.push_back(newView);
  gtk_object_set_user_data(GTK_OBJECT(newView), this);

  return GTK_WIDGET(newView); // gtk_label_new(m_Filename.c_str());
}


gint WDEEditorPane::DeleteChild(GnomeMDI *mdi, GnomeMDIChild *child, gpointer
				data) {

  GList *views = g_list_copy(child->views);
  GList *head = views;
  gint allow = TRUE;

  while (views != NULL && allow) {
    allow &= DeleteView(mdi, GTK_WIDGET(views->data), this); 
    //gnome_mdi_remove_view(mdi, GTK_WIDGET(views->data), false);
    views = g_list_next(views);
  }

  g_list_free(head);
  if (allow) 
    delete this;

  return allow;
}


gint WDEEditorPane::DeleteView(GnomeMDI *mdi, GtkWidget *widget, gpointer
				data) {
  // Destroy all objects related to the editor properly here. Return FALSE
  // if the editor is not to be deleted, i.e. the user clicks cancel.
  // This is essentially a "close" function, and should be treated as
  // such.

  if (IsNewborn() || IsDirty()) {
    // We may need to perform a save. Ask the user if they would like to
    // save.
    char messageStr[256];

    g_snprintf(messageStr, 256, WDEEDITORPANE_TEXT_CLOSEDIALOG, GetName().c_str());

    GtkWidget *dlg = gnome_message_box_new(messageStr,
					   GNOME_MESSAGE_BOX_QUESTION,
					   GNOME_STOCK_BUTTON_YES,
					   GNOME_STOCK_BUTTON_NO,
					   GNOME_STOCK_BUTTON_CANCEL,
					   NULL);

    if (m_Views.size() < 1) {
      OutputMessage("Error: No view for child at WDEEditorPane::DeleteView()");
      return TRUE;
    }
    GnomeApp *app = gnome_mdi_get_app_from_view(widget);
    gnome_dialog_set_parent(GNOME_DIALOG(dlg),
			    GTK_WINDOW(app));
    

    int i;

    bool shouldSave = false;
    i = gnome_dialog_run_and_close(GNOME_DIALOG(dlg));
    switch (i) {
    case 0:
      /*the user pressed Yes, so record that we should save and
	exit dialog. */
      //OutputMessage("user clicked yes\n");
      shouldSave = true;
      break;
    case -1:
      /*the user closed the dialog from the window
	manager*/
      // interpret as cancel
      //OutputMessage("user clicked close box\n");
      return FALSE;
      break;
    case 1:
      /*user pressed no, so we don't want to save*/
      //OutputMessage("user clicked no\n");
      shouldSave = false;
      break;
    case 2:
      /*user pressed cancel so we don't want to close*/
      //OutputMessage("user clicked cancel\n");
      return FALSE;
      break;
    } 
 
    // Now we simply use the shouldSave flag to determine whether we should
    // save or not, and remove the project.
    if (shouldSave) {
      Save();
      if (IsNewborn() || IsDirty()) {
	// User didn't actually save their new document, that crazy
	// devil. Return false.
	return FALSE;
      }
    }
  }

  // Close completed successfully. Clean up memory.
  
  // Close and destroy all associated objects.
  // Note: actually do this
  /*
  OutputMessage("Begin destroying browser");
  //gtk_widget_destroy(m_Designs[0]);
  OutputMessage("Begin destroying text view");
  gtk_widget_destroy(m_Editors[0]);
  OutputMessage("Destruction complete");
  m_Editors.erase(m_Editors.begin());
  m_Designs.erase(m_Designs.begin());

  // Note: do we delete the view here? or not?
  if (m_Views.size() > 0)
    gtk_widget_destroy(m_Views[0]);
  m_Views.erase(m_Views.begin());

  if (m_Views.size() == 0) {
    //gtk_signal_emit_stop_by_name(GTK_OBJECT(mdi), "remove_view");
    delete this;
  }
  */
  return TRUE;

}

void WDEEditorPane::Save(bool force = false) {
  // perform a save here, from the text buffer to the file. set the
  // modified bit to false through IsDirty(FALSE).

  // We should ask the user about saving here, same as in the project.

  if (IsNewborn()) {
    SaveAs();
    return;
  }

  if (IsDirty() || force) {
    // write the data from the text buffer back out to a file
    ofstream editorFile(m_Filename.c_str());


  unsigned long length = SendEditor(SCI_GETLENGTH);
  if (length > 0) {
    char *textBuf = new char[length + 100];
    SendEditorString(SCI_GETTEXT, length, textBuf);
    editorFile << textBuf;
    delete textBuf;
  }

  }

  SetDirty(FALSE);
}


void WDEEditorPane::SaveAs() {
  // This is the first time the project has been saved. So ask the user
  // to save the project in a given location.
  // JustBorn() should not have been set for druid-ized projects.

  // Asks for a file, then provides the filename to a new WDEEditorPane
  // that it creates, adding the editor to the MDIManager.
  // Using sample code from
  // http://developer.gnome.org/doc/tutorials/gnome-libs/using-the-libgnomeui-library.html

  GtkFileSelection *fsel;
  
  /* create a new file selection widget */
  fsel = GTK_FILE_SELECTION
    (gtk_file_selection_new (_("Sash WDE: Save Editor File As...")));
    
  /* Connect the signals for Ok and Cancel */
  gpointer* args = new gpointer[2];
  args[0] = this;
  args[1] = fsel;
  gtk_signal_connect (GTK_OBJECT (fsel->ok_button), "clicked",
		      GTK_SIGNAL_FUNC
		      (WDEEditorPane_ExternSaveAsClickedOK), args);
  gtk_signal_connect_object
    (GTK_OBJECT (fsel->cancel_button), "clicked",
     GTK_SIGNAL_FUNC (gtk_main_quit), 
     GTK_OBJECT(fsel));
  
  /* set the position at the mouse cursor, we do this because this
     is not a gnome dialog and thus it isn't set for us.  You
     shouldn't do this for normal gnome dialogs though */
  gtk_window_position (GTK_WINDOW (fsel), GTK_WIN_POS_MOUSE);
  gtk_file_selection_set_filename(fsel, m_Filename.c_str());
  gtk_window_set_modal(GTK_WINDOW(fsel), true);
  gtk_widget_show (GTK_WIDGET (fsel));

  gtk_main();
  gtk_widget_destroy(GTK_WIDGET(fsel));

}


void WDEEditorPane::BecomeForeground() {
  // bring this editor pane to the foreground, and throw proper state event
  // Note: this code is a little rude. it would be better to code this in
  // the MDIManager, and always call the mdi manager to bring an editor to
  // foreground. But doing it this way makes things a little easier,
  // without requiring the MDIManager to know about the EditorPane children.
  /*
  assert(g_list_first(GNOME_MDI_CHILD(m_EditorChild)->views) ==
	 g_list_last(GNOME_MDI_CHILD(m_EditorChild)->views));
  GtkWidget *view = GTK_WIDGET(g_list_first(GNOME_MDI_CHILD(m_EditorChild)->views));
  assert(view != NULL);

  gnome_mdi_set_active_view(GNOME_MDI(GNOME_MDI_CHILD(m_EditorChild)->parent),
			    view);
  */
  if (m_Views.size() < 1) {
    OutputMessage("Error: No views available at WDEEditorPane::BecomeForeground()\n");
  } else {
    gnome_mdi_set_active_view(GNOME_MDI(GNOME_MDI_CHILD(m_EditorChild)->parent), m_Views[0]);
  }

  BroadcastEvent(EVENT_State_EditorForeground, this);
  
}

GtkWidget *WDEEditorPane::LabelFunc(GnomeMDIChild *child,
        GtkWidget *currentLabel,gpointer data)
{
  GtkWidget *output;

  if(currentLabel == NULL) {
    GtkWidget *text = gtk_label_new(GetName().c_str());
    GtkWidget *button = gtk_button_new();
    gtk_widget_set_usize(button, 16, 12);
    gtk_container_set_border_width(GTK_CONTAINER(button), 2);
    gtk_object_set_user_data(GTK_OBJECT(button), this);
    gtk_signal_connect(GTK_OBJECT(button),
		       "clicked",
		       GTK_SIGNAL_FUNC(WDEEditorPane_ExternCloseBoxClicked),
		       (gpointer)this);

    GtkWidget *hbox = gtk_hbox_new(FALSE, 3);
    gtk_box_pack_start(GTK_BOX(hbox), text, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);

    gtk_widget_show(text);
    gtk_widget_show(button);
    gtk_widget_show(hbox);
    output = hbox;
  } else {
    GList *children = gtk_container_children(GTK_CONTAINER(currentLabel));
    GtkWidget *current;
    while (children != NULL) {
      current = GTK_WIDGET(children->data);
      if (GTK_IS_LABEL(current))
	break;
    }
    gtk_label_set_text(GTK_LABEL(current),
		       GetName().c_str());

    output = currentLabel;
  }
  
  return(output);
}


// Extern prototypes for GTK Signal bindings
extern "C" GtkWidget *WDEEditorPane_ExternCreateView(GnomeMDIChild *child,
						gpointer data) {
  // Note: do dynamic type checking here.
  return ((WDEEditorPane*)data)->CreateView(child, data);
}

extern "C" GtkWidget *WDEEditorPane_ExternLabelFunc(GnomeMDIChild *child,
						     GtkWidget
						     *currentLabel, 
						     gpointer data) {
  // Note: do dynamic type checking here.
  return ((WDEEditorPane*)data)->LabelFunc(child, currentLabel, data);
}



extern "C" gint WDEEditorPane_ExternDeleteView(GnomeMDI *mdi, GtkWidget *widget, gpointer
					       data) {
  // Note: do dynamic type checking here.
  WDEEditorPane *editor = (WDEEditorPane*)gtk_object_get_user_data(GTK_OBJECT(widget));
  return editor->DeleteView(mdi, widget, data);
}

extern "C" gint WDEEditorPane_ExternDeleteChild(GnomeMDI *mdi, GnomeMDIChild *child, gpointer
					       data) {
  // Note: do dynamic type checking here.
  WDEEditorPane *editor = (WDEEditorPane*)gtk_object_get_user_data(GTK_OBJECT(child));
  return editor->DeleteChild(mdi, child, data);
}



bool WDEEditorPane::IsDirty() {
  return m_Dirty;
}

void WDEEditorPane::SetDirty(bool inDirty = TRUE) {

  if (inDirty == true) {
    OutputMessage("Error: tried to set dirty to true with Scintilla editor at WDEEditorPane::SetDirty()");
    return;
  }
  // inDirty was false, so we want to set the save point reached
  SendEditor(SCI_SETSAVEPOINT);
}


void WDEEditorPane::SetSavePoint() {
  SetDirty(false);
}


void WDEEditorPane::SaveAsClickedOK(GtkObject *button,
					       gpointer args)
{
  string fileName = gtk_file_selection_get_filename(GTK_FILE_SELECTION(args));
  GtkWidget *win = gtk_widget_get_toplevel(GTK_WIDGET(button));
  if (win == NULL || !GTK_IS_WINDOW(win)) {
    OutputMessage("Error: button is not in a window at"
" WDEEditorPane::SaveAsClickedOK\n");
    gtk_main_quit();
    return;
  }

  char fullPathChar[PATH_MAX];
  realpath(fileName.c_str(), fullPathChar);

  struct stat buf;
  if (stat(fullPathChar, &buf) == -1) {
    OutputMessage("Error: bad filename provided at WDEEditorPane::SaveAsClickedOK");
    gtk_main_quit();
  }

  string fullPath(fullPathChar);
  
  int n = fullPath.length() - 1;
  while (fullPath[n] != '/' && n >= 0)
    n--;
  // set n to the character after the last slash, or 0
  n++;
  
  string editorName = fullPath.substr(n);

  m_JustBorn = false;
  SetName(editorName);

  m_ProjManager->RemapEditorFilename(m_Filename, fullPath, this);
  m_Filename = fullPath;

  Save(true);
  gtk_main_quit();
}

extern "C" void WDEEditorPane_ExternSaveAsClickedOK(GtkObject
							   *button,
							   gpointer args)
{
  ((WDEEditorPane*)(((gpointer*)args)[0]))->SaveAsClickedOK(button,
							    ((gpointer*)args)[1]);

}

extern "C" void WDEEditorPane_ExternCloseBoxClicked(GtkObject
						    *button,
						    gpointer data)
{
  ((WDEEditorPane*)(((gpointer)data)))->HandleEvent(EVENT_Editor_Close,
						    NULL);

}

extern "C" void WDEEditorPane_ExternNotebookPageSwitch(GtkNotebook *notebook,
						       GtkNotebookPage *page,
						       gint page_num,
						       WDEEditorPane *editor)
{
  printf("ex_note_pg_sw\n\n");
  // Very neccesary, very annoying hack. Page switch event is thrown at
  // shutdown time, so must check and not deal with page switch if we are
  // unrealized, as otherwise we dereference a already destroyed C++
  // object!
  if (!GTK_WIDGET_REALIZED(GTK_WIDGET(notebook)))
    return;

  // Note: do dynamic type checking here.
  return editor->NotebookPageSwitch(notebook, page,
				    page_num, editor);
}


void WDEEditorPane::NotebookPageSwitch(GtkNotebook *notebook,
				       GtkNotebookPage *page,
				       gint page_num,
				       gpointer data) {

  GtkWidget *current_page = page->child;

  if (GTK_IS_SCROLLED_WINDOW(page->child)) {
    // change current_page to be the child of the scrolled window
    GList *list = gtk_container_children(GTK_CONTAINER(page->child));
    if (list != NULL) {
      current_page = (GtkWidget*)list->data;
    }
  }

  if (GTK_IS_MOZ_EMBED(current_page)) {
    if (!GTK_WIDGET_REALIZED(current_page)) {
      return;
    }
    
    WDEEditorPane_ExternMozRealize(current_page, m_TextBuffer);

  }
}



extern "C" void WDEEditorPane_ExternMozRealize(GtkWidget *widget,
					       gpointer data) {
printf("extern moz real\n\n");
  unsigned long length = scintilla_send_message(SCINTILLA(data),
						SCI_GETLENGTH, 0, 0);
  if (length > 0) {
    char *textBuf = new char[length + 100];
    scintilla_send_message(SCINTILLA(data),
			   SCI_GETTEXT, length, reinterpret_cast<LPARAM>(textBuf));
    gtk_moz_embed_render_data(GTK_MOZ_EMBED(widget), 
			      textBuf, length,
			      "file://",
			      "text/html");

    delete textBuf;
  }
}


LRESULT WDEEditorPane::SendEditor(UINT msg, WPARAM wParam = 0, LPARAM
				  lParam = 0) {
	return Platform::SendScintilla(GTK_WIDGET(m_TextBuffer), msg, wParam, lParam);
}


LRESULT WDEEditorPane::SendEditorString(UINT msg, WPARAM wParam, const char *s) {
	return SendEditor(msg, wParam, reinterpret_cast<LPARAM>(s));
}

void WDEEditorPane::Notify(SCNotification *notification) {
	switch (notification->nmhdr.code) {
	case SCN_KEY: { /*
			int mods = 0;
			if (notification->modifiers & SHIFT_PRESSED)
				mods |= GDK_SHIFT_MASK;
			if (notification->modifiers & LEFT_CTRL_PRESSED)
				mods |= GDK_CONTROL_MASK;
			if (notification->modifiers & LEFT_ALT_PRESSED)
				mods |= GDK_MOD1_MASK;
			//Platform::DebugPrintf("SCN_KEY: %d %d\n", notification->ch, mods);
			if ((mods == GDK_CONTROL_MASK) && (notification->ch == VK_TAB)) {
				//Command(IDM_NEXTFILE);
			} else if ((mods == GDK_CONTROL_MASK | GDK_SHIFT_MASK ) && (notification->ch == VK_TAB)) {
				//Command(IDM_PREVFILE);
			} else {
				//gtk_accel_group_activate(accelGroup, notification->ch,
				//                         static_cast<GdkModifierType>(mods));
			}
			*/
		}
		break;

	case SCN_SAVEPOINTREACHED:
	  m_Dirty = false;
	  // Insert calls here to actually trigger proper redraws
	  // everywhere
	  // Note: actually do this
          //OutputMessage("editor document has been set to NOT DIRTY");
	  break;
	  
        case SCN_SAVEPOINTLEFT:
	  m_Dirty = true;
	  // Insert calls here to actually trigger proper redraws
	  // everywhere
	  // Note: actually do this
          //OutputMessage("editor document IS NOW DIRTY");
	  break;

	default:
	  // Do nothing
	  // OutputMessage("Scintilla event: %d", notification->nmhdr.code);
		break;

	}
}

// Code taken from SciTE dist file "SciTEBase.cxx"
//

void WDEEditorPane::SetStyleFor(Window &win, const char *lang) {
	for (int style = 0; style <= STYLE_MAX; style++) {
		if (style != STYLE_DEFAULT) {
			char key[200];
			sprintf(key, "style.%s.%0d", lang, style);
			SString sval = m_Props.GetNewExpand(key, WDEEDITORPANE_CONFIG_FILENAME);
			SetOneStyle(win, style, sval.c_str());
		}
	}
}

void WDEEditorPane::SetOneStyle(Window &win, int style, const char *s) {
	char *val = StringDup(s);
	//Platform::DebugPrintf("Style %d is [%s]\n", style, val);
	char *opt = val;
	while (opt) {
		char *cpComma = strchr(opt, ',');
		if (cpComma)
			*cpComma = '\0';
		char *colon = strchr(opt, ':');
		if (colon)
			*colon++ = '\0';
		if (0 == strcmp(opt, "italics"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETITALIC, style, 1);
		if (0 == strcmp(opt, "notitalics"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETITALIC, style, 0);
		if (0 == strcmp(opt, "bold"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETBOLD, style, 1);
		if (0 == strcmp(opt, "notbold"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETBOLD, style, 0);
		if (0 == strcmp(opt, "font"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETFONT, style, reinterpret_cast<LPARAM>(colon));
		if (0 == strcmp(opt, "fore"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETFORE, style, ColourFromString(colon).AsLong());
		if (0 == strcmp(opt, "back"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETBACK, style, ColourFromString(colon).AsLong());
		if (0 == strcmp(opt, "size"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETSIZE, style, atoi(colon));
		if (0 == strcmp(opt, "eolfilled"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETEOLFILLED, style, 1);
		if (0 == strcmp(opt, "noteolfilled"))
			Platform::SendScintilla(win.GetID(), SCI_STYLESETEOLFILLED, style, 0);
		if (cpComma)
			opt = cpComma + 1;
		else
			opt = 0;
	}
	if (val)
		delete []val;
}

static int IntFromHexDigit(const char ch) {
	if (isdigit(ch))
		return ch - '0';
	else if (ch >= 'A' && ch <= 'F')
		return ch - 'A' + 10;
	else if (ch >= 'a' && ch <= 'f')
		return ch - 'a' + 10;
	else
		return 0;
}

static Colour ColourFromString(const char *val) {
	int r = IntFromHexDigit(val[1]) * 16 + IntFromHexDigit(val[2]);
	int g = IntFromHexDigit(val[3]) * 16 + IntFromHexDigit(val[4]);
	int b = IntFromHexDigit(val[5]) * 16 + IntFromHexDigit(val[6]);
	return Colour(r, g, b);
}

extern "C" void WDEEditorPane_NotifySignal(GtkWidget *, gint /*wParam*/, gpointer lParam, WDEEditorPane *editor) {
	//Platform::DebugPrintf("Notify: %x %x %x\n", w, wParam, lParam);
	editor->Notify(reinterpret_cast<SCNotification *>(lParam));
}

