/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/

// WDEMDIManager.cpp
// MDI manager class for the Sash WDE.

#include "WDEMDIManager.h"
#include "WDEProjectManager.h"
#include <gnome.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include "WDEglobal.h"
#include "wdeconstants.h"
#include "sash_error.h"
#include "events.h"

WDEMDIManager::WDEMDIManager(WDEProjectManager *manager) {
  // Do all necessary MDI setup here. Must create first toplevel MDI
  // window, which will be empty.
  m_ProjManager = manager;

  CreateMDI();
}

void WDEMDIManager::CreateMDI() {

  m_MDI = gnome_mdi_new("sashwdemdi","Sash WDE MDI");

  gtk_object_set_user_data(m_MDI, this);

  // We are overriding defaults here becuase the defaults are
  // unreasonable in our application. Later, these should be
  // user-configurable.
  gnome_mdi_set_mode(GNOME_MDI(m_MDI), GNOME_MDI_NOTEBOOK);
  GNOME_MDI(m_MDI)->tab_pos = GTK_POS_TOP;
  
  gtk_signal_connect(m_MDI,"destroy",
  	     GTK_SIGNAL_FUNC(WDEMDIManager_ExternAppDeleted),this);

  gtk_signal_connect(m_MDI,"app_created",
		     GTK_SIGNAL_FUNC(WDEMDIManager_ExternAppCreated),this);

  gtk_signal_connect(m_MDI, "remove_view",
		     GTK_SIGNAL_FUNC(WDEEditorPane_ExternDeleteView),this);
  
  gtk_signal_connect(m_MDI, "remove_child",
		     GTK_SIGNAL_FUNC(WDEEditorPane_ExternDeleteChild),this);

 		     /*
		       GTK_SIGNAL_FUNC(eventDestroy),NULL);
  gtk_signal_connect(m_MDI,"app_created",
		     GTK_SIGNAL_FUNC(appCreated),NULL);
  gtk_signal_connect(m_MDI,"remove_child",
		     GTK_SIGNAL_FUNC(removeChild),NULL);
  */

  // Do any other setup here

  gnome_mdi_open_toplevel(GNOME_MDI(m_MDI));

}


WDEMDIManager::~WDEMDIManager() {
  // Destroy all views, children, and the mdi object.
  // Note: actually do this

}

bool WDEMDIManager::HandleEvent(EventType event, void *data) {

  bool wasHandled = false;

  switch (event) {
    // case EVENT_Type_SomeEvent:
    //   DoSomething();
    //   wasHandled = true;
    //   break;

  default:
    // do nothing
    break;
  }

  return wasHandled;
}


GtkObject* WDEMDIManager::GetCurrentWindow() {
  return (GtkObject*)gnome_mdi_get_active_window(GNOME_MDI(m_MDI));
}

GtkObject* WDEMDIManager::GetCurrentChild() {
  return (GtkObject*)gnome_mdi_get_active_child(GNOME_MDI(m_MDI));
}

void* WDEMDIManager::GetCurrentObject() {
  GtkObject *child = GetCurrentChild();
  if (GNOME_IS_MDI_GENERIC_CHILD(child))
    return (void*)((GNOME_MDI_GENERIC_CHILD(child))->create_view_data);
  else {
    OutputMessage("Error: WDEMDIManager::GetCurrentObject() found
 non-GenericChild.\n");
    return NULL;
  }
}

void WDEMDIManager::OpenNewWindow(GnomeMDIChild *child, bool makeToplevel =
				  false) {
  // Insert child/view addition code here.

  gnome_mdi_add_child(GNOME_MDI(m_MDI),
		      GNOME_MDI_CHILD(child));
  if (makeToplevel) {
    gnome_mdi_add_toplevel_view(GNOME_MDI(m_MDI),
		       GNOME_MDI_CHILD(child));
  } else {
    gnome_mdi_add_view(GNOME_MDI(m_MDI),
		       GNOME_MDI_CHILD(child));
  }
}

void WDEMDIManager::CloseWindowByChild(GnomeMDIChild *child) {
  // Search through the list of children, and remove the child that
  // matches the given child.
  // Note: actually do this
}

void CloseWindowByName(string editorName) {
  // Similar to above, except search through for some name string.
  // Note: actually do this
}

// Extern prototypes for GTK Signal bindings
extern "C" void WDEMDIManager_ExternAppCreated(GnomeMDI *mdi,GnomeApp *app,
        gpointer data)
{
   // Note: do dynamic type checking here.
OutputMessage("App Created\n");
  return ((WDEMDIManager*)data)->AppCreated(mdi, app, data);
}

void WDEMDIManager::AppCreated(GnomeMDI *mdi, GnomeApp *app, gpointer data) {
  // set the default window size
  gtk_window_set_default_size(GTK_WINDOW(app),
			      WDEMDIMANAGER_WINDOW_WIDTH,
			      WDEMDIMANAGER_WINDOW_HEIGHT);

  // create the menubars and toolbars here
  
  // create menubar
  GtkWidget *newMenubar = GladeGrabWidget(GetWDEGladeFile(WDEMDIMANAGER_GLADE_FILENAME),
					  WDEMDIMANAGER_GLADE_MENUBAR,
					  this, true);
  gnome_app_set_menus(app, GTK_MENU_BAR(newMenubar));
  
  // create toolbar
  GNOME_DOCK(app->dock)->floating_items_allowed = FALSE;
  GtkWidget *newToolbar = GladeGrabWidget(GetWDEGladeFile(WDEMDIMANAGER_GLADE_FILENAME),
					  WDEMDIMANAGER_GLADE_TOOLBAR,
					  this, true);
  gnome_app_set_toolbar(app, GTK_TOOLBAR(newToolbar));

  gtk_object_set_user_data(GTK_OBJECT(app), this);

  //gtk_signal_disconnect_by_func(GTK_OBJECT(app), app_destroy, m_MDI);
  //gtk_signal_connect(GTK_OBJECT(app),"destroy",
		     /*
		       GTK_SIGNAL_FUNC(MDIEventMagic),
		     (gpointer)EVENT_WDE_CloseProject);
		     
		     GTK_SIGNAL_FUNC(WDEMDIManager_ExternAppDeleted),
		     this);
		     */
}


extern "C" gint WDEMDIManager_ExternAppDeleted(GtkWidget *widget, gpointer data) {
  OutputMessage("ExternAppDeleted recived\n");
  //gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "destroy");

  //return TRUE;
   // Note: do dynamic type checking here.
  return ((WDEMDIManager*)data)->AppDeleted(widget, data);
}

gint WDEMDIManager::AppDeleted(GtkWidget *widget, gpointer data) {

  //if (g_list_length(GNOME_MDI(m_MDI)->windows) == 0) {
    BroadcastEvent(EVENT_WDE_Quit, NULL);
  
    if (m_ProjManager->GetCurrentProject() != NULL) {
      // User clicked cancel, so we don't actually want to quit
      CreateMDI();
    }

    //}
  return TRUE;
}

bool WDEMDIManager::Closedown() {
  return gnome_mdi_remove_all(GNOME_MDI(m_MDI), FALSE);
}

