/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/

#ifndef WDEPROJECTMANAGER_H
#define WDEPROJECTMANAGER_H

#include "EventHandler.h"
#include "WeblicationProject.h"
#include "WDEMDIManager.h"
#include "WDESettingsPane.h"
#include "WDEEditorPane.h"
#include "WDEActionsPane.h"
#include "WDEInstallSettingsPane.h"
#include "WDEAddFilePane.h"
#include <string>
#include <map>
#include <vector>

class SashExtension;
class SashExtensionItem;
struct ActionDruidInfo;

class WDEProjectManager : public EventHandler {
 public:
  WDEProjectManager();
  ~WDEProjectManager();

  bool HandleEvent(EventType event, void *data);

  // Responses to WDE Events
  void Quit();
  void NewProject();//moved to WDEApplication
  void CloseProject();//moved to WDEApplication
  void SaveProject();//moved to WeblicationProject
  void OpenProject();//moved to WeblicationProject
  void OpenEditorFile();
  void NewEditorFile();
  void NewAction();//moved to WeblicationProject
  void SaveProjectAs();
  void ShowAboutBox();
  
  // publishes to subfolder in the current project folder
  void Publish(const std::string & foldername);
  void ProjectSaveAsClickedOK(GtkObject *button,
			      gpointer args);
  void EditorOpenFileClickedOK(GtkObject *button,
			       gpointer args);
  void ProjectOpenClickedOK(GtkObject *button,
			    gpointer args);
  bool RequestEditorFilename(string inFilename, bool
			     createNew = false);
  void UnmapEditorFilename(string oldpath);
  void RemapEditorFilename(string oldpath, string
			   newpath, WDEEditorPane
			   *editor);
			
  // WeblicationProject *GetProject();
  WeblicationProject *GetProject();
  WDEMDIManager *GetMDIManager();

  
  // This stuff seems to be broken for the moment
  WeblicationProject* GetCurrentProject() const;
  
 private:
  // Private Functions
  void CreateNewWeblicationProject(const std::string &projectpath,
				   const std::string &projectname, bool load = FALSE);

  WeblicationProject *m_project;

  // All other project wide objects should be created and added here.
  WDEMDIManager          m_MDIManager;
  WDESettingsPane        m_SettingsPane;
  WDEActionsPane         m_ActionsPane;
  WDEInstallSettingsPane m_InstallSetPane;
  WDEAddFilePane         m_AddFileDialog;

  // Other member data
  std::map<std::string, WDEEditorPane*> m_EditorMap;
  //std::map<std::string, WDEActionsInstance*> m_ActionsMap;
  int m_UntitledProjectCounter;
  int m_UntitledEditorCounter;
};

extern "C" void WDEProjectManager_ExternEditorOpenFileClickedOK(GtkObject
								*button,
								gpointer args);
extern "C" void WDEProjectManager_ExternProjectOpenClickedOK(GtkObject
							     *button,
							     gpointer args);
extern "C" void WDEProjectManager_ExternProjectSaveAsClickedOK(GtkObject
							      *button,
							      gpointer
							       args);

#endif /* WDEPROJECTMANAGER_H */
