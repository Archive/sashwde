function VorbForm_onLoad()
{

}

function VorbForm_onUnload()
{

}


/*
 * signal handler stub
 *
 *   widget: addButton
 *   signal: clicked
 *
 */
function addClicked(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: fastforwardButton
 *   signal: clicked
 *
 */
function fastforwardClicked(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: pauseButton
 *   signal: clicked
 *
 */
function pauseClicked(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: playButton
 *   signal: clicked
 *
 */
function playClicked(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: playlistToggle
 *   signal: toggled
 *
 */
function playlistToggled(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: removeButton
 *   signal: clicked
 *
 */
function removeClicked(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: repeatToggle
 *   signal: toggled
 *
 */
function repeatToggled(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: rewindButton
 *   signal: clicked
 *
 */
function rewindClicked(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: shuffleToggle
 *   signal: toggled
 *
 */
function shuffleToggled(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: stopButton
 *   signal: clicked
 *
 */
function stopClicked(widget, user_data)
{
}

function VorbForm()
{
	this.gladeForm = new Sash.Glade.GladeFile('vorbisplayer.glade');
	this.onLoad();

	this.mainwindow = this.gladeForm.getWidget('mainwindow');
	this.dock2 = this.gladeForm.getWidget('dock2');
	this.dockitem4 = this.gladeForm.getWidget('dockitem4');
	this.scrolledwindow2 = this.gladeForm.getWidget('scrolledwindow2');
	this.toolbar3 = this.gladeForm.getWidget('toolbar3');
	this.rewindButton = this.gladeForm.getWidget('rewindButton');
	this.playButton = this.gladeForm.getWidget('playButton');
	this.pauseButton = this.gladeForm.getWidget('pauseButton');
	this.stopButton = this.gladeForm.getWidget('stopButton');
	this.fastforwardButton = this.gladeForm.getWidget('fastforwardButton');
	this.playlistToggle = this.gladeForm.getWidget('playlistToggle');
	this.shuffleToggle = this.gladeForm.getWidget('shuffleToggle');
	this.repeatToggle = this.gladeForm.getWidget('repeatToggle');
	this.vbox9 = this.gladeForm.getWidget('vbox9');
	this.scrolledwindow3 = this.gladeForm.getWidget('scrolledwindow3');
	this.hbox3 = this.gladeForm.getWidget('hbox3');
	this.viewport2 = this.gladeForm.getWidget('viewport2');
	this.vbox10 = this.gladeForm.getWidget('vbox10');
	this.playlistVBox = this.gladeForm.getWidget('playlistVBox');
	this.addButton = this.gladeForm.getWidget('addButton');
	this.removeButton = this.gladeForm.getWidget('removeButton');
}

VorbForm.prototype.onLoad = VorbForm_onLoad;
VorbForm.prototype.onUnload = VorbForm_onUnload;
VorbForm.prototype.gladeForm = '';

VorbForm.prototype.rewindButton_onclicked = rewindClicked;
VorbForm.prototype.playButton_onclicked = playClicked;
VorbForm.prototype.pauseButton_onclicked = pauseClicked;
VorbForm.prototype.stopButton_onclicked = stopClicked;
VorbForm.prototype.fastforwardButton_onclicked = fastforwardClicked;
VorbForm.prototype.playlistToggle_ontoggled = playlistToggled;
VorbForm.prototype.shuffleToggle_ontoggled = shuffleToggled;
VorbForm.prototype.repeatToggle_ontoggled = repeatToggled;
VorbForm.prototype.addButton_onclicked = addClicked;
VorbForm.prototype.removeButton_onclicked = removeClicked;
