/**
  Contributors: Tyeler Quentmeyer, Stefan Atev
*/

using namespace std;

#include "config.h"
#include <glib.h>
#include <vector>
#include <gtk/gtk.h>
#include <gtkmozembed.h>
#include <gnome.h>
#include <glade/glade.h>

#include "WDEApp.h"
#include "prenv.h"
#include "WDEIGUI.h"
#include "WDEIEvent.h"
#include "nsLocalFile.h"

static void AppRegisterDirectories(vector<string> &cDirs) {
  nsILocalFile *bin_path;
  for (vector<string>::iterator i= cDirs.begin(); i< cDirs.end(); ++i) {
	   NS_NewLocalFile(i->c_str(), PR_FALSE, &bin_path);	   
	   nsComponentManager::AutoRegister(nsIComponentManagerObsolete::NS_Startup, bin_path);
  }
}

void WDEApp::Run(int argc, char **argv) {
	g_thread_init(NULL);
  glade_gnome_init();
  gnome_init("SashWDE", "1.0", argc, argv);
	gtk_moz_embed_push_startup();
//	SashErrorPushMode(SASH_ERROR_GNOME); 
  vector<string> components;
	// ~/.sashwde/components somehow does not work for me
	components.push_back("/home/bovine/tyeler/.sashwde/components");
	components.push_back("~/.sashwde/components");
	AppRegisterDirectories(components);
  nsCOMPtr<WDEIGUI> gui = do_CreateInstance("@gnome.org/SashWDE/WDEGUI;1");
  if (gui)
    gui->Run();
  else
    cerr << "GUI is null" << endl;
}
