/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/

#ifndef _WDE_APP_H
#define _WDE_APP_H

#include "nscore.h"
#include "nsCOMPtr.h"
#include "nsIFactory.h"
#include "nsIFile.h"
#include "nsComponentManagerUtils.h"
#include "nsIServiceManager.h"
#include "nsIGenericFactory.h"

//#include "InstallationManager.h"

#include "xpcomtools.h"
#include "WDEComMacros.h"

class WDEApp
{
public:
	static void Run(int argc, char **argv);
	
private:
};

#endif
