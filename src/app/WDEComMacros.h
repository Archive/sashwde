#ifndef WDECOMMACROS_H
#define WDECOMMACROS_H

/*! \file WDEComMacros.h
    \brief XPCOM utility macros used throughout the WDE
    
    Provides macros to return string constants, get/set properties
    and implement the default sashIUnknown methods
*/

//! Sets out to the (char *) value of in and returns NS_OK
#define XP_RETURN_STRING(in, out) \
XP_COPY_STRING(in, out); \
return NS_OK;

// Getter macros

//! Sets *out to in and returns NS_OK
#define XPCOM_SIMPLE_GETTER(in, out) \
*out = in; \
return NS_OK;

//! Sets *out to in and returns NS_OK; Returns NS_ERROR_NULL_POINTER if out is a null pointer
#define XPCOM_PTR_GETTER(in, out) \
if(!out) return NS_ERROR_NULL_POINTER; \
*out = in; \
return NS_OK;

//! Sets *out to in and increases *out's reference count. Behaves like XPCOM_PTR_GETTER 
#define XPCOM_COMPTR_GETTER(in, out) \
if(!out) return NS_ERROR_NULL_POINTER; \
*out = in; \
NS_IF_ADDREF(*out); \
return NS_OK;

//! Sets *out to the string in; Behaves like XPCOM_PTR_GETTER(in, out)
#define XPCOM_STR_GETTER(in, out) \
if(!out) return NS_ERROR_NULL_POINTER; \
*out = PL_strdup(in); \
return NS_OK;

// Setter macros

//! Sets member to param and returns NS_OK
#define XPCOM_SIMPLE_SETTER(member, param) \
member = param; \
return NS_OK;

//! Sets member to param and returns NS_OK
#define XPCOM_COMPTR_SETTER(member, param) \
member = do_QueryInterface(param); \
return NS_OK;

#define WDE_DEBUG(a) if (true) {a};


#endif
