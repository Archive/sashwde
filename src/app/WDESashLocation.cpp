#include <jsapi.h>
#include "WDESashLocation.h"
#include "debugmsg.h"
#include "sashIRuntime.h"
#include "sashIActionRuntime.h"
#include "extensiontools.h"
#include "sash_constants.h"
#include "sashIXPXMLDocument.h"
#include "sashIXPXMLNode.h"
#include <unistd.h>
#include "sashVariantUtils.h"

SASH_LOCATION_IMPL(WDESashLocation, WDEISashLocation, 
				   WDESASHLOCATION, "WDESashLocation", "WDE Sash location");

WDESashLocation::WDESashLocation() {
  NS_INIT_ISUPPORTS();
  NewSashJSEmbed(&m_js);
  m_js->StartJSInterpreter();
  m_gui= do_CreateInstance(WDEGUI_CONTRACT_ID);
  m_wde= do_CreateInstance(WDEWDE_CONTRACT_ID);
  m_jsevent= do_CreateInstance(WDEJSEVENTADAPTER_CONTRACT_ID);
  m_eventConst= do_CreateInstance(WDEEVENT_CONTRACT_ID);
}

WDESashLocation::~WDESashLocation() {
}

NS_IMETHODIMP WDESashLocation::Cleanup(){
  return NS_OK;
}

NS_IMETHODIMP WDESashLocation::Initialize(sashIActionRuntime *act,
		     const char *xml,
		     const char *instanceGuid, 
		     JSContext *context, JSObject *object) {
		     
  string xmlfile;
  m_act= act;
  
  JSContext *jsContext;
  m_js->GetScriptContext(&jsContext);
  m_act->CreateSashObjectNative(jsContext);
  m_jsevent->SetEventContext(m_js, m_act);

  nsCOMPtr<sashIXPXMLDocument> consoleXML= do_CreateInstance((nsCID)XPXMLDOCUMENT_CID);
  if (!consoleXML)
	  OutputError("Unable to create the XML parser\n");
  PRBool res;
  consoleXML->LoadFromString(xml, &res);
  if (!res)
	  OutputError("Unable to parse XML registration\n");

  nsCOMPtr<sashIXPXMLNode> root;
  consoleXML->GetRootNode(getter_AddRefs(root));
  if (!root)
	  OutputError("Unable to parse XML registration\n");

  nsCOMPtr<sashIXPXMLNode> jsfileNode;
  root->GetFirstChildNode("jsfilename", getter_AddRefs(jsfileNode));
  if (jsfileNode) {
	  XP_GET_STRING(jsfileNode->GetText, m_jsfile);
    if (m_jsfile!= "")
			return NS_OK;
  }
  return NS_COMFALSE;
}

NS_IMETHODIMP WDESashLocation::Run() {
  /* Evaluate the Javascript file */
  if (m_js->EvalScriptFile(m_jsfile.c_str())!= NS_OK) {
    return NS_COMFALSE;
  }
  m_act->EventLoop();
  return NS_OK;
}

NS_IMETHODIMP WDESashLocation::ProcessEvent() {
  return NS_OK;
}

NS_IMETHODIMP WDESashLocation::Quit() {
	m_act->Cleanup();
	_exit(0);
}

/************ WDEISashLocation Methods *****************/

/* readonly attribute WDEIWDE WDE; */
NS_IMETHODIMP WDESashLocation::GetWDE(WDEIWDE * *aWDE) {
  XPCOM_COMPTR_GETTER(m_wde, aWDE);
}

/* readonly attribute WDEIGUI GUI; */
NS_IMETHODIMP WDESashLocation::GetGUI(WDEIGUI * *aGUI) {
  XPCOM_COMPTR_GETTER(m_gui, aGUI);
}

/* readonly attribute WDEIJSEventAdapter EventAdapter; */
NS_IMETHODIMP WDESashLocation::GetEventAdapter(WDEIJSEventAdapter * *aEventAdapter) {
  XPCOM_COMPTR_GETTER(m_jsevent, aEventAdapter);
}

/* readonly attribute WDEIJSEventAdapter EventAdapter; */
NS_IMETHODIMP WDESashLocation::GetEventConst(WDEIEvent * *aEvent) {
  XPCOM_COMPTR_GETTER(m_eventConst, aEvent);
}

/* void print (in string msg); */
NS_IMETHODIMP WDESashLocation::Print(const char *msg) {
  cerr << msg;
  return NS_OK;
}

/* void println (in string msg); */
NS_IMETHODIMP WDESashLocation::Println(const char *msg) {
  cerr << msg << endl;
  return NS_OK;
}
