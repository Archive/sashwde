#ifndef WDESASHLOCATION_H
#define WDESASHLOCATION_H

#include "WDEISashLocation.h"
#include "sashILocation.h"
#include "sashIJSEmbed.h"
#include <string>
#include "WDEGUI.h"
#include "WDEWDEImpl.h"
#include "WDEJSEventAdapter.h"
#include "WDEEvents.h"
#include "WDEComMacros.h"

//fa71fdd3-2460-4976-a834-4c73bffe8546
#define WDESASHLOCATION_CID {0xfa71fdd3, 0x2460, 0x4976, {0xa8, 0x34, 0x4c, 0x73, 0xbf, 0xfe, 0x85, 0x46}}
#define WDESASHLOCATION_CONTRACT_ID "@gnome.org/SashWDE/WDESashLocation;1"

NS_DEFINE_CID(kWDESashLocationCID, WDESASHLOCATION_CID);

class WDESashLocation: public WDEISashLocation,
					public sashILocation
{
  NS_DECL_ISUPPORTS;
  NS_DECL_WDEISASHLOCATION;
  NS_DECL_SASHIEXTENSION;
  NS_DECL_SASHILOCATION;
  
  WDESashLocation();
  virtual ~WDESashLocation();

 private:
  sashIActionRuntime *m_act;
  string m_jsfile;
  sashIJSEmbed *m_js;
  nsCOMPtr<WDEIGUI> m_gui;
  nsCOMPtr<WDEIWDE> m_wde;
  nsCOMPtr<WDEIJSEventAdapter> m_jsevent;
  nsCOMPtr<WDEIEvent> m_eventConst;
};

#endif
