var wloc= Sash.WDESashLocation;
var evc= wloc.EventConst;

// "open a file"
var stab= wloc.GUI.sourcePane.createGenericSourceTab();
stab.name= "hello.js";
stab.editor.text= "Ok, if this works I can retire";
wloc.GUI.sourcePane.appendTab(stab);
// And another one
var stab2= wloc.GUI.sourcePane.createGenericSourceTab();
stab2.name= "world.js";
stab2.editor.text= "Maybe it did work after all";
wloc.GUI.sourcePane.appendTab(stab2);

// Listen to other people's messages :)
var pop= wloc.GUI.debugOutputPane.popupMenu;
wloc.EventAdapter.addJSProxy(pop, evc.PopupMenuItemSelected, "OnPop");

wloc.GUI.run();
wloc.Quit();

function OnPop(nsEv) {
  var event= nsEv.QueryInterface(Components.interfaces.WDEIEvent);
  switch (event.intData) {
    case 5001:
      wloc.println("Debug Item, Whatever Selected!");
      break;
  }
}
  