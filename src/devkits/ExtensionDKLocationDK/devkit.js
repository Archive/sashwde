/*
 Uncomment this section to add documentation for your development kit.
 Sash.LocationDK.helpDocs is a WDEITreeHelpItem.  See help documentation
 for more information.
*/
/*
Sash.LocationDK.helpDocs = Components.classes['@gnome.org/SashWDE/WDETreeHelpItem;1'].createInstance(Components.interfaces.WDEITreeItem);
Sash.LocationDK.helpDocs.name = 'yourname';
Sash.LocationDK.helpDocs.associatedHTML = true;
Sash.LocationDK.helpDocs.fileName = Sash.LocationDK.dataDirectory + 'yourhelpdoc.html';
*/

/*
 Some helper functions for your development kit.
*/
function escapeNewLine(str)
{
	var newstr = '';
	for(p in str)
		if(str[p] == '\n') newstr += '\\n';
		else newstr += str[p];
	return newstr;
}

var targetIID = "{unset}";
var targetCID = "{unset}";

configureactionwindow = "blah";

function ok_clicked_handler(widget)
{
	var win = gladewindow.getWidget("window");
	var nameentry = gladewindow.getWidget("name_entry");
	var targetiidentry = gladewindow.getWidget("targetiid_entry");	
	var targetcidentry = gladewindow.getWidget("targetcid_entry");
	
	win.hide();

	Sash.LocationDK.action.name = nameentry.getText();
	var dkfile = Sash.LocationDK.action.AddNewFile(0);

	dkfile.fileType = dkfile.WDETreeFileItemTypeJS;
	dkfile.fileName = Sash.LocationDK.WDE.activeProject.path + "devkit.js";

	targetIID = targetiidentry.getText();
	targetCID = targetcidentry.getText();

	var template = "";

	template += "/*\n";
	template += " Uncomment this section to add documentation for your development kit.\n";
	template += " Sash.ExtensionDK.helpDocs is a WDEITreeHelpItem.  See help documentation\n";
	template += " for more information.\n";
	template += "*/\n";
	
	template += "/*\n";
	template += "Sash.ExtensionDK.helpDocs = Components.classes['@gnome.org/SashWDE/WDETreeHelpItem;1'].createInstance(Components.interfaces.WDEITreeItem);\n";
	template += "Sash.ExtensionDK.helpDocs.name = 'yourname';\n";
	template += "Sash.ExtensionDK.helpDocs.associatedHTML = true;\n";
	template += "Sash.ExtensionDK.helpDocs.fileName = Sash.ExtensionDK.dataDirectory + 'yourhelpdoc.html';\n";
	template += "*/\n\n";
	
	template += "/*\n";
	template += " Some helper functions for your development kit.\n";
	template += "*/\n";
	
	template += "function escapeNewLine(str)\n";
	template += "{\n";
	template += "\tvar newstr = '';\n";
	template += "\tfor(p in str)\n";
	template += "\t\tif(str[p] == '\\n') newstr += '\\\\n';\n";
	template += "\t\telse newstr += str[p];\n";
	template += "\treturn newstr;\n";
	template += "}\n\n";
		
	dkfile.editorDM.text = template;
}

function NewAction()
{
/*
 This function is called when the user makes a new action in this location.
 If the user cancels inside this code, you need to do
 Sash.LocationDK.newActionCancelled = true
*/
	
	gladewindow = new Sash.Glade.GladeFile(Sash.LocationDK.dataDirectory + "edk.glade");
}

function BuildRegistration()
{
/*
 This function is called before writing the registration to the wdf.
 It should set the registration string to something meaningful.
*/

	var reg = "<jsfilename>" + Sash.LocationDK.action.GetChild(0).name + "</jsfilename>";
	reg += "<targetIID>" + targetIID + "</targetIID>";
	reg += "<targetCID>" + targetCID + "</targetCID>";

	Sash.LocationDK.registration = reg;
}

function LoadAction()
{
/*
 This function is called when loading a project with an action of this
 type.  It should restore the internal state of the development kit
 and the associated action by reading the registration string.
*/

	var reg = "<registration>" + Sash.LocationDK.registration + "</registration>";
	var d = new Sash.XML.Document();
	d.loadFromString(reg);
	var r = d.rootNode;

	//r.getFirstChildNode("jsfilename").text;
	targetIID = r.getFirstChildNode("targetIID").text;
	targetCID = r.getFirstChildNode("targetCID").text;
}

function ConfigureAction()
{
/*
 This function is called when the user wants to configure settings
 for an action using this development kit.  It should allow you to
 edit any internal settings you have.
*/
	if(configureactionwindow == "blah")
		configureactionwindow = new Sash.Glade.GladeFile(Sash.LocationDK.dataDirectory + "edk-config.glade");

	var win = configureactionwindow.getWidget("window");
	var nameentry = configureactionwindow.getWidget("name_entry");
	var targetiidentry = configureactionwindow.getWidget("targetiid_entry");	
	var targetcidentry = configureactionwindow.getWidget("targetcid_entry");
	
	nameentry.setText(Sash.LocationDK.action.name);
	targetiidentry.setText(targetIID);
	targetcidentry.setText(targetCID);

	win.show();
}

function config_ok_clicked_handler(widget)
{
	var win = configureactionwindow.getWidget("window");
	var nameentry = configureactionwindow.getWidget("name_entry");
	var targetiidentry = configureactionwindow.getWidget("targetiid_entry");	
	var targetcidentry = configureactionwindow.getWidget("targetcid_entry");
	
	Sash.LocationDK.action.name = nameentry.getText();
	targetIID = targetiidentry.getText();
	targetCID = targetcidentry.getText();
	
	win.hide();
}

function GetActionFileCountFromRegistration(keg) {
  var reg = "<registration>" + Sash.LocationDK.registration + "</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  Sash.LocationDK.filecount = r.hasChildNodes("jsfilename");  
}

function GetActionFileFromRegistration(keg) {
  var reg = "<registration>" + Sash.LocationDK.registration + "</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  var trav = r.getFirstChildNode("jsfilename");
  Sash.LocationDK.actionfile = trav.text;  
}
