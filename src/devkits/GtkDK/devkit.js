/*
 Uncomment this section to add documentation for your development kit.
 Sash.ExtensionDK.helpDocs is a WDEITreeHelpItem.  See help documentation
 for more information.
*/
Sash.ExtensionDK.helpDocs = Components.classes['@gnome.org/SashWDE/WDETreeHelpItem;1'].createInstance(Components.interfaces.WDEITreeItem);
Sash.ExtensionDK.helpDocs.name = 'GTK Extension';
Sash.ExtensionDK.helpDocs.associatedHTML = true;
Sash.ExtensionDK.helpDocs.fileName = Sash.ExtensionDK.dataDirectory + 'gtkdk.html';

/*
 Some helper functions for your development kit.
*/
