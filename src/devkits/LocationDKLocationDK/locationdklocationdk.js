Sash.LocationDK.helpDocs = Components.classes["@gnome.org/SashWDE/WDETreeHelpItem;1"].createInstance(Components.interfaces.WDEITreeItem);
Sash.LocationDK.helpDocs.name = "Location DK Location DK";
Sash.LocationDK.helpDocs.associatedHTML = true;
Sash.LocationDK.helpDocs.fileName = Sash.LocationDK.dataDirectory + "goat.html";

function escapeNewLine(str)
{
	var newstr = "";
	for(p in str)
		if(str[p] == '\n') newstr += "\\n";
		else newstr += str[p];
	return newstr;
}

var targetIID = "{unset}";
var targetCID = "{unset}";
var descriptionText = "";
var splashScreenFName = "";
var newactionwindow= "{unset}";
var configureactionwindow = "blah";

function ok_clicked_handler(widget)
{
	var win = newactionwindow.getWidget("window");
	var nameentry = newactionwindow.getWidget("name_entry");
	var targetiidentry = newactionwindow.getWidget("targetiid_entry");	
	var targetcidentry = newactionwindow.getWidget("targetcid_entry");
	var abstracttext = newactionwindow.getWidget("abstract_text");
	var splashscreenentry = newactionwindow.getWidget("splashscreenfilename_entry");

	win.hide();

	Sash.LocationDK.action.name = nameentry.getText();
	var dkfile = Sash.LocationDK.action.AddNewFile(0);

	dkfile.fileType = dkfile.WDETreeFileItemTypeJS;
	dkfile.fileName = Sash.LocationDK.WDE.activeProject.path + "devkit.js";

	targetIID = targetiidentry.getText();
	targetCID = targetcidentry.getText();
	descriptionText = escapeNewLine(abstracttext.text);
	splashScreenFName = splashscreenentry.getText();

	var template = "";

	template += "/*\n";
	template += " Uncomment this section to add documentation for your development kit.\n";
	template += " Sash.LocationDK.helpDocs is a WDEITreeHelpItem.  See help documentation\n";
	template += " for more information.\n";
	template += "*/\n";
	
	template += "/*\n";
	template += "Sash.LocationDK.helpDocs = Components.classes['@gnome.org/SashWDE/WDETreeHelpItem;1'].createInstance(Components.interfaces.WDEITreeItem);\n";
	template += "Sash.LocationDK.helpDocs.name = 'yourname';\n";
	template += "Sash.LocationDK.helpDocs.associatedHTML = true;\n";
	template += "Sash.LocationDK.helpDocs.fileName = Sash.LocationDK.dataDirectory + 'yourhelpdoc.html';\n";
	template += "*/\n\n";
	
	template += "/*\n";
	template += " Some helper functions for your development kit.\n";
	template += "*/\n";
	
	template += "function escapeNewLine(str)\n";
	template += "{\n";
	template += "\tvar newstr = '';\n";
	template += "\tfor(p in str)\n";
	template += "\t\tif(str[p] == '\\n') newstr += '\\\\n';\n";
	template += "\t\telse newstr += str[p];\n";
	template += "\treturn newstr;\n";
	template += "}\n\n";
	
	template += "function NewAction()\n{\n";
	template += "/*\n";
	template += " This function is called when the user makes a new action in this location.\n";
	template += " If the user cancels inside this code, you need to do\n";
	template += " Sash.LocationDK.newActionCancelled = true\n";
	template += "*/\n";
	template += "\n}\n\n";

	template += "function BuildRegistration()\n{\n";
	template += "/*\n";
	template += " This function is called before writing the registration to the wdf.\n";
	template += " It should set the registration string to something meaningful.\n";
	template += "*/\n";
	template += "//\tSash.LocationDK.registration = '';\n";
	template += "}\n\n";
	
	template += "function LoadAction()\n{\n";
	template += "/*\n";
	template += " This function is called when loading a project with an action of this\n";
	template += " type.  It should restore the internal state of the development kit\n";
	template += " and the associated action by reading the registration string.\n";
	template += "*/\n";
	template += "\n}\n\n";
	
	template += "function ConfigureAction()\n{\n";
	template += "/*\n";
	template += " This function is called when the user wants to configure settings\n";
	template += " for an action using this development kit.  It should allow you to\n";
	template += " edit any internal settings you have.\n";
	template += "*/\n";
	template += "\n}\n\n";	

	dkfile.editorDM.text = template;
}

function NewAction()
{
	newactionwindow = new Sash.Glade.GladeFile(Sash.LocationDK.dataDirectory + "ldk.glade");
}

function BuildRegistration()
{
	var reg = "<jsfilename>" + Sash.LocationDK.action.GetChild(0).name + "</jsfilename>";
	reg += "<targetIID>" + targetIID + "</targetIID>";
	reg += "<targetCID>" + targetCID + "</targetCID>";
	reg += "<description>" + descriptionText + "</description>";
	reg += "<splashscreenfilename>" + splashScreenFName + "</splashscreenfilename>";

	Sash.LocationDK.registration = reg;
}

function LoadAction()
{
	var reg = "<registration>\n" + Sash.LocationDK.registration + "\n</registration>";
	var d = new Sash.XML.Document();
	d.loadFromString(reg);
	var r = d.rootNode;
	targetIID = r.getFirstChildNode("targetIID").text;
	targetCID = r.getFirstChildNode("targetCID").text;
	descriptionText = r.getFirstChildNode("description").text;
	splashScreenFName = r.getFirstChildNode("splashscreenfilename").text;
}

function ConfigureAction()
{
	if(configureactionwindow == "blah")
		configureactionwindow = new Sash.Glade.GladeFile(Sash.LocationDK.dataDirectory + "ldk-config.glade");

	var win = configureactionwindow.getWidget("window");
	var nameentry = configureactionwindow.getWidget("name_entry");
	var targetiidentry = configureactionwindow.getWidget("targetiid_entry");	
	var targetcidentry = configureactionwindow.getWidget("targetcid_entry");
	var abstracttext = configureactionwindow.getWidget("abstract_text");
	var splashscreenentry = configureactionwindow.getWidget("splashscreenfilename_entry");
	
	nameentry.setText(Sash.LocationDK.action.name);
	targetiidentry.setText(targetIID);
	targetcidentry.setText(targetCID);
	abstracttext.text = descriptionText;
	splashscreenentry.setText(splashScreenFName);

	win.show();
}

function config_ok_clicked_handler(widget)
{
	var win = configureactionwindow.getWidget("window");
	var nameentry = configureactionwindow.getWidget("name_entry");
	var targetiidentry = configureactionwindow.getWidget("targetiid_entry");	
	var targetcidentry = configureactionwindow.getWidget("targetcid_entry");
	var abstracttext = configureactionwindow.getWidget("abstract_text");
	var splashscreenentry = configureactionwindow.getWidget("splashscreenfilename_entry");

	Sash.LocationDK.action.name = nameentry.getText();
	targetIID = targetiidentry.getText();
	targetCID = targetcidentry.getText();
	descriptionText = escapeNewLine(abstracttext.text);
	splashScreenFName = splashscreenentry.getText();

	win.hide();
}

function GetActionFileCountFromRegistration(keg) {
  var reg = "<registration>\n" + Sash.LocationDK.registration + "\n</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  Sash.LocationDK.filecount = r.hasChildNodes("jsfilename");  
}

function GetActionFileFromRegistration(keg) {
  var reg = "<registration>\n" + Sash.LocationDK.registration + "\n</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  var trav = r.getFirstChildNode("jsfilename");
  Sash.LocationDK.actionfile = trav.text;  
}
