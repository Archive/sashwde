/*
 Uncomment this section to add documentation for your development kit.
 Sash.LocationDK.helpDocs is a WDEITreeHelpItem.  See help documentation
 for more information.
*/
/*
Sash.LocationDK.helpDocs = Components.classes['@gnome.org/SashWDE/WDETreeHelpItem;1'].createInstance(Components.interfaces.WDEITreeItem);
Sash.LocationDK.helpDocs.name = 'yourname';
Sash.LocationDK.helpDocs.associatedHTML = true;
Sash.LocationDK.helpDocs.fileName = Sash.LocationDK.dataDirectory + 'yourhelpdoc.html';
*/

var js_name = "main.html";
var length = 200;
var action_name = "Panel Action";
var win = "unset"

/*
 Some helper functions for your development kit.
*/
function escapeNewLine(str)
{
	var newstr = '';
	for(p in str)
		if(str[p] == '\n') newstr += '\\n';
		else newstr += str[p];
	return newstr;
}

function PanelAppOnOKClicked(widget) {
	win.hide();

	js_name = js_entry.getText();
	length = length_entry.getValueAsInt();
	action_name = action_entry.getText();
	Sash.LocationDK.action.name = action_name;
	var dk_file = Sash.LocationDK.action.AddNewFile(0);

	dk_file.fileType = dk_file.WDETreeFileItemTypeHTML;
	dk_file.fileName = Sash.LocationDK.WDE.activeProject.path + js_name;

	var template = "";

	template += "<html><head>\n\n";
	template += "<script language=\"JavaScript\">\n\n";
	
	template += "/*\n";
	template += " * To add context menu items with one of the available stock icons, use:\n";
	template += " *   Sash.PanelApp.AddStockMenuItem(MENU_ENTRY_NAME, Sash.PanelApp.ICON_NAME_HERE, CALLBACK).\n";
	template += " * Available icons are :\n"; 
	template += " *   NewIcon, SaveIcon, SaveAsIcon, OpenIcon, CloseIcon, QuitIcon,\n";
	template += " *   CutIcon, CopyIcon, PasteIcon, PropertiesIcon, PreferencesIcon,\n";
	template += " *   StopIcon, RefreshIcon, BackIcon, ForwardIcon, HomeIcon.\n";
	template += " * To add context menu items without icons, use:\n";
	template += " *   Sash.PanelApp.AddMenuItem(MENU_ENTRY_NAME, CALLBACK)\n";
	template += " * Both AddStockMenuItem and AddMenuItem accept menu names that look like\n";
	template += " * paths, for example 'SubMenu/SomeMenuItem' which designates that the item\n";
	template += " * SomeMenuItem should appear in the sub-menu SubMenu.\n";
	template += " */\n\n";
	
	if (ck_properties.value)
		template += "Sash.PanelApp.AddStockMenuItem('Properties...', Sash.PanelApp.PropertiesIcon, OnContextProperties);\n";
	if (ck_close.value)
		template += "Sash.PanelApp.AddStockMenuItem('Close', Sash.PanelApp.CloseIcon, OnContextClose);\n";
	if (ck_help.value)
		template += "Sash.PanelApp.AddStockMenuItem('Help', Sash.PanelApp.HelpIcon, OnContextHelp);\n";
	if (ck_about.value)
		template += "Sash.PanelApp.AddStockMenuItem('About...', Sash.PanelApp.AboutIcon, OnContextAbout);\n";
		
	template += "\n/*\n";
	template += " * You can change the tooltip message at any time, just like that:\n";
	template += " * Sash.PanelApp.Tooltip = 'New tooltip text'\n";
	template += " */\n\n";
	template += "Sash.PanelApp.Tooltip = '" + tooltip_entry.getText() + "';\n";
	template += "\n";

	template += "/*\n";
	template += " * You can change the length (width) of your panel weblication like that:\n";
	template += " * Sash.PanelApp.Length = SIZE_IN_PIXELS\n";
	template += " */\n\n";
	template += "Sash.PanelApp.Length = "+ length+ ";\n";
	template += "\n";

	template += "/*\n";
	template += " * In order to handle the resizing of the Gnome Panel, you should\n";
	template += " * provide a handler for the OnSizeChange event\n";
	template += " */\n\n";
	template += "Sash.PanelApp.OnSizeChange = Resize;\n";
	template += "\n// thickness is the width of the Gnome Panel in pixels\n";
	template += "function Resize(thickness) {\n";
	template += "\t// do something useful here, like changing the page font size\n";
	template += "}\n";
	template += "\n";
	
	template += "/*\n";
	template += " * In order to handle the reorientation of the Gnome Panel, you should\n";
	template += " * provide a handler for the OnOrientationChange event\n";
	template += " */\n\n";
	template += "Sash.PanelApp.OnOrientationChange= Reorient;\n";
	template += "\n// orientation is 0 for UP, 1 for DOWN, 2 for LEFT and 3 for RIGHT\n";
	template += "function Reorient(orientation) {\n";
	template += "\t// do something useful here, like changing the page layout\n";
	template += "}\n";
	template += "\n";
	
	if (ck_properties.value) {
		template += "function OnContextProperties() {\n";
		template += "\t// your code here\n";
		template += "}\n";
		template += "\n";
	}

	if (ck_close.value) {
		template += "function OnContextClose() {\n";
		template += "\tSash.PanelApp.Quit();\n";
		template += "}\n";
		template += "\n";
	}

	if (ck_help.value) {
		template += "function OnContextHelp() {\n";
		template += "\t// your code here\n";
		template += "}\n";
		template += "\n";
	}

	if (ck_about.value) {
		template += "function OnContextAbout() {\n";
		template += "\t// your code here\n";
		template += "}\n";
		template += "\n";
	}

	template += "</script></head>\n";
	template += "<body>\n";
	template += "Your HTML here\n";
	template += "</body></html>\n";
	
	dk_file.editorDM.text = template;
}

function PanelAppOnCancelClicked(widget) {
	Sash.LocationDK.newActionCancelled = true;
	win.hide();
}

function NewAction()
{
	panel_gf = new Sash.Glade.GladeFile(Sash.LocationDK.dataDirectory+ 'panelnew.glade');
	win = panel_gf.getWidget('PanelAppDialog');
	ck_properties = panel_gf.getWidget('ck_properties');
	ck_close = panel_gf.getWidget('ck_close');
	ck_help = panel_gf.getWidget('ck_help');
	ck_about = panel_gf.getWidget('ck_about');
	js_entry = panel_gf.getWidget('js_entry');
	tooltip_entry = panel_gf.getWidget('tooltip_entry');
	length_entry = panel_gf.getWidget('length_entry');
	action_entry = panel_gf.getWidget('action_entry');
}

function BuildRegistration()
{
	var reg = "<htmlfile>" + Sash.LocationDK.action.GetChild(0).name + "</htmlfile>";
	reg += "<length>" + length.toString() + "</length>";
	Sash.LocationDK.registration = reg;
}

function LoadAction()
{
	var reg = "<registration>" + Sash.LocationDK.registration + "</registration>";
	var d = new Sash.XML.Document();
	d.loadFromString(reg);
	var r = d.rootNode;

	length = r.getFirstChildNode("length").text;
	js_name = r.getFirstChildNode("htmlfile").text;
}

function ConfigureAction()
{
}

function GetActionFileCountFromRegistration(keg) {
  var reg = "<registration>" + Sash.LocationDK.registration + "</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  Sash.LocationDK.filecount = r.hasChildNodes("htmlfile");  
}

function GetActionFileFromRegistration(keg) {
  var reg = "<registration>" + Sash.LocationDK.registration + "</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  var trav = r.getFirstChildNode("htmlfile");
  Sash.LocationDK.actionfile = trav.text;  
}
