/*
 Uncomment this section to add documentation for your development kit.
 Sash.LocationDK.helpDocs is a WDEITreeHelpItem.  See help documentation
 for more information.
*/
/*
Sash.LocationDK.helpDocs = Components.classes['@gnome.org/SashWDE/WDETreeHelpItem;1'].createInstance(Components.interfaces.WDEITreeItem);
Sash.LocationDK.helpDocs.name = 'yourname';
Sash.LocationDK.helpDocs.associatedHTML = true;
Sash.LocationDK.helpDocs.fileName = Sash.LocationDK.dataDirectory + 'yourhelpdoc.html';
*/

/*
 Some helper functions for your development kit.
*/
function escapeNewLine(str)
{
	var newstr = '';
	for(p in str)
		if(str[p] == '\n') newstr += '\\n';
		else newstr += str[p];
	return newstr;
}

function NewAction()
{
/*
 This function is called when the user makes a new action in this location.
 If the user cancels inside this code, you need to do
 Sash.LocationDK.newActionCancelled = true
*/

	Sash.LocationDK.action.name = "Simple Location";
	var dkfile = Sash.LocationDK.action.AddNewFile(0);
	dkfile.fileType = dkfile.WDETreeFileItemTypeJS;
	dkfile.fileName = Sash.LocationDK.WDE.activeProject.path + 'main.js';

	var template = "";
	template += "/*\n";
	template += " Put your javascript code here\n";
	template += "*/\n";

	dkfile.editorDM.text = template;
}

function BuildRegistration()
{
/*
 This function is called before writing the registration to the wdf.
 It should set the registration string to something meaningful.
*/

	Sash.LocationDK.registration = '<jsfilename>' + Sash.LocationDK.action.GetChild(0).name + '</jsfilename>';
}

function LoadAction()
{
/*
 This function is called when loading a project with an action of this
 type.  It should restore the internal state of the development kit
 and the associated action by reading the registration string.
*/

}

function ConfigureAction()
{
/*
 This function is called when the user wants to configure settings
 for an action using this development kit.  It should allow you to
 edit any internal settings you have.
*/

}

function GetActionFileCountFromRegistration(keg) {
  reg= "<registration>"+ Sash.LocationDK.registration+ "</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  Sash.LocationDK.filecount = r.hasChildNodes("jsfilename");  
}

function GetActionFileFromRegistration(keg) {
  reg= "<registration>"+ Sash.LocationDK.registration+ "</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  var trav = r.getFirstChildNode("jsfilename");
  Sash.LocationDK.actionfile = trav.text;  
}
