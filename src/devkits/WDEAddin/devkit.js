/*
 Uncomment this section to add documentation for your development kit.
 Sash.LocationDK.helpDocs is a WDEITreeHelpItem.  See help documentation
 for more information.
*/
/*
Sash.LocationDK.helpDocs = Components.classes['@gnome.org/SashWDE/WDETreeHelpItem;1'].createInstance(Components.interfaces.WDEITreeItem);
Sash.LocationDK.helpDocs.name = 'yourname';
Sash.LocationDK.helpDocs.associatedHTML = true;
Sash.LocationDK.helpDocs.fileName = Sash.LocationDK.dataDirectory + 'yourhelpdoc.html';
*/

/*
 Some helper functions for your development kit.
*/
function escapeNewLine(str)
{
	var newstr = '';
	for(p in str)
		if(str[p] == '\n') newstr += '\\n';
		else newstr += str[p];
	return newstr;
}

function NewAction()
{
/*
 This function is called when the user makes a new action in this location.
 If the user cancels inside this code, you need to do
 Sash.LocationDK.newActionCancelled = true
*/
	newactionwindow = new Sash.Glade.GladeFile(Sash.LocationDK.dataDirectory + "wdeaddin.glade");
	
	var dkfile = Sash.LocationDK.action.AddNewFile(0);
	dkfile.fileType = dkfile.WDETreeFileItemTypeJS;
	dkfile.fileName = Sash.LocationDK.WDE.activeProject.path + "addin.js";
}

function ok_clicked_handler(widget)
{
	var win = newactionwindow.getWidget("window");
	var nameentry = newactionwindow.getWidget("name_entry");

	win.hide();

	Sash.LocationDK.action.name = nameentry.getText();

}

function BuildRegistration()
{
/*
 This function is called before writing the registration to the wdf.
 It should set the registration string to something meaningful.
*/
//	Sash.LocationDK.registration = '';
	Sash.LocationDK.registration = Sash.LocationDK.action.GetChild(0).name;

}

function LoadAction()
{
/*
 This function is called when loading a project with an action of this
 type.  It should restore the internal state of the development kit
 and the associated action by reading the registration string.
*/

}

function ConfigureAction()
{
/*
 This function is called when the user wants to configure settings
 for an action using this development kit.  It should allow you to
 edit any internal settings you have.
*/

}

function GetActionFileCountFromRegistration(reg) {
  Sash.LocationDK.filecount = 1;
}

function GetActionFileFromRegistration(reg) {
  Sash.LocationDK.actionfile = reg;  
}


