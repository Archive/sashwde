function ConfigForm_onLoad()
{

}

function ConfigForm_onUnload()
{

}


/*
 * signal handler stub
 *
 *   widget: cancel_button
 *   signal: clicked
 *
 */
function WinAppDKOnConfCancelClicked(widget, user_data)
{
	conffrm.window.hide();
}


/*
 * signal handler stub
 *
 *   widget: ok_button
 *   signal: clicked
 *
 */
function WinAppDKOnConfOKClicked(widget, user_data)
{
	conffrm.window.hide();
	init_width= conffrm.def_width.getValueAsInt();
	init_height= conffrm.def_height.getValueAsInt();
	Sash.LocationDK.action.name = conffrm.name_entry.getText();	
}

function ConfigForm()
{
	this.gladeForm = new Sash.Glade.GladeFile('confwinapp.glade');

	this.window = this.gladeForm.getWidget('window');
	this.vbox1 = this.gladeForm.getWidget('vbox1');
	this.hbox1 = this.gladeForm.getWidget('hbox1');
	this.table1 = this.gladeForm.getWidget('table1');
	this.label4 = this.gladeForm.getWidget('label4');
	this.hbuttonbox1 = this.gladeForm.getWidget('hbuttonbox1');
	this.label1 = this.gladeForm.getWidget('label1');
	this.name_entry = this.gladeForm.getWidget('name_entry');
	this.label2 = this.gladeForm.getWidget('label2');
	this.label3 = this.gladeForm.getWidget('label3');
	this.def_width = this.gladeForm.getWidget('def_width');
	this.def_height = this.gladeForm.getWidget('def_height');
	this.ok_button = this.gladeForm.getWidget('ok_button');
	this.cancel_button = this.gladeForm.getWidget('cancel_button');

	this.onLoad();
}

ConfigForm.prototype.onLoad = ConfigForm_onLoad;
ConfigForm.prototype.onUnload = ConfigForm_onUnload;
ConfigForm.prototype.gladeForm = '';

ConfigForm.prototype.ok_button_onclicked = WinAppDKOnConfOKClicked;
ConfigForm.prototype.cancel_button_onclicked = WinAppDKOnConfCancelClicked;
