function InitForm_onLoad()
{

}

function InitForm_onUnload()
{

}


/*
 * signal handler stub
 *
 *   widget: cancel_button
 *   signal: clicked
 *
 */
function WinAppDKOnCancelClicked(widget, user_data)
{
	Sash.LocationDK.newActionCancelled = true;
	frm.window.hide();
}


/*
 * signal handler stub
 *
 *   widget: min_height
 *   signal: changed
 *
 */
function WinAppDKOnMinHeightChanged(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: min_width
 *   signal: changed
 *
 */
function WinAppDKOnMinWidthChanged(widget, user_data)
{
}


/*
 * signal handler stub
 *
 *   widget: ok_button
 *   signal: clicked
 *
 */
function WinAppDKOnOKClicked(widget, user_data)
{
	frm.window.hide();

	var template = "";

	template += "<html><head>\n\n";
	template += "<script language=\"JavaScript\">\n\n";
	
	template += "var mainWin= Sash.WindowApp.MainWindow\n\n";
	template += "// Set Window Title\n";
	template += "mainWin.TitleText= '"+ frm.title_entry.getText() + "';\n\n";
	template += "// Set Title Visibility\n";
	template += "mainWin.CaptionVisible= "+ frm.title_visible.value + ";\n\n";
	template += "// Set Initial Status Bar Text\n";
	template += "mainWin.StatusText= '"+ frm.status_entry.getText() + "';\n\n";
	template += "// Set Status Bar Visibility\n";
	template += "mainWin.StatusVisible= "+ frm.status_visible.value + ";\n\n";
	template += "// Set Minimum Width\n";
	template += "mainWin.MinWidth= "+ frm.min_width.getValueAsInt() + ";\n\n";
	template += "// Set Minimum Height\n";
	template += "mainWin.MinHeight= "+ frm.min_height.getValueAsInt() + ";\n\n";
	template += "// To Set Width\n";
	template += "// mainWin.Width= DESIRED_WIDTH;\n\n";
	template += "// To Set Height\n";
	template += "// mainWin.Height= DESIRED_HEIGHT;\n\n";

	template += "// Set Initial Window State\n";
	template += "mainWin.Maximized= "+ frm.win_size_max.value+ ";\n";
	template += "mainWin.Minimized= "+ frm.win_size_min.value+ ";\n";
	template += "mainWin.Visible= true;\n\n";

	template += "// Set Other Properties\n";
	template += "mainWin.MaximizeBox= "+ frm.maxbox_visible.value+ ";\n";
	template += "mainWin.AlwaysOnTop= "+ frm.always_on_top.value+ ";\n";
	template += "mainWin.Resizable= "+ frm.resize_check.value+ ";\n\n";

	template += "// Handler for Activate event\n";
	template += "function OnMainWindowActivate() {\n";
	template += "\t// Your code here\n";
	template += "}\n\n";
	template += "mainWin.OnActivate= OnMainWindowActivate();\n\n";

	template += "// Handler for Close event\n";
	template += "function OnMainWindowClose() {\n";
	template += "\t// Cleanup and make sure you exit\n";
	template += "\tSash.WindowApp.Quit();\n";
	template += "}\n\n";
	template += "mainWin.OnClose= OnMainWindowClose();\n\n";

	template += "// Handler for Maximize event\n";
	template += "function OnMainWindowMaximize() {\n";
	template += "\t// Your code here\n";
	template += "}\n\n";
	template += "mainWin.OnMaximize= OnMainWindowMaximize();\n\n";

	template += "// Handler for Minimize event\n";
	template += "function OnMainWindowMinimize() {\n";
	template += "\t// Your code here\n";
	template += "}\n\n";
	template += "mainWin.OnMinimize= OnMainWindowMinimize();\n\n";

  	template += "// Handler for Restore event\n";
	template += "function OnMainWindowRestore() {\n";
	template += "\t// Your code here\n";
	template += "}\n\n";
	template += "mainWin.OnRestore= OnMainWindowRestore();\n\n";

	template += "// Handler for SaveModified event\n";
	template += "function OnMainWindowSaveModified() {\n";
	template += "\t// Your code here\n";
	template += "}\n\n";
	template += "mainWin.OnSaveModified= OnMainWindowSaveModified();\n\n";

	template += "// Handler for SetForeGround event\n";
	template += "function OnMainWindowSetForeGround() {\n";
	template += "\t// Your code here\n";
	template += "}\n\n";
	template += "mainWin.OnSetForeGround= OnMainWindowSetForeGround();\n\n";

	template += "</script></head>\n";
	template += "<body>\n";
	template += "Your HTML here\n";
	template += "</body></html>\n";

	init_width= frm.def_width.getValueAsInt();
	init_height= frm.def_height.getValueAsInt();
	init_action_name= frm.action_entry.getText();
	init_html_filename= frm.html_entry.getText();

	Sash.LocationDK.action.name = init_action_name;
	var dk_file = Sash.LocationDK.action.AddNewFile(0);
	dk_file.fileType = dk_file.WDETreeFileItemTypeHTML;
	dk_file.fileName = Sash.LocationDK.WDE.activeProject.path + init_html_filename;
	dk_file.editorDM.text = template;	
}


function InitForm()
{
	this.gladeForm = new Sash.Glade.GladeFile('winappdk.glade');

	this.window = this.gladeForm.getWidget('window');
	this.vbox1 = this.gladeForm.getWidget('vbox1');
	this.vbox2 = this.gladeForm.getWidget('vbox2');
	this.hbuttonbox1 = this.gladeForm.getWidget('hbuttonbox1');
	this.table1 = this.gladeForm.getWidget('table1');
	this.table2 = this.gladeForm.getWidget('table2');
	this.vbox4 = this.gladeForm.getWidget('vbox4');
	this.label1 = this.gladeForm.getWidget('label1');
	this.action_entry = this.gladeForm.getWidget('action_entry');
	this.label2 = this.gladeForm.getWidget('label2');
	this.html_entry = this.gladeForm.getWidget('html_entry');
	this.label3 = this.gladeForm.getWidget('label3');
	this.title_entry = this.gladeForm.getWidget('title_entry');
	this.title_visible = this.gladeForm.getWidget('title_visible');
	this.label4 = this.gladeForm.getWidget('label4');
	this.status_entry = this.gladeForm.getWidget('status_entry');
	this.status_visible = this.gladeForm.getWidget('status_visible');
	this.label5 = this.gladeForm.getWidget('label5');
	this.label6 = this.gladeForm.getWidget('label6');
	this.label7 = this.gladeForm.getWidget('label7');
	this.def_height = this.gladeForm.getWidget('def_height');
	this.def_width = this.gladeForm.getWidget('def_width');
	this.min_width = this.gladeForm.getWidget('min_width');
	this.min_height = this.gladeForm.getWidget('min_height');
	this.label8 = this.gladeForm.getWidget('label8');
	this.label9 = this.gladeForm.getWidget('label9');
	this.win_size_normal = this.gladeForm.getWidget('win_size_normal');
	this.win_size_max = this.gladeForm.getWidget('win_size_max');
	this.win_size_min = this.gladeForm.getWidget('win_size_min');
	this.maxbox_visible = this.gladeForm.getWidget('maxbox_visible');
	this.always_on_top = this.gladeForm.getWidget('always_on_top');
	this.resize_check = this.gladeForm.getWidget('resize_check');
	this.ok_button = this.gladeForm.getWidget('ok_button');
	this.cancel_button = this.gladeForm.getWidget('cancel_button');

	this.onLoad();
}

InitForm.prototype.onLoad = InitForm_onLoad;
InitForm.prototype.onUnload = InitForm_onUnload;
InitForm.prototype.gladeForm = '';

InitForm.prototype.min_width_onchanged = WinAppDKOnMinWidthChanged;
InitForm.prototype.min_height_onchanged = WinAppDKOnMinHeightChanged;
InitForm.prototype.ok_button_onclicked = WinAppDKOnOKClicked;
InitForm.prototype.cancel_button_onclicked = WinAppDKOnCancelClicked;
