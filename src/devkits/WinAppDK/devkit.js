/*
 Uncomment this section to add documentation for your development kit.
 Sash.LocationDK.helpDocs is a WDEITreeHelpItem.  See help documentation
 for more information.
*/

Sash.LocationDK.helpDocs = Components.classes['@gnome.org/SashWDE/WDETreeHelpItem;1'].createInstance(Components.interfaces.WDEITreeItem);
Sash.LocationDK.helpDocs.name = 'WindowApp';
Sash.LocationDK.helpDocs.associatedHTML = true;
Sash.LocationDK.helpDocs.fileName = Sash.LocationDK.dataDirectory + 'winappdkhelp.html';


/*
 Some helper functions for your development kit.
*/

var frm= "{unset}";
var conffrm= "{unset}";
var init_width= 800;
var init__height= 600;
var init_html_filename= "main.html";
var init_action_name= "WinApp Weblication";

load('InitForm.js');
load('ConfigForm.js');

function NewAction() {
	frm= new InitForm();
}

function BuildRegistration() {
	var reg = "<htmlfile>" + Sash.LocationDK.action.GetChild(0).name + "</htmlfile>";
	reg += "<width>" + init_width + "</width>";
	reg += "<height>" + init_height + "</height>";
	Sash.LocationDK.registration = reg;
}

function LoadAction() {
	var reg = "<registration>\n" + Sash.LocationDK.registration + "\n</registration>";
	var d = new Sash.XML.Document();
	d.loadFromString(reg);
	var r = d.rootNode;

	init_width = r.getFirstChildNode("width").text;
	init_height = r.getFirstChildNode("height").text;
	init_html_name = r.getFirstChildNode("htmlfile").text;
}

function ConfigureAction() {
	if (conffrm== "{unset}")
		conffrm= new ConfigForm();
	conffrm.window.show();
	conffrm.name_entry.setText(Sash.LocationDK.action.name);
	conffrm.def_width.setValue(init_width);
	conffrm.def_height.setValue(init_height);
}

function GetActionFileCountFromRegistration(keg) {
  var reg = "<registration>\n" + Sash.LocationDK.registration + "\n</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  Sash.LocationDK.filecount = r.hasChildNodes("htmlfile");  
}

function GetActionFileFromRegistration(keg) {
  var reg = "<registration>\n" + Sash.LocationDK.registration + "\n</registration>";
  var d = new Sash.XML.Document();
  d.loadFromString(reg);
  var r = d.rootNode;
  var trav = r.getFirstChildNode("htmlfile");
  Sash.LocationDK.actionfile = trav.text;  
}
