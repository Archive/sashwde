/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/

#include "WDEEvent.h"
#include "WDEComMacros.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS1_CI(WDEEvent, WDEIEvent)

WDEEvent::WDEEvent() {
  m_id= 0;
  m_intData= 0;
  m_strData= "";
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEEvent::~WDEEvent()
{
  /* destructor code */
}

/* attribute PRInt32 ID; */
NS_IMETHODIMP WDEEvent::GetID(PRInt32 *aID)
{
  XPCOM_SIMPLE_GETTER(m_id, aID);
}
NS_IMETHODIMP WDEEvent::SetID(PRInt32 aID)
{
  XPCOM_SIMPLE_SETTER(m_id, aID);
}

/* attribute WDEIEventSender sender; */
NS_IMETHODIMP WDEEvent::GetSender(WDEIEventSender * *aSender)
{
  XPCOM_COMPTR_GETTER(m_sender, aSender);
}
NS_IMETHODIMP WDEEvent::SetSender(WDEIEventSender * aSender)
{
  XPCOM_SIMPLE_SETTER(m_sender, aSender);
}

/* attribute string strData; */
NS_IMETHODIMP WDEEvent::GetStrData(char * *aStrData)
{
  XPCOM_STR_GETTER(m_strData.c_str(), aStrData);
}
NS_IMETHODIMP WDEEvent::SetStrData(const char * aStrData)
{
  XPCOM_SIMPLE_SETTER(m_strData, aStrData);
}

/* attribute PRInt32 intData; */
NS_IMETHODIMP WDEEvent::GetIntData(PRInt32 *aIntData)
{
  XPCOM_SIMPLE_GETTER(m_intData, aIntData);
}
NS_IMETHODIMP WDEEvent::SetIntData(PRInt32 aIntData)
{
  XPCOM_SIMPLE_SETTER(m_intData, aIntData);
}

/* attribute nsISupports ptrData; */
NS_IMETHODIMP WDEEvent::GetPtrData(nsISupports * *aPtrData)
{
  XPCOM_COMPTR_GETTER(m_ptrData, aPtrData);
}

NS_IMETHODIMP WDEEvent::SetPtrData(nsISupports * aPtrData)
{
  XPCOM_SIMPLE_SETTER(m_ptrData, aPtrData);
}

