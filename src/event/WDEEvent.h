/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/


#ifndef _WDEEVENT_H
#define _WDEEVENT_H

#include "WDEIEvent.h"
#include "nsCOMPtr.h"
#include <string>

#include "WDEComMacros.h"

#include "WDEIEventSender.h"

//A6E6A804-7DBD-41BF-A981-80B714D61670
#define WDEEVENT_CID {0xA6E6A804, 0x7DBD, 0x41BF, {0xA9, 0x81, 0x80, 0xB7, 0x14, 0xD6, 0x16, 0x70}}
NS_DEFINE_CID(kWDEEventCID, WDEEVENT_CID);

#define WDEEVENT_CONTRACT_ID "@gnome.org/SashWDE/WDEEvent;1"


class WDEEvent : public WDEIEvent
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIEVENT

  WDEEvent();
  virtual ~WDEEvent();
  /* additional members */

private:
	int m_id;
	int m_intData;
	string m_strData;
	nsCOMPtr<WDEIEventSender> m_sender;
	nsCOMPtr<nsISupports> m_ptrData;	
};

#endif
