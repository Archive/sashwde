#include "WDEEventListener.h"
#include <algorithm>
#include "WDEComMacros.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS1_CI(WDEEventListener, WDEIEventListener)

WDEEventListener::WDEEventListener() {
  NS_INIT_ISUPPORTS();
  m_handler= 0;
  m_thisP= this;
}

WDEEventListener::~WDEEventListener() {
  GoDeaf();
}

/* attribute WDEIEventHandler handler; */
NS_IMETHODIMP WDEEventListener::GetHandler(WDEIEventHandler * *aHandler) {
  XPCOM_COMPTR_GETTER(m_handler, aHandler);
}

NS_IMETHODIMP WDEEventListener::SetHandler(WDEIEventHandler * aHandler) {
  XPCOM_COMPTR_SETTER(m_handler, aHandler);
}

NS_IMETHODIMP WDEEventListener::AddSender(WDEIEventSender *sender) {
  nsCOMPtr<WDEIEventSender> _sender(sender);
  if (find(pm_senders.begin(), pm_senders.end(), _sender)== pm_senders.end())
    pm_senders.push_back(_sender);
  return NS_OK;
}

NS_IMETHODIMP WDEEventListener::RemoveSender(WDEIEventSender *sender) {
  nsCOMPtr<WDEIEventSender> _sender(sender);
  vector<WDEIEventSender *>::iterator i= find(pm_senders.begin(), pm_senders.end(), _sender);
  if (i!= pm_senders.end()) pm_senders.erase(i);
  return NS_OK;
}

NS_IMETHODIMP WDEEventListener::ListenTo(WDEIEventSender *sender) {
  nsCOMPtr<WDEIEventSender> _sender(sender);
  nsCOMPtr<WDEIEventListener> _this(m_thisP);
  AddSender(_sender);
  _sender->AddListener(_this);
  return NS_OK;
}

NS_IMETHODIMP WDEEventListener::DontListenTo(WDEIEventSender *sender) {
  nsCOMPtr<WDEIEventSender> _sender(sender);
  nsCOMPtr<WDEIEventListener> _this(m_thisP);
  RemoveSender(_sender);
  _sender->RemoveListener(_this);
  return NS_OK;
}

NS_IMETHODIMP WDEEventListener::GoDeaf() {
  nsCOMPtr<WDEIEventListener> _this(m_thisP);
  vector<WDEIEventSender * >::iterator i;
  for (i= pm_senders.begin(); i< pm_senders.end(); ++i)
    (*i)->RemoveListener(_this);
  pm_senders.clear();
  return NS_OK;
}

NS_IMETHODIMP WDEEventListener::ProxyThis(WDEIEventListener *proxy) {
  m_thisP= proxy;
  return NS_OK;
}
