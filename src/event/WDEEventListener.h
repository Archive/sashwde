#ifndef WDEEVENTLISTENER_H
#define WDEEVENTLISTENER_H

#include "WDEIEventListener.h"
#include "WDEIEventSender.h"
#include <vector>

#include "nscore.h"
#include "nsCOMPtr.h"
#include "nsIFactory.h"
#include "nsIFile.h"
#include "nsComponentManagerUtils.h"
#include "nsIServiceManager.h"
#include "nsIGenericFactory.h"
#include "xpcomtools.h"
#include "WDEComMacros.h"

#define WDEEVENTLISTENER_CID {0x821e4f75, 0xd2af, 0x4f85, {0xb4, 0xc8, 0xdf, 0x72, 0x6b, 0xb4, 0x7b, 0xd7}}
NS_DEFINE_CID(kWDEEventListenerCID, WDEEVENTLISTENER_CID);

#define WDEEVENTLISTENER_CONTRACT_ID "@gnome.org/SashWDE/WDEEventListener;1"

class WDEEventListener: public WDEIEventListener {

  public:
    NS_DECL_ISUPPORTS
    NS_DECL_WDEIEVENTLISTENER

    WDEEventListener();
    virtual ~WDEEventListener();
    
  private:
    nsCOMPtr<WDEIEventHandler> m_handler;
    vector<WDEIEventSender * > pm_senders;
    WDEIEventListener *m_thisP;
};

#endif
