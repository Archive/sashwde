#include "nsIGenericFactory.h"

#include "WDEEvent.h"
#include "WDEEventListener.h"
#include "WDEEventSender.h"
#include "WDEJSEventAdapter.h"

NS_DECL_CLASSINFO(WDEEvent)
NS_DECL_CLASSINFO(WDEEventListener)
NS_DECL_CLASSINFO(WDEEventSender)
NS_DECL_CLASSINFO(WDEJSEventAdapter)

NS_GENERIC_FACTORY_CONSTRUCTOR(WDEEvent)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEEventListener)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEEventSender)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEJSEventAdapter)

static nsModuleComponentInfo components[] = {
  {
	  "WDEEvent",
	  WDEEVENT_CID,
	  WDEEVENT_CONTRACT_ID,
	  WDEEventConstructor
  },
  {
	  "WDEEventListener",
	  WDEEVENTLISTENER_CID,
	  WDEEVENTLISTENER_CONTRACT_ID,
	  WDEEventListenerConstructor
  },
  {
	  "WDEEventSender",
	  WDEEVENTSENDER_CID,
	  WDEEVENTSENDER_CONTRACT_ID,
	  WDEEventSenderConstructor
  },
  {
	  "WDEJSEventAdapter",
	  WDEJSEVENTADAPTER_CID,
	  WDEJSEVENTADAPTER_CONTRACT_ID,
	  WDEJSEventAdapterConstructor
  }
};
 	
NS_IMPL_NSGETMODULE("WDEEvent module", components)
