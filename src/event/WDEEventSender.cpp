#include "WDEEventSender.h"
#include "WDEEvent.h"
#include <algorithm>
#include "WDEComMacros.h"
#include "xpcomtools.h"
#include "prenv.h"
#include "nsCOMPtr.h"
#include "nsComponentManagerUtils.h"


NS_IMPL_ISUPPORTS1_CI(WDEEventSender, WDEIEventSender)

WDEEventSender::WDEEventSender() {
  NS_INIT_ISUPPORTS();
  m_thisP= this;
}

WDEEventSender::~WDEEventSender() {
  GoMute();
}

NS_IMETHODIMP WDEEventSender::CreateEvent(PRInt32 event, const char *str, PRInt32 attr, nsISupports *user, WDEIEvent **_retval) {
  nsCOMPtr<WDEIEvent> result= do_CreateInstance(WDEEVENT_CONTRACT_ID);
  nsCOMPtr<WDEIEventSender> _this(m_thisP);
  result->SetID(event);
  result->SetSender(_this);
  result->SetStrData(str);
  result->SetIntData(attr);
  result->SetPtrData(user);
  XPCOM_COMPTR_GETTER(result, _retval);
}

NS_IMETHODIMP WDEEventSender::BroadcastEventObject(WDEIEvent *event) {
  nsCOMPtr<WDEIEvent> eventObj(event);
  vector<WDEIEventListener* > temp(pm_listeners.begin(), pm_listeners.end());
  vector<WDEIEventListener* >::iterator i;
  for (i= temp.begin(); i< temp.end(); ++i) {
    nsCOMPtr<WDEIEventHandler> evh= 0;
    if (NS_SUCCEEDED((*i)->GetHandler(getter_AddRefs(evh))))
      if (evh)
        evh->HandleEvent(eventObj);
  }
  temp.clear();
  return NS_OK;
}

NS_IMETHODIMP WDEEventSender::BroadcastEvent(PRInt32 event, const char *str, PRInt32 attr, nsISupports *user) {
  nsCOMPtr<WDEIEvent> eventObj= NULL;
  CreateEvent(event, str, attr, user, getter_AddRefs(eventObj));
  BroadcastEventObject(eventObj);
  return NS_OK;
}

NS_IMETHODIMP WDEEventSender::AddListener(WDEIEventListener *listener) {
  nsCOMPtr<WDEIEventListener> _listener(listener);
  nsCOMPtr<WDEIEventSender> _this(m_thisP);
  if (find(pm_listeners.begin(), pm_listeners.end(), _listener)== pm_listeners.end())
    pm_listeners.push_back(_listener);
  return NS_OK;
}

NS_IMETHODIMP WDEEventSender::RemoveListener(WDEIEventListener *listener) {
  nsCOMPtr<WDEIEventListener> _listener(listener);
  nsCOMPtr<WDEIEventSender> _this(m_thisP);
  vector<WDEIEventListener* >::iterator i= find(pm_listeners.begin(), pm_listeners.end(), _listener);
  if (i!= pm_listeners.end()) pm_listeners.erase(i);
  return NS_OK;
}

NS_IMETHODIMP WDEEventSender::SendTo(WDEIEventListener *listener) {
  nsCOMPtr<WDEIEventListener> _listener(listener);
  nsCOMPtr<WDEIEventSender> _this(m_thisP);
  AddListener(_listener);
  _listener->AddSender(_this);
  return NS_OK;
}

NS_IMETHODIMP WDEEventSender::DontSendTo(WDEIEventListener *listener) {
  nsCOMPtr<WDEIEventListener> _listener(listener);
  nsCOMPtr<WDEIEventSender> _this(m_thisP);
  RemoveListener(_listener);
  _listener->RemoveSender(_this);
  return NS_OK;
}

NS_IMETHODIMP WDEEventSender::GoMute() {
  nsCOMPtr<WDEIEventSender> _this(m_thisP);
  vector<WDEIEventListener *>::iterator i;
  for (i= pm_listeners.begin(); i< pm_listeners.end(); ++i) {
    (*i)->RemoveSender(_this);
  }
  pm_listeners.clear();
  return NS_OK;
}

NS_IMETHODIMP WDEEventSender::ProxyThis(WDEIEventSender *proxy) {
  m_thisP= proxy;
  return NS_OK;
}
