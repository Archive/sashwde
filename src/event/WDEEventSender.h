#ifndef WDEEVENTSENDER_H
#define WDEEVENTSENDER_H

#include "WDEIEventSender.h"
#include "WDEIEventListener.h"
#include <vector>
#include "nsCOMPtr.h"

#define WDEEVENTSENDER_CID {0x7475df65, 0x133f, 0x495b, {0x88, 0x45, 0x6e, 0x5a, 0xc7, 0xe5, 0xbc, 0x33}}
NS_DEFINE_CID(kWDEEventSenderCID, WDEEVENTSENDER_CID);

#define WDEEVENTSENDER_CONTRACT_ID "@gnome.org/SashWDE/WDEEventSender;1"

class WDEEventSender: public WDEIEventSender {

  public:

    NS_DECL_ISUPPORTS
    NS_DECL_WDEIEVENTSENDER

    WDEEventSender();
    virtual ~WDEEventSender();
    
  private:
    
    vector<WDEIEventListener *> pm_listeners;
    WDEIEventSender *m_thisP;
};

#endif
