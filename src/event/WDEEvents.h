#ifndef WDE_EVENTS_H_SYSTEM
#define WDE_EVENTS_H_SYSTEM

#include "WDEEvent.h"
#include "WDEEventListener.h"
#include "WDEEventSender.h"
#include "nsMemory.h"
#include <string>
#include "nsCOMPtr.h"

#define WDE_DECL_LISTENER \
private: nsCOMPtr<WDEIEventListener> __m__LISTENER_forwarder; \
public: NS_FORWARD_WDEIEVENTLISTENER(__m__LISTENER_forwarder->) \
public: NS_DECL_WDEIEVENTHANDLER

#define WDE_DECL_SENDER \
private: nsCOMPtr<WDEIEventSender> __m__SENDER_forwarder; \
public: NS_FORWARD_WDEIEVENTSENDER(__m__SENDER_forwarder->)

#define WDE_DECL_SENDER_LISTENER \
WDE_DECL_LISTENER \
WDE_DECL_SENDER
// synonym
#define WDE_DECL_LISTENER_SENDER \
WDE_DECL_SENDER_LISTENER

#define WDE_INIT_LISTENER \
__m__LISTENER_forwarder= do_CreateInstance(WDEEVENTLISTENER_CONTRACT_ID); \
__m__LISTENER_forwarder->SetHandler(this); \
__m__LISTENER_forwarder->ProxyThis(this);

#define WDE_INIT_SENDER \
__m__SENDER_forwarder= do_CreateInstance(WDEEVENTSENDER_CONTRACT_ID); \
__m__SENDER_forwarder->ProxyThis(this);

#define WDE_INIT_SENDER_LISTENER \
WDE_INIT_LISTENER \
WDE_INIT_SENDER
// synonym
#define WDE_INIT_LISTENER_SENDER \
WDE_INIT_SENDER_LISTENER

#define WDE_EVENT_TO_LOCAL_VARS(evObj, evCode, evStr, evAttr, evPtr, evSender) \
PRInt32 evCode= 0; \
string evStr= ""; \
PRInt32 evAttr= 0; \
nsISupports *evPtr= 0; \
WDEIEventSender *evSender= 0; \
{ /* scope */ \
nsCOMPtr<WDEIEvent> __tmp__= do_QueryInterface(evObj); \
__tmp__->GetSender(&evSender); \
__tmp__->GetID(&evCode); \
__tmp__->GetIntData(&evAttr); \
char *__tmp_str__; \
__tmp__->GetStrData(&__tmp_str__); \
if (__tmp_str__) { evStr= __tmp_str__; nsMemory::Free(__tmp_str__); } \
__tmp__->GetPtrData(&evPtr); \
}

#endif
