#include "WDEJSEventAdapter.h"
#include <algorithm>
#include "sashVariantUtils.h"

NS_IMPL_ISUPPORTS3_CI(WDEJSEventAdapter, WDEIJSEventAdapter, WDEIEventListener, WDEIEventHandler)

JSProxyInfo::JSProxyInfo() {
  idFilter.clear();
}

JSProxyInfo::JSProxyInfo(const JSProxyInfo &other) {
  idFilter= other.idFilter;
}

WDEJSEventAdapter::WDEJSEventAdapter() : m_act(0), m_js(0) {
  NS_INIT_ISUPPORTS();
  WDE_INIT_LISTENER
}

WDEJSEventAdapter::~WDEJSEventAdapter() {
}

/** EventHandler **/
NS_IMETHODIMP WDEJSEventAdapter::HandleEvent(WDEIEvent *eventObj) {
  cerr << "JSAdapter Received Event! " << eventObj << endl;
  WDE_EVENT_TO_LOCAL_VARS(eventObj, id, dumm1, dumm2, dumm3, sender);
  cerr << id << " " << dumm1 << " " << dumm2 << " " << dumm3 << endl;
  if (!sender)
    return NS_OK;
  cerr << "--Sender:" << sender << endl;
  map<WDEIEventSender * , JSProxyInfo>::iterator i;
  i= m_tab.find(sender);
  if (i== m_tab.end()) {
    cerr << "--Sender not found!" << endl;
    return NS_OK;
  }
  map<int, string>::iterator j= i->second.idFilter.find(id);
  if (j== i->second.idFilter.end()) {
    cerr << "--Event ID " << id << " not found!" << endl;
    return NS_OK;
  }
  string handler= j->second; // got function name here
  cerr << "--Will try to invoke " << handler << endl;
  JSContext *cx;
  m_js->GetScriptContext(&cx);
  nsISupports *ret;
  std::vector<nsIVariant *> args(1);
  NewVariant(&(args[0]));
  eventObj->QueryInterface(NS_GET_IID(nsISupports), (void **) &ret);
	VariantSetInterface(args[0], ret);
  CallEvent(m_act, cx, handler, args);
  return NS_OK;
}

/* void addJSProxy (in WDEIEventSender sender, in long eventID, in string jsHandlerName); */
NS_IMETHODIMP WDEJSEventAdapter::AddJSProxy(WDEIEventSender *sender, PRInt32 eventID, const char *jsHandlerName) {
  map<WDEIEventSender *, JSProxyInfo>::iterator i;
  JSProxyInfo jspi;
  string handler= jsHandlerName;
  cerr << "Proxying ID " << eventID << " for sender " << sender << endl;
  i= m_tab.find(sender);
  if (i== m_tab.end()) { // new object
    if (!handler.empty()) {
      jspi.idFilter[eventID]= handler;
      m_tab[sender]= jspi;
      ListenTo(sender);
    }
  }
  else { // already listening to sender
    if (!handler.empty())
      i->second.idFilter[eventID]= handler;
    else { // remove the dude
      map<int, string>::iterator j= i->second.idFilter.find(eventID);
      if (j!= i->second.idFilter.end())
        i->second.idFilter.erase(j);
      if (i->second.idFilter.empty()) { // stop listening to sender
        m_tab.erase(i);
        DontListenTo(sender);
      }
    }
  }    
  return NS_OK;
}

/* void removeJSProxy (in WDEIEventSender sender, in long eventID); */
NS_IMETHODIMP WDEJSEventAdapter::RemoveJSProxy(WDEIEventSender *sender, PRInt32 eventID) {
  return AddJSProxy(sender, eventID, "");
}

/* string getJSProxy (in WDEIEventSender sender, in long eventID); */
NS_IMETHODIMP WDEJSEventAdapter::GetJSProxy(WDEIEventSender *sender, PRInt32 eventID, char **_retval) {
  string handler;
  map<WDEIEventSender * , JSProxyInfo>::iterator i;
  i= m_tab.find(sender);
  if (i!= m_tab.end()) {
    map<int, string>::iterator j= i->second.idFilter.find(eventID);
    if (j!= i->second.idFilter.end())
      handler= j->second;
  }
  XP_RETURN_STRING(handler, _retval);
}

/* void removeJSAllProxys (); */
NS_IMETHODIMP WDEJSEventAdapter::RemoveJSAllProxys() {
  GoDeaf(); // don't listen at all
  m_tab.clear();
  return NS_OK;
}

/* void setEventContext (in sashIJSEmbed jsEmbed, in sashIActionRuntime act); */
NS_IMETHODIMP WDEJSEventAdapter::SetEventContext(sashIJSEmbed *jsEmbed, sashIActionRuntime *act) {
  // non-owning references
  m_js= jsEmbed;
  m_act= act;
  return NS_OK;
}
