#ifndef _WDEJSEVENTADAPTER_H
#define _WDEJSEVENTADAPTER_H

#include "nsCOMPtr.h"
#include "WDEEvents.h"
#include "sashIActionRuntime.h"
#include "sashIJSEmbed.h"
#include "WDEIJSEventAdapter.h"
#include <map>
#include <string>

//93f732c0-c678-40a0-8b5f-454d18367e5c

#define WDEJSEVENTADAPTER_CID {0x93f732c0, 0xc678, 0x40a0, {0x8b, 0x5f, 0x45, 0x4d, 0x18, 0x36, 0x7e, 0x5c}}
NS_DEFINE_CID(kWDEJSEventAdapterCID, WDEJSEVENTADAPTER_CID);

#define WDEJSEVENTADAPTER_CONTRACT_ID "@gnome.org/SashWDE/WDEJSEventAdapter;1"

class JSProxyInfo {

  public:

    JSProxyInfo();
    JSProxyInfo(const JSProxyInfo &other);
    map<int, string> idFilter;
};

class WDEJSEventAdapter: public WDEIJSEventAdapter,
                         public WDEIEventListener,
                         public WDEIEventHandler {

  public:
    NS_DECL_ISUPPORTS
    NS_DECL_WDEIJSEVENTADAPTER

    WDE_DECL_LISTENER

    WDEJSEventAdapter();
    virtual ~WDEJSEventAdapter();
    
  private:
  
    sashIActionRuntime *m_act;
    sashIJSEmbed *m_js;
    map<WDEIEventSender * , JSProxyInfo> m_tab;

};

#endif
