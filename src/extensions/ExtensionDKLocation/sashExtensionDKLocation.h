
/***************************************************************
    SashWDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _SASHEXTENSIONDKLOCATION_H
#define _SASHEXTENSIONDKLOCATION_H

#include "sashWDEAddin.h"
#include "sashIExtensionDKLocation.h"

//6A3F620B-0F8E-415F-AC12-E083CCCD5A85
#define SASHEXTENSIONDKLOCATION_CID {0x6A3F620B, 0x0F8E, 0x415F, {0xAC, 0x12, 0xE0, 0x83, 0xCC, 0xCD, 0x5A, 0x85}}
NS_DEFINE_CID(ksashExtensionDKLocationCID, SASHEXTENSIONDKLOCATION_CID);

#define SASHEXTENSIONDKLOCATION_CONTRACT_ID "@gnome.org/SashMo/sashExtensionDKLocation;1"


#define SASHEXTENSIONDKLOCATION_PRIVATEMEMBERS  \
	string m_targetIID; \
	string m_syntaxList; \
	string m_targetCID;
	
#define SASHEXTENSIONDKLOCATION_CONSTRUCTOR	 \
  m_targetIID = ""; \
	m_syntaxList = ""; \
	m_targetCID = "";

#define SASHEXTENSIONDKLOCATION_DESTRUCTOR

#define SASHEXTENSIONDKLOCATION_FUNCTIONS(sashExtensionDKLocation) \
/* attribute string TargetInterfaceID; */ \
NS_IMETHODIMP sashExtensionDKLocation ::GetTargetInterfaceID(char * *aTargetInterfaceID) \
{ \
	XP_RETURN_STRING(m_targetIID.c_str(), aTargetInterfaceID); \
  return NS_OK; \
} \
NS_IMETHODIMP sashExtensionDKLocation ::SetTargetInterfaceID(const char * aTargetInterfaceID) \
{ \
  m_targetIID = aTargetInterfaceID; \
  return NS_OK; \
} \
\
/* attribute string TargetCID; */ \
NS_IMETHODIMP sashExtensionDKLocation ::GetTargetCID(char * *aTargetCID) \
{ \
	XP_RETURN_STRING(m_targetCID.c_str(), aTargetCID); \
  return NS_OK; \
} \
NS_IMETHODIMP sashExtensionDKLocation ::SetTargetCID(const char * aTargetCID) \
{ \
  m_targetCID = aTargetCID; \
  return NS_OK; \
} \
\
/* attribute string syntaxCompletionList; */ \
NS_IMETHODIMP sashExtensionDKLocation::GetSyntaxCompletionList(char * *aSyntaxCompletionList) \
{ \
	XP_RETURN_STRING(m_syntaxList.c_str(), aSyntaxCompletionList); \
} \
NS_IMETHODIMP sashExtensionDKLocation::SetSyntaxCompletionList(const char * aSyntaxCompletionList) \
{ \
	m_syntaxList = aSyntaxCompletionList; \
	return NS_OK; \
}


class sashExtensionDKLocation : public sashIExtensionDKLocation
{ 
public:
  NS_DECL_ISUPPORTS
  NS_DECL_SASHIUNKNOWN
  NS_DECL_SASHIEXTENSION
  NS_DECL_SASHILOCATION
	NS_DECL_SASHIWDEADDIN
  NS_DECL_SASHIEXTENSIONDKLOCATION

  sashExtensionDKLocation();
  virtual ~sashExtensionDKLocation();
  /* additional members */
	
private:
	SASHWDEADDIN_PRIVATEMEMBERS
	SASHEXTENSIONDKLOCATION_PRIVATEMEMBERS	
};

#endif
