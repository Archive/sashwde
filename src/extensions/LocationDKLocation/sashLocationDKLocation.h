
/***************************************************************
    SashWDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _SASHLOCATIONDKLOCATION_H
#define _SASHLOCATIONDKLOCATION_H

#include "sashILocationDKLocation.h"
#include "sashExtensionDKLocation.h"

//47CF1082-0C12-4072-9EC0-0536C5C83A11
#define SASHLOCATIONDKLOCATION_CID {0x47CF1082, 0x0C12, 0x4072, {0x9E, 0xC0, 0x05, 0x36, 0xC5, 0xC8, 0x3A, 0x11}}
NS_DEFINE_CID(ksashLocationDKLocationCID, SASHLOCATIONDKLOCATION_CID);

#define SASHLOCATIONDKLOCATION_CONTRACT_ID "@gnome.org/SashMo/sashLocationDKLocation;1"

#define SASHLOCATIONDKLOCATION_PRIVATEMEMBERS \
	string m_abstract; \
	string m_splashScreenFName; \
	string m_registration; \
	WDEIAction *m_action; \
	bool m_newActionCancelled; \
	string m_actionfile; \
  PRInt32 m_filecount;
	
#define SASHLOCATIONDKLOCATION_CONSTRUCTOR \
	m_splashScreenFName = ""; \
	m_abstract = ""; \
	m_registration = ""; \
	m_action = NULL; \
	m_newActionCancelled = false; \
	m_actionfile= ""; \
	m_filecount= 0;

#define SASHLOCATIONDKLOCATION_DESTRUCTOR \
	NS_IF_RELEASE(m_action);

#define SASHLOCATIONDKLOCATION_FUNCTIONS(sashLocationDKLocation) \
/* attribute string Abstract; */ \
NS_IMETHODIMP sashLocationDKLocation ::GetDescription(char * *aAbstract) \
{ \
	XP_RETURN_STRING(m_abstract.c_str(), aAbstract); \
	return NS_OK; \
} \
NS_IMETHODIMP sashLocationDKLocation ::SetDescription(const char * aAbstract) \
{ \
	m_abstract = aAbstract; \
	return NS_OK; \
} \
\
/* attribute string SplashScreenFileName; */ \
NS_IMETHODIMP sashLocationDKLocation ::GetSplashScreenFileName(char * *aSplashScreenFileName) \
{ \
	XP_RETURN_STRING(m_splashScreenFName.c_str(), aSplashScreenFileName); \
	return NS_OK; \
} \
NS_IMETHODIMP sashLocationDKLocation ::SetSplashScreenFileName(const char * aSplashScreenFileName) \
{ \
	m_splashScreenFName = aSplashScreenFileName; \
	return NS_OK; \
} \
\
/* void NewActionInitialization (in WDEIAction action); */ \
NS_IMETHODIMP sashLocationDKLocation::NewActionInitialization(WDEIAction *action) \
{ \
	m_action = action; \
	string scr = "NewAction();"; \
	m_js->EvalScript(scr.c_str()); \
	return NS_OK; \
}\
\
/* void GenerateRegistrationString (); */ \
NS_IMETHODIMP sashLocationDKLocation::GenerateRegistrationString() \
{ \
	string scr = "BuildRegistration();"; \
	m_js->EvalScript(scr.c_str()); \
	return NS_OK; \
} \
\
/* readonly attribute long actionFileCount */ \
NS_IMETHODIMP sashLocationDKLocation::GetActionFileCount(PRInt32 *_retval) \
{ \
  m_filecount= 0; \
	string scr= "GetActionFileCountFromRegistration('');"; \
	m_js->EvalScript(scr.c_str()); /* this will set m_filecount */ \
	*_retval= m_filecount; \
	return NS_OK; \
} \
\
/* void getActionFile(index) */ \
NS_IMETHODIMP sashLocationDKLocation::GetActionFile(const PRInt32 index, char **_retval) \
{ \
  m_actionfile= ""; \
  m_filecount= index; /* double action stuff, currently not used */\
	string scr= "GetActionFileFromRegistration('');"; \
	m_js->EvalScript(scr.c_str()); /* this will set m_actionfile */ \
  XP_RETURN_STRING(m_actionfile.c_str(), _retval); \
} \
\
/* attribute string registration; */ \
NS_IMETHODIMP sashLocationDKLocation::GetRegistration(char * *aRegistration) \
{ \
	XP_RETURN_STRING(m_registration.c_str(), aRegistration); \
} \
NS_IMETHODIMP sashLocationDKLocation::SetRegistration(const char * aRegistration) \
{ \
	m_registration = aRegistration; \
  if (g_strncasecmp(m_registration.c_str(), "<registration>", 14)== 0) \
	  m_registration= m_registration.substr(14, m_registration.length()- 29); \
	while (m_registration[0]== ' ' || m_registration[0]== '\n' || m_registration[0]== '\t') \
	  m_registration.erase(0, 1); \
	while (m_registration[m_registration.length()- 1]== ' ' || m_registration[m_registration.length()- 1]== '\n' || m_registration[m_registration.length()- 1]== '\t') \
	  m_registration.erase(m_registration.length()- 1); \
	return NS_OK; \
} \
\
/* readonly attribute WDEIAction action; */ \
NS_IMETHODIMP sashLocationDKLocation::GetAction(WDEIAction * *aAction) \
{ \
	*aAction = m_action; \
	NS_IF_ADDREF(m_action); \
	return NS_OK; \
} \
\
/* void LoadActionInitialization (in WDEIAction action, in string registration); */ \
NS_IMETHODIMP sashLocationDKLocation::LoadActionInitialization(WDEIAction *action, const char *registration) \
{ \
	m_action = action; \
	m_registration = registration; \
	string scr = "LoadAction();"; \
	m_js->EvalScript(scr.c_str()); \
	return NS_OK; \
} \
\
/* attribute boolean newActionCancelled; */ \
NS_IMETHODIMP sashLocationDKLocation::GetNewActionCancelled(PRBool *aNewActionCancelled) \
{ \
	*aNewActionCancelled = m_newActionCancelled; \
	return NS_OK; \
} \
NS_IMETHODIMP sashLocationDKLocation::SetNewActionCancelled(PRBool aNewActionCancelled) \
{ \
	m_newActionCancelled = aNewActionCancelled; \
	return NS_OK; \
} \
\
/* void Configure (); */ \
NS_IMETHODIMP sashLocationDKLocation::Configure() \
{ \
	string scr = "ConfigureAction();"; \
	m_js->EvalScript(scr.c_str()); \
	return NS_OK; \
} \
\
/* attribute string actionfile; */ \
NS_IMETHODIMP sashLocationDKLocation::GetActionfile(char * *aActionfile) { \
	XP_RETURN_STRING(m_actionfile.c_str(), aActionfile); \
} \
\
NS_IMETHODIMP sashLocationDKLocation::SetActionfile(const char * aActionfile) { \
  m_actionfile= aActionfile; \
  return NS_OK; \
} \
\
/* attribute long filecount; */ \
NS_IMETHODIMP sashLocationDKLocation::GetFilecount(PRInt32 *aFilecount) { \
  *aFilecount= m_filecount; \
  return NS_OK; \
} \
\
NS_IMETHODIMP sashLocationDKLocation::SetFilecount(PRInt32 aFilecount) { \
  m_filecount= aFilecount; \
  return NS_OK; \
}

class WDEITreeItem;

class sashLocationDKLocation : public sashILocationDKLocation
{
public:
  NS_DECL_ISUPPORTS
	NS_DECL_SASHIUNKNOWN
	NS_DECL_SASHIEXTENSION
	NS_DECL_SASHILOCATION
  NS_DECL_SASHIWDEADDIN
  NS_DECL_SASHIEXTENSIONDKLOCATION
  NS_DECL_SASHILOCATIONDKLOCATION

  sashLocationDKLocation();
  virtual ~sashLocationDKLocation();
  /* additional members */
	
private:
	SASHWDEADDIN_PRIVATEMEMBERS
	SASHEXTENSIONDKLOCATION_PRIVATEMEMBERS	
	SASHLOCATIONDKLOCATION_PRIVATEMEMBERS
};

#endif
