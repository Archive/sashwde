
/***************************************************************
    SashWDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _SASHWDEADDIN_H
#define _SASHWDEADDIN_H

#include "sashILocation.h"
#include "sashISashRuntime.h"
#include "sashIActionRuntime.h"
#include "sashIWDEAddin.h"
#include "sashJSEmbed.h"
#include "WDEITreeItem.h"
#include "WDEITreeHelpItem.h"
#include <string>
#include <unistd.h>

//4FFBC165-C903-49CD-BBA4-269667328E67
#define SASHWDEADDIN_CID {0x4FFBC165, 0xC903, 0x49CD, {0xBB, 0xA4, 0x26, 0x96, 0x67, 0x32, 0x8E, 0x67}}
NS_DEFINE_CID(ksashWDEAddinCID, SASHWDEADDIN_CID);

#define SASHWDEADDIN_CONTRACT_ID "@gnome.org/SashWDE/sashWDEAddin;1"

#define SASHWDEADDIN_PRIVATEMEMBERS \
	string m_name; \
	string m_dataDir; \
	string m_actionFName; \
	sashIActionRuntime *m_act; \
	JSContext *m_jsContext; \
	sashIJSEmbed *m_js; \
	WDEITreeHelpItem *m_helpTree; \
	void ChangeDir();
	
#define SASHWDEADDIN_CONSTRUCTOR \
	m_name = ""; \
	m_dataDir = ""; \
	m_act = NULL; \
	m_helpTree = NULL; \
	m_actionFName = "";
	
#define SASHWDEADDIN_DESTRUCTOR \
  NS_IF_RELEASE(m_helpTree); \
  NS_IF_RELEASE(m_act); \
  NS_IF_RELEASE(m_js);
	
#define SASHWDEADDIN_FUNCTIONS(sashWDEAddin) \
/* readonly attribute WDEIWDE WDE; */ \
NS_IMETHODIMP sashWDEAddin ::GetWDE(WDEIWDE * *aWDE) \
{ \
	WDEApp::GetWDE(aWDE); \
	return NS_OK; \
} \
\
/* attribute string name; */ \
NS_IMETHODIMP sashWDEAddin ::GetName(char * *aName) \
{ \
	XP_RETURN_STRING(m_name.c_str(), aName); \
} \
NS_IMETHODIMP sashWDEAddin::SetName(const char * aName) \
{ \
	m_name = aName; \
	return NS_OK; \
} \
\
/* readonly attribute string dataDirectory; */ \
NS_IMETHODIMP sashWDEAddin ::GetDataDirectory(char * *aDataDirectory) \
{ \
	XP_RETURN_STRING(m_dataDir.c_str(), aDataDirectory); \
} \
\
/* attribute WDEITreeHelpItem helpDocs; */ \
NS_IMETHODIMP sashWDEAddin ::GetHelpDocs(WDEITreeHelpItem * *aHelpDocs) \
{ \
	NS_IF_ADDREF(m_helpTree); \
	*aHelpDocs = m_helpTree; \
	return NS_OK; \
} \
NS_IMETHODIMP sashWDEAddin ::SetHelpDocs(WDEITreeHelpItem * aHelpDocs) \
{ \
	NS_IF_RELEASE(m_helpTree); \
	m_helpTree = aHelpDocs; \
	NS_IF_ADDREF(m_helpTree); \
	return NS_OK; \
} \
\
/* void run (in string url); */ \
NS_IMETHODIMP sashWDEAddin ::Run() \
{ \
	m_act->CreateSashObjectNative(m_jsContext); \
	m_js->SetSecurityMode(sashJSEmbedSecurityManager::OK_ALL); \
\
	m_js->EvalScriptFile(m_actionFName.c_str()); \
	return NS_OK; \
} \
\
NS_IMETHODIMP sashWDEAddin::Cleanup() { \
	 return NS_OK; \
} \
\
NS_IMETHODIMP sashWDEAddin::ProcessEvent() \
{ \
	return NS_OK; \
} \
\
NS_IMETHODIMP sashWDEAddin::Quit() \
{ \
	return NS_OK; \
} \
\
void sashWDEAddin::ChangeDir() \
{ \
  char* datadir; \
\
  m_act->GetDataDirectory(&datadir); \
\
  chdir(datadir); \
  nsMemory::Free(datadir); \
}

/* Header file */
class sashWDEAddin : public sashIWDEAddin
{
public:
  NS_DECL_ISUPPORTS
	NS_DECL_SASHIUNKNOWN
	NS_DECL_SASHIEXTENSION
	NS_DECL_SASHILOCATION
  NS_DECL_SASHIWDEADDIN

  sashWDEAddin();
  virtual ~sashWDEAddin();
  /* additional members */
	
private:
	SASHWDEADDIN_PRIVATEMEMBERS
};

#endif
