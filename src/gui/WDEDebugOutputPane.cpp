#include "WDEDebugOutputPane.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEDebugOutputPane, WDEPane, WDEIDebugOutputPane)

WDEDebugOutputPane::WDEDebugOutputPane()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
	
	m_webDebugTab = do_CreateInstance("@gnome.org/SashWDE/WDEWeblicationDebugPaneTab;1");
	m_webOutputTab = do_CreateInstance("@gnome.org/SashWDE/WDEWeblicationOutputPaneTab;1");
	m_macroOutputTab = do_CreateInstance("@gnome.org/SashWDE/WDEMacroOutputPaneTab;1");
}

WDEDebugOutputPane::~WDEDebugOutputPane()
{
  /* destructor code */
}

/* readonly attribute WDEIWeblicationDebugPaneTab weblicationDebugTab; */
NS_IMETHODIMP WDEDebugOutputPane::GetWeblicationDebugTab(WDEIWeblicationDebugPaneTab * *aWeblicationDebugTab)
{
	XPCOM_COMPTR_GETTER(m_webDebugTab, aWeblicationDebugTab);
}

/* readonly attribute WDEIWeblicationOutputPaneTab weblicationOutputTab; */
NS_IMETHODIMP WDEDebugOutputPane::GetWeblicationOutputTab(WDEIWeblicationOutputPaneTab * *aWeblicationOutputTab)
{
	XPCOM_COMPTR_GETTER(m_webOutputTab, aWeblicationOutputTab);
}

/* readonly attribute WDEIMacroOutputPaneTab macroOutputTab; */
NS_IMETHODIMP WDEDebugOutputPane::GetMacroOutputTab(WDEIMacroOutputPaneTab * *aMacroOutputTab)
{
	XPCOM_COMPTR_GETTER(m_macroOutputTab, aMacroOutputTab);
}

/* void build (); */
NS_IMETHODIMP WDEDebugOutputPane::Build()
{
	m_webDebugTab->Build();
	m_webOutputTab->Build();
	m_macroOutputTab->Build();
	
	nsCOMPtr<WDEIPaneTab> t;
	t = do_QueryInterface(m_webDebugTab);
	AppendTab(t);
	
	t = do_QueryInterface(m_webOutputTab);
	AppendTab(t);
	
	t = do_QueryInterface(m_macroOutputTab);
	AppendTab(t);
	
	SetName("Debug Pane");
	m_popupMenu->AddItem("Debug\\Debug Item, Whatever", 5001);
	return NS_OK;
}
