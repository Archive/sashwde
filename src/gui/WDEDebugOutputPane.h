#ifndef _WDEDEBUGOUTPUTPANE_H
#define _WDEDEBUGOUTPUTPANE_H

#include <gtk/gtk.h>
#include "WDEIDebugOutputPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//71236FCA-93D8-4AB6-8C2B-0FB0BDF71777
#define WDEDEBUGOUTPUTPANE_CID {0x71236FCA, 0x93D8, 0x4AB6, {0x8C, 0x2B, 0x0F, 0xB0, 0xBD, 0xF7, 0x17, 0x77}}
NS_DEFINE_CID(kWDEDebugOutputPaneCID, WDEDEBUGOUTPUTPANE_CID);

#define WDEDEBUGOUTPUTPANE_CONTRACT_ID "@gnome.org/SashWDE/WDEDebugOutputPane;1"

class WDEDebugOutputPane : public WDEIDebugOutputPane, public WDEPane
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIDEBUGOUTPUTPANE
	NS_FORWARD_WDEIPANE(WDEPane::)
		
  WDEDebugOutputPane();
  virtual ~WDEDebugOutputPane();
  /* additional members */
	
private:
	nsCOMPtr<WDEIWeblicationDebugPaneTab> m_webDebugTab;
	nsCOMPtr<WDEIWeblicationOutputPaneTab> m_webOutputTab;
	nsCOMPtr<WDEIMacroOutputPaneTab> m_macroOutputTab;
};

#endif
