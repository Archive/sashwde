#include "WDEFunctionBrowserPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEFunctionBrowserPaneTab, WDEPaneTab, WDEIFunctionBrowserPaneTab)

WDEFunctionBrowserPaneTab::WDEFunctionBrowserPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEFunctionBrowserPaneTab::~WDEFunctionBrowserPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEFunctionBrowserPaneTab::Build()
{
	m_name = "Functions";
	return NS_OK;
}
