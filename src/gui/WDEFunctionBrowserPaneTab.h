#ifndef _WDEFUNCTIONBROWSERPANETAB_H
#define _WDEFUNCTIONBROWSERPANETAB_H

#include <gtk/gtk.h>
#include "WDEIProjectBrowserPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//6224B5AC-FE0F-4005-983C-171F7CA1E756
#define WDEFUNCTIONBROWSERPANETAB_CID {0x6224B5AC, 0xFE0F, 0x4005, {0x98, 0x3C, 0x17, 0x1F, 0x7C, 0xA1, 0xE7, 0x56}}
NS_DEFINE_CID(kWDEFunctionBrowserPaneTabCID, WDEFUNCTIONBROWSERPANETAB_CID);

#define WDEFUNCTIONBROWSERPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEFunctionBrowserPaneTab;1"

class WDEFunctionBrowserPaneTab : public WDEIFunctionBrowserPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIFUNCTIONBROWSERPANETAB
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)

  WDEFunctionBrowserPaneTab();
  virtual ~WDEFunctionBrowserPaneTab();
  /* additional members */
};

#endif
