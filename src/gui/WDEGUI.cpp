#include "WDEGUI.h"
#include <gnome.h>
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS1(WDEGUI, WDEIGUI)

WDEGUI::WDEGUI()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */

	//build the panetree	
	m_contentTree = do_CreateInstance("@gnome.org/SashWDE/WDEPaneTreeItem;1");
	m_projectBrowserTI = do_CreateInstance("@gnome.org/SashWDE/WDEPaneTreeItem;1");
	m_debugOutputTI = NULL;
	m_sourceTI = do_CreateInstance("@gnome.org/SashWDE/WDEPaneTreeItem;1");
	m_propertyTI = NULL;
  m_helpTI = NULL;
	
	nsCOMPtr<WDEIPaneTreeItem> t1 = NULL;	
  nsCOMPtr<WDEIPaneTreeItem> t2 = do_CreateInstance("@gnome.org/SashWDE/WDEPaneTreeItem;1");
  nsCOMPtr<WDEIPaneTreeItem> t3 = NULL;
	m_contentTree->Split(WDEIPaneTreeItem::WDEPaneTreeItemHorizontal, WDEIPaneTreeItem::WDEPaneTreeItemLeft, t2, getter_AddRefs(t1));
  t2->Split(WDEIPaneTreeItem::WDEPaneTreeItemVertical, WDEIPaneTreeItem::WDEPaneTreeItemTop, m_projectBrowserTI, getter_AddRefs(m_propertyTI));
  
	t3 = do_CreateInstance("@gnome.org/SashWDE/WDEPaneTreeItem;1");
	t1->Split(WDEIPaneTreeItem::WDEPaneTreeItemVertical, WDEIPaneTreeItem::WDEPaneTreeItemTop, t3, getter_AddRefs(m_debugOutputTI));
	t3->Split(WDEIPaneTreeItem::WDEPaneTreeItemHorizontal, WDEIPaneTreeItem::WDEPaneTreeItemLeft, m_sourceTI, getter_AddRefs(m_helpTI));
	
	//make the panes
  m_projectBrowserPane = do_CreateInstance("@gnome.org/SashWDE/WDEProjectBrowserPane;1");
	m_debugOutputPane = do_CreateInstance("@gnome.org/SashWDE/WDEDebugOutputPane;1");
	m_sourcePane = do_CreateInstance("@gnome.org/SashWDE/WDESourcePane;1");
  m_helpPane = do_CreateInstance("@gnome.org/SashWDE/WDEHelpPane;1");
	m_propertyPane = do_CreateInstance("@gnome.org/SashWDE/WDEPropertyPane;1");

	nsCOMPtr<WDEIPane> t;
	t = do_QueryInterface(m_projectBrowserPane);
	m_projectBrowserTI->SetPane(t);

	t = do_QueryInterface(m_debugOutputPane);
	m_debugOutputTI->SetPane(t);

	t = do_QueryInterface(m_sourcePane);
	m_sourceTI->SetPane(t);
	
	t = do_QueryInterface(m_helpPane);
	m_helpTI->SetPane(t);

	t = do_QueryInterface(m_propertyPane);
	m_propertyTI->SetPane(t);
							
	m_projectBrowserPane->Build();
	m_debugOutputPane->Build();
	m_sourcePane->Build();
	m_helpPane->Build();
	m_propertyPane->Build();
}

WDEGUI::~WDEGUI()
{
  /* destructor code */
}

/* readonly attribute WDEIPaneTreeItem mainContentTree; */
NS_IMETHODIMP WDEGUI::GetMainContentTree(WDEIPaneTreeItem * *aMainContentTree)
{
	XPCOM_COMPTR_GETTER(m_contentTree, aMainContentTree);
}

/* readonly attribute WDEIProjectBrowserPane projectBrowserPane; */
NS_IMETHODIMP WDEGUI::GetProjectBrowserPane(WDEIProjectBrowserPane * *aProjectBrowserPane)
{
	XPCOM_COMPTR_GETTER(m_projectBrowserPane, aProjectBrowserPane);
}

/* readonly attribute WDEIDebugOutputPane debugOutputPane; */
NS_IMETHODIMP WDEGUI::GetDebugOutputPane(WDEIDebugOutputPane * *aDebugOutputPane)
{
	XPCOM_COMPTR_GETTER(m_debugOutputPane, aDebugOutputPane);
}

/* readonly attribute WDEISourcePane sourcePane; */
NS_IMETHODIMP WDEGUI::GetSourcePane(WDEISourcePane * *aSourcePane)
{
	XPCOM_COMPTR_GETTER(m_sourcePane, aSourcePane);
}

/* readonly attribute WDEIHelpPane helpPane; */
NS_IMETHODIMP WDEGUI::GetHelpPane(WDEIHelpPane * *aHelpPane)
{
  XPCOM_COMPTR_GETTER(m_helpPane, aHelpPane);
}

/* readonly attribute WDEIPropertyPane propertyPane; */
NS_IMETHODIMP WDEGUI::GetPropertyPane(WDEIPropertyPane * *aPropertyPane)
{
	XPCOM_COMPTR_GETTER(m_propertyPane, aPropertyPane);
}

/* void Run (); */
NS_IMETHODIMP WDEGUI::Run()
{
	GtkWidget *contain;
	m_contentTree->GetGtkContainer(&contain);	
	GtkWidget *win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	GtkWidget *hbox = gtk_hbox_new(FALSE, 0);

	gtk_container_add(GTK_CONTAINER(win), hbox);	
	gtk_container_add(GTK_CONTAINER(hbox), contain);
	
	gtk_widget_show(hbox);
	gtk_widget_show(win);
	/* Try to fix closing problem */
	gtk_signal_connect(GTK_OBJECT(win), "destroy", GTK_SIGNAL_FUNC(gtk_main_quit), 0);
	/* hope it works */
	gtk_main();

	return NS_OK;
}
