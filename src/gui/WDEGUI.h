#ifndef _WDEGUI_H
#define _WDEGUI_H

#include <gtk/gtk.h>
#include "WDEIGUI.h"
#include "nsCOMPtr.h"
#include <string>

#include "WDEIProjectBrowserPane.h"
#include "WDEIDebugOutputPane.h"
#include "WDEISourcePane.h"
#include "WDEIHelpPane.h"
#include "WDEIPropertyPane.h"

//8C302DEA-334B-4C26-9B0E-D1B815F022CA
#define WDEGUI_CID {0x8C302DEA, 0x334B, 0x4C26, {0x9B, 0x0E, 0xD1, 0xB8, 0x15, 0xF0, 0x22, 0xCA}}
NS_DEFINE_CID(kWDEGUICID, WDEGUI_CID);

#define WDEGUI_CONTRACT_ID "@gnome.org/SashWDE/WDEGUI;1"

class WDEGUI : public WDEIGUI
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIGUI

  WDEGUI();
  virtual ~WDEGUI();
  /* additional members */
	
private:
	nsCOMPtr<WDEIPaneTreeItem> m_contentTree;
	nsCOMPtr<WDEIPaneTreeItem> m_projectBrowserTI;
	nsCOMPtr<WDEIPaneTreeItem> m_debugOutputTI;
	nsCOMPtr<WDEIPaneTreeItem> m_sourceTI;
	nsCOMPtr<WDEIPaneTreeItem> m_helpTI;
	nsCOMPtr<WDEIPaneTreeItem> m_propertyTI;
	nsCOMPtr<WDEIProjectBrowserPane> m_projectBrowserPane;
	nsCOMPtr<WDEIDebugOutputPane> m_debugOutputPane;
	nsCOMPtr<WDEISourcePane> m_sourcePane;
	nsCOMPtr<WDEIHelpPane> m_helpPane;
	nsCOMPtr<WDEIPropertyPane> m_propertyPane;
};

#endif
