#include "nsIGenericFactory.h"
#include "WDEPane.h"
#include "WDEGUI.h"

//project browser
#include "WDEProjectBrowserPane.h"
#include "WDEProjectBrowserPaneTab.h"
#include "WDEFunctionBrowserPaneTab.h"
#include "WDEMacroBrowserPaneTab.h"

//debug output
#include "WDEWeblicationDebugPaneTab.h"
#include "WDEDebugOutputPane.h"
#include "WDEWeblicationOutputPaneTab.h"
#include "WDEMacroOutputPaneTab.h"

//help
#include "WDEHelpPane.h"

//source
#include "WDESourcePane.h"

//properties
#include "WDEPropertyPane.h"
#include "WDEProjectItemPropertyPaneTab.h"
#include "WDEHtmlWidgetPaneTab.h"
#include "WDEGtkWidgetPaneTab.h"

//popup menu
#include "WDEPopupMenu.h"

//source tabs
#include "WDEGenericSourceTab.h"
#include "WDEScintillaEditor.h"

NS_GENERIC_FACTORY_CONSTRUCTOR(WDEGUI)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEPane)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEPaneTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEPaneTreeItem)

//project browser
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEProjectBrowserPane)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEProjectBrowserPaneTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEFunctionBrowserPaneTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEMacroBrowserPaneTab)

//debug output
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEWeblicationDebugPaneTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEWeblicationOutputPaneTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEMacroOutputPaneTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEDebugOutputPane)

//help
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEHelpPane)

//source
NS_GENERIC_FACTORY_CONSTRUCTOR(WDESourcePane)

//properties
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEPropertyPane)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEProjectItemPropertyPaneTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEHtmlWidgetPaneTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEGtkWidgetPaneTab)

//popup menu
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEPopupMenu)
//source tabs
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEGenericSourceTab)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEScintillaEditor)

				
static nsModuleComponentInfo components[] = {
  {
    "WDEGUI module",
    WDEGUI_CID,
    WDEGUI_CONTRACT_ID,
    WDEGUIConstructor,
    NULL,
    NULL
  },
  {
    "WDEPane",
    WDEPANE_CID,
    WDEPANE_CONTRACT_ID,
    WDEPaneConstructor,
    NULL,
    NULL
  } ,
  {
    "WDEPaneTab",
    WDEPANETAB_CID,
    WDEPANETAB_CONTRACT_ID,
    WDEPaneTabConstructor,
    NULL,
    NULL
  } ,
  {
    "WDEPaneTreeItem",
    WDEPANETREEITEM_CID,
    WDEPANETREEITEM_CONTRACT_ID,
    WDEPaneTreeItemConstructor,
    NULL,
    NULL
  },
  {
    "WDEProjectBrowserPane",
    WDEPROJECTBROWSERPANE_CID,
    WDEPROJECTBROWSERPANE_CONTRACT_ID,
    WDEProjectBrowserPaneConstructor,
    NULL,
    NULL
  } ,
  {
    "WDEProjectBrowserPaneTab",
    WDEPROJECTBROWSERPANETAB_CID,
    WDEPROJECTBROWSERPANETAB_CONTRACT_ID,
    WDEProjectBrowserPaneTabConstructor,
    NULL,
    NULL
  } ,
  {
    "WDEMacroBrowserPaneTab",
    WDEMACROBROWSERPANETAB_CID,
    WDEMACROBROWSERPANETAB_CONTRACT_ID,
    WDEMacroBrowserPaneTabConstructor,
    NULL,
    NULL
  } ,
  {
    "WDEFunctionBrowserPaneTab",
    WDEFUNCTIONBROWSERPANETAB_CID,
    WDEFUNCTIONBROWSERPANETAB_CONTRACT_ID,
    WDEFunctionBrowserPaneTabConstructor,
    NULL,
    NULL
  },
  {
    "WDEWeblicationDebugPaneTab",
    WDEWEBLICATIONDEBUGPANETAB_CID,
    WDEWEBLICATIONDEBUGPANETAB_CONTRACT_ID,
    WDEWeblicationDebugPaneTabConstructor,
    NULL,
    NULL
  } ,
  {
    "WDEWeblicationOutputPaneTab",
    WDEWEBLICATIONOUTPUTPANETAB_CID,
    WDEWEBLICATIONOUTPUTPANETAB_CONTRACT_ID,
    WDEWeblicationOutputPaneTabConstructor,
    NULL,
    NULL
  } ,
  {
    "WDEMacroOutputPaneTab",
    WDEMACROOUTPUTPANETAB_CID,
    WDEMACROOUTPUTPANETAB_CONTRACT_ID,
    WDEMacroOutputPaneTabConstructor,
    NULL,
    NULL
  } ,
  {
    "WDEDebugOutputPane",
    WDEDEBUGOUTPUTPANE_CID,
    WDEDEBUGOUTPUTPANE_CONTRACT_ID,
    WDEDebugOutputPaneConstructor,
    NULL,
    NULL
  },
  {
    "WDEHelpPane",
    WDEHELPPANE_CID,
    WDEHELPPANE_CONTRACT_ID,
    WDEHelpPaneConstructor,
    NULL,
    NULL
  },
  {
    "WDESourcePane",
    WDESOURCEPANE_CID,
    WDESOURCEPANE_CONTRACT_ID,
    WDESourcePaneConstructor,
    NULL,
    NULL
  },
  {
    "WDEPropertyPane",
    WDEPROPERTYPANE_CID,
    WDEPROPERTYPANE_CONTRACT_ID,
    WDEPropertyPaneConstructor,
    NULL,
    NULL
  },
  {
    "WDEProjectItemPropertyPaneTab",
    WDEPROJECTITEMPROPERTYPANETAB_CID,
    WDEPROJECTITEMPROPERTYPANETAB_CONTRACT_ID,
    WDEProjectItemPropertyPaneTabConstructor,
    NULL,
    NULL
  },
  {
    "WDEHtmlWidgetPaneTab",
    WDEHTMLWIDGETPANETAB_CID,
    WDEHTMLWIDGETPANETAB_CONTRACT_ID,
    WDEHtmlWidgetPaneTabConstructor,
    NULL,
    NULL
  },
  {
    "WDEGtkWidgetPaneTab",
    WDEGTKWIDGETPANETAB_CID,
    WDEGTKWIDGETPANETAB_CONTRACT_ID,
    WDEGtkWidgetPaneTabConstructor,
    NULL,
    NULL
  },
  {
    "WDEPopupMenu",
    WDEPOPUPMENU_CID,
    WDEPOPUPMENU_CONTRACT_ID,
    WDEPopupMenuConstructor,
    NULL,
    NULL
  },
  {
    "WDEGenericSourceTab",
    WDEGENERICSOURCETAB_CID,
    WDEGENERICSOURCETAB_CONTRACT_ID,
    WDEGenericSourceTabConstructor,
    NULL,
    NULL
  },
  {
    "WDEScintillaEditor",
    WDESCINTILLAEDITOR_CID,
    WDESCINTILLAEDITOR_CONTRACT_ID,
    WDEScintillaEditorConstructor,
    NULL,
    NULL
  }
};

NS_IMPL_NSGETMODULE("WDEGUI module", components)
