#include "WDEGenericSourceTab.h"
#include "WDEScintillaEditor.h"

NS_IMPL_ISUPPORTS_INHERITED2(WDEGenericSourceTab, WDEPaneTab, WDEIGenericSourceTab, WDEIEventSender)

WDEGenericSourceTab::WDEGenericSourceTab() {
  NS_INIT_ISUPPORTS();
  WDE_INIT_SENDER
  m_popupMenu= do_CreateInstance(WDEPOPUPMENU_CONTRACT_ID);
  m_textEditor= do_CreateInstance(WDESCINTILLAEDITOR_CONTRACT_ID);
  GtkWidget *sciWid;
//  sciWid= gtk_label_new("BADABING");
//  gtk_widget_show(sciWid);
  m_textEditor->GetGtkWidget(&sciWid);
  gtk_box_pack_start(GTK_BOX(m_gtkContainer), sciWid, TRUE, TRUE, 0);
//  m_popupMenu->AttachMenu(m_gtkContainer);
}

WDEGenericSourceTab::~WDEGenericSourceTab() {
}

/* readonly attribute WDEITextEditor editor; */
NS_IMETHODIMP WDEGenericSourceTab::GetEditor(WDEITextEditor * *aEditor) {
  XPCOM_COMPTR_GETTER(m_textEditor, aEditor);
}

/* readonly attribute WDEIPopupMenu popupMenu; */
NS_IMETHODIMP WDEGenericSourceTab::GetPopupMenu(WDEIPopupMenu * *aPopupMenu) {
  XPCOM_COMPTR_GETTER(m_popupMenu, aPopupMenu);
}
