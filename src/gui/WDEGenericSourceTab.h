#ifndef WDEGENERICSOURCETAB_H
#define WDEGENERICSOURCETAB_H

#include <gtk/gtk.h>
#include "WDEPane.h"
#include "WDEIGenericSourceTab.h"
#include "WDEPopupMenu.h"
#include "WDEITextEditor.h"
#include "xpcomtools.h"

//e90ab8ae-7bc4-44e4-827b-4f2da58bb049
#define WDEGENERICSOURCETAB_CID {0xe90ab8ae, 0x7bc4, 0x44e4, {0x82, 0x7b, 0x4f, 0x2d, 0xa5, 0x8b, 0xb0, 0x49}}
#define WDEGENERICSOURCETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEGenericSourceTab;1"

NS_DEFINE_CID(kWDEGenericSourceTabCID, WDEGENERICSOURCETAB_CID);

class WDEGenericSourceTab:
  public WDEIGenericSourceTab,
  public WDEIEventSender,
  public WDEPaneTab
{
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_WDEIGENERICSOURCETAB
    NS_FORWARD_WDEIPANETAB(WDEPaneTab::)
    WDE_DECL_SENDER
  
    WDEGenericSourceTab();
    virtual ~WDEGenericSourceTab();

  private:
    nsCOMPtr<WDEIPopupMenu> m_popupMenu;
    nsCOMPtr<WDEITextEditor> m_textEditor;
    
};

#endif
