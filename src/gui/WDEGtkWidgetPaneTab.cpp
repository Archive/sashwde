#include "WDEGtkWidgetPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEGtkWidgetPaneTab, WDEPaneTab, WDEIGtkWidgetPaneTab)

WDEGtkWidgetPaneTab::WDEGtkWidgetPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEGtkWidgetPaneTab::~WDEGtkWidgetPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEGtkWidgetPaneTab::Build()
{
	m_name = "Gtk";
	return NS_OK;
}
