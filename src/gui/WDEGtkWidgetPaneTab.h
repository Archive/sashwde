#ifndef _WDEGTKWIDGETPANETAB_H
#define _WDEGTKWIDGETPANETAB_H

#include <gtk/gtk.h>
#include "WDEIPropertyPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//4C8A18E7-B6E3-4D60-8C06-7E8F18BA17D8
#define WDEGTKWIDGETPANETAB_CID {0x4C8A18E7, 0xB6E3, 0x4D60, {0x8C, 0x06, 0x7E, 0x8F, 0x18, 0xBA, 0x17, 0xD8}}
NS_DEFINE_CID(kWDEGtkWidgetPaneTabCID, WDEGTKWIDGETPANETAB_CID);

#define WDEGTKWIDGETPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEGtkWidgetPaneTab;1"

class WDEGtkWidgetPaneTab : public WDEIGtkWidgetPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIGTKWIDGETPANETAB
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)

  WDEGtkWidgetPaneTab();
  virtual ~WDEGtkWidgetPaneTab();
  /* additional members */
};

#endif
