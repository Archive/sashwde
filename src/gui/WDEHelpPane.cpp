#include "WDEHelpPane.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEHelpPane, WDEPane, WDEIHelpPane)

WDEHelpPane::WDEHelpPane()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEHelpPane::~WDEHelpPane()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEHelpPane::Build()
{
  SetName("Help Pane");
  m_popupMenu->AddItem("Help\\Contents", 1001);
  m_popupMenu->AddItem("Help\\Index", 1002);
	return NS_OK;
}
