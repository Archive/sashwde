#ifndef _WDEHELPPANE_H
#define _WDEHELPPANE_H

#include <gtk/gtk.h>
#include "WDEIHelpPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//5D4DD37E-E025-4CD9-86CB-C86C7A8C23E4
#define WDEHELPPANE_CID {0x5D4DD37E, 0xE025, 0x4CD9, {0x86, 0xCB, 0xC8, 0x6C, 0x7A, 0x8C, 0x23, 0xE4}}
NS_DEFINE_CID(kWDEHelpPaneCID, WDEHELPPANE_CID);

#define WDEHELPPANE_CONTRACT_ID "@gnome.org/SashWDE/WDEHelpPane;1"

class WDEHelpPane : public WDEIHelpPane, public WDEPane
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIHELPPANE
	NS_FORWARD_WDEIPANE(WDEPane::)

  WDEHelpPane();
  virtual ~WDEHelpPane();
  /* additional members */
};

#endif
