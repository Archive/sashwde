#include "WDEHtmlWidgetPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEHtmlWidgetPaneTab, WDEPaneTab, WDEIHtmlWidgetPaneTab)

WDEHtmlWidgetPaneTab::WDEHtmlWidgetPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEHtmlWidgetPaneTab::~WDEHtmlWidgetPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEHtmlWidgetPaneTab::Build()
{
	m_name = "HTML";
	return NS_OK;
}
