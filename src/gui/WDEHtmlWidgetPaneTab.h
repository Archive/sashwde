#ifndef _WDEHTMLWIDGETPANETAB_H
#define _WDEHTMLWIDGETPANETAB_H

#include <gtk/gtk.h>
#include "WDEIPropertyPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//1AC40B76-9F18-4CD2-9D4F-F3A760EEEF44
#define WDEHTMLWIDGETPANETAB_CID {0x1AC40B76, 0x9F18, 0x4CD2, {0x9D, 0x4F, 0xF3, 0xA7, 0x60, 0xEE, 0xEF, 0x44}}
NS_DEFINE_CID(kWDEHtmlWidgetPaneTabCID, WDEHTMLWIDGETPANETAB_CID);

#define WDEHTMLWIDGETPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEHtmlWidgetPaneTab;1"

class WDEHtmlWidgetPaneTab : public WDEIHtmlWidgetPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIHTMLWIDGETPANETAB
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)

  WDEHtmlWidgetPaneTab();
  virtual ~WDEHtmlWidgetPaneTab();
  /* additional members */
};

#endif
