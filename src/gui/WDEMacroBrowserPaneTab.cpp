#include "WDEMacroBrowserPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEMacroBrowserPaneTab, WDEPaneTab, WDEIMacroBrowserPaneTab)

WDEMacroBrowserPaneTab::WDEMacroBrowserPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEMacroBrowserPaneTab::~WDEMacroBrowserPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEMacroBrowserPaneTab::Build()
{
	m_name = "Macros";
	return NS_OK;
}
