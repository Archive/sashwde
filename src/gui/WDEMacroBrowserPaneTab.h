#ifndef _WDEMACROBROWSERPANETAB_H
#define _WDEMACROBROWSERPANETAB_H

#include <gtk/gtk.h>
#include "WDEIProjectBrowserPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//55E4B05E-61DC-4E0F-B0F4-ED1D7D2671E5
#define WDEMACROBROWSERPANETAB_CID {0x55E4B05E, 0x61DC, 0x4E0F, {0xB0, 0xF4, 0xED, 0x1D, 0x7D, 0x26, 0x71, 0xE5}}
NS_DEFINE_CID(kWDEMacroBrowserPaneTabCID, WDEMACROBROWSERPANETAB_CID);

#define WDEMACROBROWSERPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEMacroBrowserPaneTab;1"

class WDEMacroBrowserPaneTab : public WDEIMacroBrowserPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIMACROBROWSERPANETAB
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)

  WDEMacroBrowserPaneTab();
  virtual ~WDEMacroBrowserPaneTab();
  /* additional members */
};

#endif
