#include "WDEMacroOutputPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEMacroOutputPaneTab, WDEPaneTab, WDEIMacroOutputPaneTab)

WDEMacroOutputPaneTab::WDEMacroOutputPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEMacroOutputPaneTab::~WDEMacroOutputPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEMacroOutputPaneTab::Build()
{
	m_name = "Macro Output";
	return NS_OK;
}
