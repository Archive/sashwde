#ifndef _WDEMACROOUTPUTPANETAB_H
#define _WDEMACROOUTPUTPANETAB_H

#include <gtk/gtk.h>
#include "WDEIDebugOutputPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//B56AF9CD-EA1C-4257-9769-FE14DD7B49D0
#define WDEMACROOUTPUTPANETAB_CID {0xB56AF9CD, 0xEA1C, 0x4257, {0x97, 0x69, 0xFE, 0x14, 0xDD, 0x7B, 0x49, 0xD0}}
NS_DEFINE_CID(kWDEMacroOutputPaneTabCID, WDEMACROOUTPUTPANETAB_CID);

#define WDEMACROOUTPUTPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEMacroOutputPaneTab;1"

class WDEMacroOutputPaneTab : public WDEIMacroOutputPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIMACROOUTPUTPANETAB
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)

  WDEMacroOutputPaneTab();
  virtual ~WDEMacroOutputPaneTab();
  /* additional members */
};

#endif
