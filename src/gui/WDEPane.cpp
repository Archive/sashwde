#include "WDEPane.h"
#include "WDEPopupMenu.h"
#include <gnome.h>
#include "WDEApp.h"
#include "xpcomtools.h"
#include <algorithm>

//*****************************************************************
//pane tree item
//*****************************************************************

NS_IMPL_ISUPPORTS1(WDEPaneTreeItem, WDEIPaneTreeItem)

WDEPaneTreeItem::WDEPaneTreeItem()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
  
  m_alignment = WDEIPaneTreeItem::WDEPaneTreeItemHorizontal;
  m_type = WDEIPaneTreeItem::WDEPaneTreeItemPane;
  m_gtkContainer = gtk_hbox_new(FALSE, 0);
	gtk_widget_show(m_gtkContainer);
	m_gtkSplitter = NULL;
  m_parent = NULL;
  m_leftChild = NULL;
  m_rightChild = NULL;
  m_leftFrame= NULL;
  m_rightFrame= NULL;
  m_pane = NULL;
}

WDEPaneTreeItem::~WDEPaneTreeItem()
{
  /* destructor code */
}

/* attribute PRInt32 alignment; */
NS_IMETHODIMP WDEPaneTreeItem::GetAlignment(PRInt32 *aAlignment)
{
  XPCOM_SIMPLE_GETTER(m_alignment, aAlignment);
}
NS_IMETHODIMP WDEPaneTreeItem::SetAlignment(PRInt32 aAlignment)
{
  XPCOM_SIMPLE_SETTER(m_alignment, aAlignment);
}

/* attribute PRInt32 type; */
NS_IMETHODIMP WDEPaneTreeItem::GetType(PRInt32 *aType)
{
  XPCOM_SIMPLE_GETTER(m_type, aType);
}
NS_IMETHODIMP WDEPaneTreeItem::SetType(PRInt32 aType)
{
  XPCOM_SIMPLE_SETTER(m_type, aType);
}

/* [noscript] attribute GtkWidget gtkContainer; */
NS_IMETHODIMP WDEPaneTreeItem::GetGtkContainer(GtkWidget * *aGtkContainer)
{
  XPCOM_PTR_GETTER(m_gtkContainer, aGtkContainer);
}
NS_IMETHODIMP WDEPaneTreeItem::SetGtkContainer(GtkWidget * aGtkContainer)
{
  XPCOM_SIMPLE_SETTER(m_gtkContainer, aGtkContainer);
}

/* [noscript] attribute GtkWidget gtkSplitter; */
NS_IMETHODIMP WDEPaneTreeItem::GetGtkSplitter(GtkWidget * *aGtkSplitter)
{
	XPCOM_PTR_GETTER(m_gtkSplitter, aGtkSplitter);
}
NS_IMETHODIMP WDEPaneTreeItem::SetGtkSplitter(GtkWidget * aGtkSplitter)
{
	XPCOM_SIMPLE_SETTER(m_gtkSplitter, aGtkSplitter);
}
/* [noscript] attribute GtkWidget gtkLeftFrame; */
NS_IMETHODIMP WDEPaneTreeItem::GetGtkLeftFrame(GtkWidget * *aGtkLeftFrame)
{
  XPCOM_PTR_GETTER(m_leftFrame, aGtkLeftFrame);
}
NS_IMETHODIMP WDEPaneTreeItem::SetGtkLeftFrame(GtkWidget * aGtkLeftFrame)
{
  XPCOM_SIMPLE_SETTER(m_leftFrame, aGtkLeftFrame);
}

/* [noscript] attribute GtkWidget gtkRightFrame; */
NS_IMETHODIMP WDEPaneTreeItem::GetGtkRightFrame(GtkWidget * *aGtkRightFrame)
{
  XPCOM_PTR_GETTER(m_rightFrame, aGtkRightFrame);
}
NS_IMETHODIMP WDEPaneTreeItem::SetGtkRightFrame(GtkWidget * aGtkRightFrame)
{
  XPCOM_SIMPLE_SETTER(m_rightFrame, aGtkRightFrame);
}

/* attribute WDEIPaneTreeItem parent; */
NS_IMETHODIMP WDEPaneTreeItem::GetParent(WDEIPaneTreeItem * *aParent)
{
  XPCOM_COMPTR_GETTER(m_parent, aParent);
}
NS_IMETHODIMP WDEPaneTreeItem::SetParent(WDEIPaneTreeItem * aParent)
{
  XPCOM_SIMPLE_SETTER(m_parent, aParent);
}

/* attribute WDEIPaneTreeItem leftChild; */
NS_IMETHODIMP WDEPaneTreeItem::GetLeftChild(WDEIPaneTreeItem * *aLeftChild)
{
  XPCOM_COMPTR_GETTER(m_leftChild, aLeftChild);
}
NS_IMETHODIMP WDEPaneTreeItem::SetLeftChild(WDEIPaneTreeItem * aLeftChild)
{
  XPCOM_SIMPLE_SETTER(m_leftChild, aLeftChild);
}

/* attribute WDEIPaneTreeItem rightChild; */
NS_IMETHODIMP WDEPaneTreeItem::GetRightChild(WDEIPaneTreeItem * *aRightChild)
{
  XPCOM_COMPTR_GETTER(m_rightChild, aRightChild);
}
NS_IMETHODIMP WDEPaneTreeItem::SetRightChild(WDEIPaneTreeItem * aRightChild)
{
  XPCOM_SIMPLE_SETTER(m_rightChild, aRightChild);
}

/* attribute WDEIPane pane; */
NS_IMETHODIMP WDEPaneTreeItem::GetPane(WDEIPane * *aPane)
{
  XPCOM_COMPTR_GETTER(m_pane, aPane);
}
NS_IMETHODIMP WDEPaneTreeItem::SetPane(WDEIPane * aPane)
{
	if(m_type != WDEIPaneTreeItem::WDEPaneTreeItemPane) return NS_OK;
	if(!aPane)
	{
		m_pane = NULL;
		return NS_OK;
	}
	
	GtkWidget *paneContainer;
	if(m_pane)
	{
		m_pane->GetGtkContainer(&paneContainer);
		gtk_container_remove(GTK_CONTAINER(m_gtkContainer), paneContainer);
	}

	nsCOMPtr<WDEIPaneTreeItem> pti = this;
	aPane->SetPaneTree(pti);
		
	aPane->GetGtkContainer(&paneContainer);
	gtk_box_pack_start(GTK_BOX(m_gtkContainer), paneContainer, TRUE, TRUE, 0);
  m_pane= aPane;
  Refresh();
  return NS_OK;
//  XPCOM_SIMPLE_SETTER(m_pane, aPane);
}

/* void collapse (in PRInt32 childToKeep); */
NS_IMETHODIMP WDEPaneTreeItem::Collapse(PRInt32 childToKeep)
{
	//TODO: this is broken, needs to be rewritten for new design
  if(!m_parent) return NS_OK;
  if(!m_leftChild) return NS_OK;

  nsCOMPtr<WDEIPaneTreeItem> keepChild;
  if(childToKeep == WDEIPaneTreeItem::WDEPaneTreeItemLeft) keepChild = m_leftChild;
  else keepChild = m_rightChild;

	GtkWidget *keepContainer;
  keepChild->GetGtkContainer(&keepContainer);

  GtkWidget *parentSplitter;
  m_parent->GetGtkSplitter(&parentSplitter);

  nsCOMPtr<WDEIPaneTreeItem> parentsLeft;
  m_parent->GetLeftChild(getter_AddRefs(parentsLeft));
  if(parentsLeft == this)
  {
    m_parent->SetLeftChild(keepChild);
    gtk_paned_pack1(GTK_PANED(parentSplitter), keepContainer, TRUE, TRUE);
  }
  else
  {
    m_parent->SetRightChild(keepChild);
    gtk_paned_pack2(GTK_PANED(parentSplitter), keepContainer, TRUE, TRUE);
  }
  
  keepChild->SetParent(m_parent);

  return NS_OK;
}

void WDEPaneTreeItem::splitCopyTo(WDEIPaneTreeItem *to)
{
	to->SetAlignment(m_alignment);
	to->SetType(m_type);
	to->SetGtkSplitter(m_gtkSplitter);
	to->SetLeftChild(m_leftChild);
	to->SetRightChild(m_rightChild);
	to->SetGtkLeftFrame(m_leftFrame);
	to->SetGtkLeftFrame(m_rightFrame);
	to->SetPane(m_pane);
		
	if(m_type == WDEIPaneTreeItem::WDEPaneTreeItemContainer)
	{
		GtkWidget *toContainer;
		to->GetGtkContainer(&toContainer);
		gtk_box_pack_start(GTK_BOX(toContainer), m_gtkSplitter, TRUE, TRUE, 0);
	}
}

/* void split (in PRInt32 align, in PRInt32 sideIsNew, in WDEIPaneTreeItem newItem, out WDEIPaneTreeItem oldItem); */
NS_IMETHODIMP WDEPaneTreeItem::Split(PRInt32 align, PRInt32 sideIsNew, WDEIPaneTreeItem *newLeaf, WDEIPaneTreeItem **oldItem)
{
	nsCOMPtr<WDEIPaneTreeItem> oldLeaf = do_CreateInstance(WDEPANETREEITEM_CONTRACT_ID);
	*oldItem = oldLeaf;
	NS_IF_ADDREF(*oldItem);
	
	nsCOMPtr<WDEIPaneTreeItem> newLeft = NULL;
	nsCOMPtr<WDEIPaneTreeItem> newRight = NULL;
	
  if(sideIsNew == WDEPaneTreeItem::WDEPaneTreeItemLeft)
  {
    newLeft = newLeaf;
    newRight = oldLeaf;
  }
  else
  {
    newLeft = oldLeaf;
    newRight = newLeaf;
  }
	
	splitCopyTo(oldLeaf);

	//remove child of container	
	if(m_type == WDEIPaneTreeItem::WDEPaneTreeItemContainer) gtk_container_remove(GTK_CONTAINER(m_gtkContainer), m_gtkSplitter);
	else if(m_pane)
	{
		GtkWidget *paneContainer;
		m_pane->GetGtkContainer(&paneContainer);
		gtk_container_remove(GTK_CONTAINER(m_gtkContainer), paneContainer);
	}

	//setup this node as the new parent
	nsCOMPtr<WDEIPaneTreeItem> tThis = this;
	newLeft->SetParent(tThis);
	newRight->SetParent(tThis);
		
	m_leftChild = newLeft;
	m_rightChild = newRight;
	m_type = WDEIPaneTreeItem::WDEPaneTreeItemContainer;
	m_alignment = align;
	m_pane = NULL;

	//make new splitter	
	if(m_alignment == WDEIPaneTreeItem::WDEPaneTreeItemHorizontal)
		m_gtkSplitter = gtk_hpaned_new();
	else m_gtkSplitter = gtk_vpaned_new();
	gtk_widget_show(m_gtkSplitter);
	gtk_box_pack_start(GTK_BOX(m_gtkContainer), m_gtkSplitter, TRUE, TRUE, 0);

	//add children to splitter
	GtkWidget *leftWidget, *rightWidget;	
  newLeft->GetGtkContainer(&leftWidget);
  newRight->GetGtkContainer(&rightWidget);

  /*** GET SUB-PANE NAMES ***/
  char *lframe;
  char *rframe;
  nsCOMPtr<WDEIPane> lpane= 0, rpane= 0;
  newLeft->GetPane(getter_AddRefs(lpane));
  newRight->GetPane(getter_AddRefs(rpane));
  if (lpane)
    lpane->GetName(&lframe);
  else
    lframe= g_strdup("");
  if (rpane)
    rpane->GetName(&rframe);
  else
    rframe= g_strdup("");
  /**************************/
  
	if (m_leftFrame) gtk_widget_destroy(m_leftFrame);
	if (m_rightFrame) gtk_widget_destroy(m_leftFrame);
	m_leftFrame = gtk_frame_new(lframe);
	g_free(lframe);
	gtk_frame_set_shadow_type(GTK_FRAME(m_leftFrame),
	  WDEPANETREEITEM_DEFAULT_GTK_FRAME_SHADOW_TYPE);
	m_rightFrame = gtk_frame_new(rframe);
	g_free(rframe);
	gtk_frame_set_shadow_type(GTK_FRAME(m_rightFrame),
	  WDEPANETREEITEM_DEFAULT_GTK_FRAME_SHADOW_TYPE);
	gtk_widget_show(m_leftFrame);
	gtk_widget_show(m_rightFrame);
	gtk_container_add(GTK_CONTAINER(m_leftFrame), leftWidget);
	gtk_container_add(GTK_CONTAINER(m_rightFrame), rightWidget);
	
	gtk_paned_pack1(GTK_PANED(m_gtkSplitter), m_leftFrame, TRUE, TRUE);
  gtk_paned_pack2(GTK_PANED(m_gtkSplitter), m_rightFrame, TRUE, TRUE);
  
  return NS_OK;
}

/* [noscript] void Refresh (); */
NS_IMETHODIMP WDEPaneTreeItem::Refresh() {
  if (!m_pane) return NS_OK;
  nsCOMPtr<WDEIPaneTreeItem> dad= 0;
  nsCOMPtr<WDEIPaneTreeItem> dadLeft= 0;
  GetParent(getter_AddRefs(dad));
  bool isLeft;
  dad->GetLeftChild(getter_AddRefs(dadLeft));
  isLeft= (dadLeft== this);
  char *paneName;
  m_pane->GetName(&paneName);
  GtkWidget *frame;
  if (isLeft)
    dad->GetGtkLeftFrame(&frame);
  else
    dad->GetGtkRightFrame(&frame);
  gtk_frame_set_label(GTK_FRAME(frame), paneName);
  PL_strfree(paneName);
  // Auto compute best splitter size
  GtkWidget *dadSplitter;
  dad->GetGtkSplitter(&dadSplitter);
  gtk_paned_set_position(GTK_PANED(dadSplitter), -1);
  return NS_OK;
}

//*****************************************************************
//pane
//*****************************************************************

NS_IMPL_ISUPPORTS3(WDEPane, WDEIPane, WDEIEventListener, WDEIEventHandler)

WDEPane::WDEPane()
{
  NS_INIT_ISUPPORTS();
  WDE_INIT_LISTENER
  /* member initializers and constructor code */
  
  m_name = "";
  m_paneTree = NULL;
  m_gtkContainer = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(m_gtkContainer);
/** DISABLED SINCE PaneTreeItem now displays the name of its panes
  m_paneLabel= gtk_label_new(m_name.c_str());
  gtk_widget_show(m_paneLabel);
  GtkWidget *eventBox= gtk_event_box_new();
  gtk_widget_show(eventBox);
  gtk_container_add(GTK_CONTAINER(eventBox), m_paneLabel);
  gtk_box_pack_start(GTK_BOX(m_gtkContainer), eventBox, FALSE, FALSE, 0);
**/
  m_notebook = gtk_notebook_new();
	gtk_widget_show(m_notebook);
  gtk_box_pack_start(GTK_BOX(m_gtkContainer), m_notebook, TRUE, TRUE, 0);

  m_popupMenu= do_CreateInstance(WDEPOPUPMENU_CONTRACT_ID);
  //! Action codes 100 - 400 reserved for Hidden tabs
  m_popupMenu->AddItem("Tab\\Float", 1);
  m_popupMenu->AddItem("Tab\\Hide", 2);
  m_popupMenu->AddItem("Tab\\Move left", 3);
  m_popupMenu->AddItem("Tab\\Move right", 4);
  m_popupMenu->AttachMenu(m_notebook);
  nsCOMPtr<WDEIEventSender> evs= do_QueryInterface(m_popupMenu);
  ListenTo(evs);
}

WDEPane::~WDEPane()
{
  /* destructor code */
}

NS_IMETHODIMP WDEPane::HandleEvent(WDEIEvent *ev) {
  WDE_EVENT_TO_LOCAL_VARS(ev, evCode, evStr, evAttr, evPtr, evSender);
  int idx;
  switch (evCode) {
    case WDEIEvent::PopupMenuItemSelected:
      switch (evAttr) {
        case 1: // Tab\\Float
          GetActiveTabIndex(&idx);
          if (idx!= -1)
            FloatTab(idx);
          break;
        case 2: // Tab\\Hide
          cerr << "TO DO: Support tab hiding" << endl;
          /** DISABLED
          GetActiveTabIndex(&idx);
          if (idx!= -1) {
            char *tabName, *tabHide;
            m_tabs[idx].tab->GetName(&tabName);
            tabHide= g_strconcat("Hidden Tabs\\", tabName, NULL);
            g_free(tabName);
            m_popupMenu->AddItem(tabHide, 100+ idx);
            g_free(tabHide);
          }
          **/
          break;
        case 3: // Tab\\Move left
          cerr<< "TO DO: Support Tab\\Move left" << endl;
          break;
        case 4: // Tab\\Move right
          cerr<< "TO DO: Support Tab\\Move right" << endl;
          break;
      }
      break;
    default:
      break;
  }
  return NS_OK;
}

/* attribute string name; */
NS_IMETHODIMP WDEPane::GetName(char * *aName)
{
  XPCOM_STR_GETTER(m_name.c_str(), aName);
}
NS_IMETHODIMP WDEPane::SetName(const char * aName)
{
/** DISABLED
  gtk_label_set_text(GTK_LABEL(m_paneLabel), aName);
**/
  m_name= aName;
  m_paneTree->Refresh();
  return NS_OK;
}

/* attribute WDEIPaneTreeItem paneTree; */
NS_IMETHODIMP WDEPane::GetPaneTree(WDEIPaneTreeItem * *aPaneTree)
{
  XPCOM_COMPTR_GETTER(m_paneTree, aPaneTree);
}
NS_IMETHODIMP WDEPane::SetPaneTree(WDEIPaneTreeItem * aPaneTree)
{
  XPCOM_SIMPLE_SETTER(m_paneTree, aPaneTree);
}

/* [noscript] attribute GtkWidget gtkContainer; */
NS_IMETHODIMP WDEPane::GetGtkContainer(GtkWidget * *aGtkContainer)
{
  XPCOM_PTR_GETTER(m_gtkContainer, aGtkContainer);
}

NS_IMETHODIMP WDEPane::SetGtkContainer(GtkWidget * aGtkContainer)
{
  XPCOM_SIMPLE_SETTER(m_gtkContainer, aGtkContainer);
}

/* WDEIPaneTab getTab (in PRInt32 idx); */
NS_IMETHODIMP WDEPane::GetTab(PRInt32 idx, WDEIPaneTab **_retval)
{
  if (idx>= (int) m_tabs.size() || idx< 0)
    return NS_ERROR_INVALID_ARG;
  *_retval= m_tabs[idx].tab;
  NS_IF_ADDREF(*_retval);
  return NS_OK;
//  GtkWidget *nthTabWidget = gtk_notebook_get_nth_page(GTK_NOTEBOOK(m_notebook), idx);
//  *_retval = (WDEIPaneTab *) gtk_object_get_user_data(GTK_OBJECT(nthTabWidget));
//  NS_IF_ADDREF(*_retval);
//  return NS_OK;
}

/* void removeTab (in WDEIPaneTab tab); */
NS_IMETHODIMP WDEPane::RemoveTab(WDEIPaneTab *tab)
{
  vector<tabInfo>::iterator i= find(m_tabs.begin(), m_tabs.end(), tab);
  if (i== m_tabs.end()) return NS_OK;
  // force defloat
  if (i->isFloat)
    gtk_widget_destroy(i->window);
  GtkWidget *tabContainer;
  i->tab->GetGtkContainer(&tabContainer);
  int idx= gtk_notebook_page_num(GTK_NOTEBOOK(m_notebook), tabContainer);
  gtk_notebook_remove_page(GTK_NOTEBOOK(m_notebook), idx);
  m_tabs.erase(i);
  return NS_OK;
//  GtkWidget *tabContainer;
//  tab->GetGtkContainer(&tabContainer);
//  int idx = gtk_notebook_page_num(GTK_NOTEBOOK(m_notebook), tabContainer);
//  gtk_notebook_remove_page(GTK_NOTEBOOK(m_notebook), idx);
//
//  return NS_OK;
}

/* void appendTab (in WDEIPaneTab tab); */
NS_IMETHODIMP WDEPane::AppendTab(WDEIPaneTab *tab)
{
  tabInfo ti;
  ti.tab= do_QueryInterface(tab);
  ti.isFloat= false;
  ti.window= 0;
  m_tabs.push_back(ti);
  
  GtkWidget *tabContainer;
  char *tabName;
  ti.tab->GetGtkContainer(&tabContainer);
  ti.tab->GetName(&tabName);
	GtkWidget *label = gtk_label_new(tabName);
	gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(m_notebook), tabContainer, label);
  PL_strfree(tabName);
  return NS_OK;  
  
/*
  GtkWidget *tabContainer;
  char *tabName;
  
  tab->GetGtkContainer(&tabContainer);
  tab->GetName(&tabName);
	GtkWidget *label = gtk_label_new(tabName);
	gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(m_notebook), tabContainer, label);
  PL_strfree(tabName);
  
  return NS_OK;
*/
}

/* long getActiveTabIndex (); */
NS_IMETHODIMP WDEPane::GetActiveTabIndex(PRInt32 *_retval) {
  int idx= gtk_notebook_get_current_page(GTK_NOTEBOOK(m_notebook));
  GtkWidget *tabContainer= gtk_notebook_get_nth_page(GTK_NOTEBOOK(m_notebook), idx);
  return GetTabIndexByContainer(tabContainer, _retval);
}

NS_IMETHODIMP WDEPane::GetPopupMenu(WDEIPopupMenu * *aPopupMenu) {
  XPCOM_COMPTR_GETTER(m_popupMenu, aPopupMenu);
}

/* boolean isFloatingTab (in long idx); */
NS_IMETHODIMP WDEPane::IsFloatingTab(PRInt32 idx, PRBool *_retval)
{
  if (idx>= (int) m_tabs.size() || idx< 0)
    return NS_ERROR_INVALID_ARG;
  *_retval= m_tabs[idx].isFloat;
  return NS_OK;
}

/* void floatTab (in long idx); */
NS_IMETHODIMP WDEPane::FloatTab(PRInt32 idx)
{
  if (idx>= (int) m_tabs.size() || idx< 0)
    return NS_ERROR_INVALID_ARG;
  if (m_tabs[idx].isFloat) return NS_OK;
  m_tabs[idx].isFloat= true;
  char *tabName, *tabTitle;
  m_tabs[idx].tab->GetName(&tabName);
  tabTitle= g_strconcat(m_name.c_str(), "::", tabName, NULL);
  PL_strfree(tabName);
  m_tabs[idx].window= gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_widget_show(m_tabs[idx].window);
  gtk_window_set_title(GTK_WINDOW(m_tabs[idx].window), tabTitle);
  PL_strfree(tabTitle);
  int width= 300;
  int height= 300;
  gtk_window_set_default_size(GTK_WINDOW(m_tabs[idx].window), width, height);
  GtkWidget *tabContainer;
  m_tabs[idx].tab->GetGtkContainer(&tabContainer);
  gtk_widget_ref(tabContainer);
  int noteidx= gtk_notebook_page_num(GTK_NOTEBOOK(m_notebook), tabContainer);
  gtk_notebook_remove_page(GTK_NOTEBOOK(m_notebook), noteidx);
  gtk_container_add(GTK_CONTAINER(m_tabs[idx].window), tabContainer);
  // Auto defloat on window destroy
  gtk_signal_connect(GTK_OBJECT(m_tabs[idx].window), "destroy",
    GTK_SIGNAL_FUNC(WDEPaneDefloatTab), this);
  return NS_OK;
}

/* [noscript] void DefloatByContainer (in GtkWidget container); */
NS_IMETHODIMP WDEPane::DefloatByContainer(GtkWidget * container) {
  int idx;
  GetTabIndexByContainer(container, &idx);
  if (idx== -1) return NS_ERROR_INVALID_ARG;
  if (!m_tabs[idx].isFloat) return NS_OK;
  m_tabs[idx].isFloat= false;
  GtkWidget *tabContainer;
  m_tabs[idx].tab->GetGtkContainer(&tabContainer);
  gtk_widget_ref(tabContainer);
  gtk_container_remove(GTK_CONTAINER(m_tabs[idx].window), tabContainer);
  char *tabName;
  m_tabs[idx].tab->GetName(&tabName);
  GtkWidget *label= gtk_label_new(tabName);
  gtk_widget_show(label);
  gtk_notebook_append_page(GTK_NOTEBOOK(m_notebook), tabContainer, label);
  PL_strfree(tabName);
  // TO DO: reorder tabs as before
  return NS_OK;  
}

/* [noscript] long GetTabIndexByContainer (in GtkWidget container); */
NS_IMETHODIMP WDEPane::GetTabIndexByContainer(GtkWidget * container, PRInt32 *_retval) {
  vector<tabInfo>::iterator i;
  for (i= m_tabs.begin(); i< m_tabs.end(); ++i) {
    GtkWidget *tabContainer;
    i->tab->GetGtkContainer(&tabContainer);
    if (tabContainer== container) {
      *_retval= i- m_tabs.begin();
      return NS_OK;
    }
  }
  *_retval= -1;
  return NS_OK;
}

extern "C" void WDEPaneDefloatTab(GtkObject *obj, WDEIPane *_pane) {
  nsCOMPtr<WDEIPane> pane= do_QueryInterface(_pane);
  GtkWidget *container= GTK_WIDGET(gtk_container_children(GTK_CONTAINER(obj))->data);
  pane->DefloatByContainer(container);
}

//*****************************************************************
//pane tab
//*****************************************************************

NS_IMPL_ISUPPORTS1(WDEPaneTab, WDEIPaneTab)

WDEPaneTab::WDEPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
  
  m_gtkContainer = gtk_hbox_new(FALSE, 0);
	gtk_widget_show(m_gtkContainer);

  //this needs to be released somehow...  
  nsCOMPtr<WDEIPaneTab> queriedThis(this);
  gtk_object_set_user_data(GTK_OBJECT(m_gtkContainer), queriedThis.get());
  WDEIPaneTab *t = queriedThis.get();
  NS_IF_ADDREF(t);
  
  m_pane = NULL;
  m_name = "";
}

WDEPaneTab::~WDEPaneTab()
{
  /* destructor code */
}

/* attribute WDEIPane pane; */
NS_IMETHODIMP WDEPaneTab::GetPane(WDEIPane * *aPane)
{
  XPCOM_COMPTR_GETTER(m_pane, aPane);
}
NS_IMETHODIMP WDEPaneTab::SetPane(WDEIPane * aPane)
{
  XPCOM_SIMPLE_SETTER(m_pane, aPane);
}

/* [noscript] attribute GtkWidget gtkContainer; */
NS_IMETHODIMP WDEPaneTab::GetGtkContainer(GtkWidget * *aGtkContainer)
{
  XPCOM_PTR_GETTER(m_gtkContainer, aGtkContainer);
}
NS_IMETHODIMP WDEPaneTab::SetGtkContainer(GtkWidget * aGtkContainer)
{
  XPCOM_SIMPLE_SETTER(m_gtkContainer, aGtkContainer);
}

/* attribute string name; */
NS_IMETHODIMP WDEPaneTab::GetName(char * *aName)
{
  XPCOM_STR_GETTER(m_name.c_str(), aName);
}
NS_IMETHODIMP WDEPaneTab::SetName(const char * aName)
{
  XPCOM_SIMPLE_SETTER(m_name, aName);
}
