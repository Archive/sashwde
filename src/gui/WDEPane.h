#ifndef _WDEPANE_H
#define _WDEPANE_H

#include <gtk/gtk.h>
#include "WDEIGUI.h"
#include "WDEIPopupMenu.h"
#include "nsCOMPtr.h"
#include <string>
#include "WDEEvents.h"

//3B92946F-D39A-4135-887C-D71BCAE57776
#define WDEPANETREEITEM_CID {0x3B92946F, 0xD39A, 0x4135, {0x88, 0x7C, 0xD7, 0x1B, 0xCA, 0xE5, 0x77, 0x76}}
NS_DEFINE_CID(kWDEPaneTreeItemCID, WDEPANETREEITEM_CID);

#define WDEPANETREEITEM_CONTRACT_ID "@gnome.org/SashWDE/WDEPaneTreeItem;1"

// GTK_SHADOW_NONE, GTK_SHADOW_IN
#define WDEPANETREEITEM_DEFAULT_GTK_FRAME_SHADOW_TYPE GTK_SHADOW_IN

class WDEPaneTreeItem : public WDEIPaneTreeItem
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPANETREEITEM

  WDEPaneTreeItem();
  virtual ~WDEPaneTreeItem();
  /* additional members */
  
protected:
  int m_alignment;
  int m_type;
  GtkWidget *m_gtkContainer;
  GtkWidget *m_gtkSplitter;
  nsCOMPtr<WDEIPaneTreeItem> m_leftChild;
  nsCOMPtr<WDEIPaneTreeItem> m_rightChild;
  nsCOMPtr<WDEIPaneTreeItem> m_parent;
  nsCOMPtr<WDEIPane> m_pane;
  
  //! Utility pointers to the left/right frame
  GtkWidget *m_leftFrame;
  GtkWidget *m_rightFrame;

  void splitCopyTo(WDEIPaneTreeItem *to);
};


//2924B78C-6684-4226-BBE3-1783375C5354
#define WDEPANE_CID {0x2924B78C, 0x6684, 0x4226, {0xBB, 0xE3, 0x17, 0x83, 0x37, 0x5C, 0x53, 0x54}}
NS_DEFINE_CID(kWDEPaneCID, WDEPANE_CID);

#define WDEPANE_CONTRACT_ID "@gnome.org/SashWDE/WDEPane;1"

class WDEPane : public WDEIPane, 
    public WDEIEventListener, public WDEIEventHandler
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPANE
  WDE_DECL_LISTENER

  WDEPane();
  virtual ~WDEPane();
  /* additional members */
  
protected:
  struct tabInfo {
    nsCOMPtr<WDEIPaneTab> tab;
    bool isFloat;
    GtkWidget *window;
    operator WDEIPaneTab *() {
      return tab.get();
    };
  };
  string m_name;
  nsCOMPtr<WDEIPaneTreeItem> m_paneTree;
  nsCOMPtr<WDEIPopupMenu> m_popupMenu;
  vector<tabInfo> m_tabs;
  GtkWidget *m_gtkContainer;
  GtkWidget *m_notebook;
  GtkWidget *m_paneLabel;
  
};


//6DE5C7D7-70B6-43A7-8254-3B6EED654A17
#define WDEPANETAB_CID {0x6DE5C7D7, 0x70B6, 0x43A7, {0x82, 0x54, 0x3B, 0x6E, 0xED, 0x65, 0x4A, 0x17}}
NS_DEFINE_CID(kWDEPaneTabCID, WDEPANETAB_CID);

#define WDEPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEPaneTab;1"

class WDEPaneTab : public WDEIPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPANETAB

  WDEPaneTab();
  virtual ~WDEPaneTab();
  /* additional members */

protected:
  string m_name;
  nsCOMPtr<WDEIPane> m_pane;
  GtkWidget *m_gtkContainer;
};

extern "C" void WDEPaneDefloatTab(GtkObject *obj, WDEIPane *_pane);

#endif
