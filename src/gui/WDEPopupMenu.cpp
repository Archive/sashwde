#include "WDEPopupMenu.h"
#include "xpcomtools.h"
//#include "WDEApp.h"

NS_IMPL_ISUPPORTS2(WDEPopupMenu, WDEIPopupMenu, WDEIEventSender)

WDEPopupMenu::WDEPopupMenu() {
  NS_INIT_ISUPPORTS();
  WDE_INIT_SENDER

  m_rootMenu= gtk_menu_new();
  gtk_widget_show(m_rootMenu);
  gtk_signal_connect(GTK_OBJECT(m_rootMenu), "selection-done",
	  GTK_SIGNAL_FUNC(WDEPopupMenuClickHandler), 0);
}

WDEPopupMenu::~WDEPopupMenu() {
  gtk_widget_destroy(m_rootMenu);
}

/* void AddItem (in string menuPath, in long actionCode); */
NS_IMETHODIMP WDEPopupMenu::AddItem(const char *menuPath, PRInt32 actionCode) {
  char **path;
  GtkWidget *menuTrav= m_rootMenu;
  GList *items;
  int i;
  path= g_strsplit(menuPath, "\\", 0);
  for (i= 0; path[i]!= NULL; ++i) {
    // find whether the item already exists
    items= gtk_container_children(GTK_CONTAINER(menuTrav));
    while (items!= NULL) {
      if (GTK_IS_MENU_ITEM(items->data)) {
        gchar *name= (gchar *) gtk_object_get_data(GTK_OBJECT(items->data), "itemname");
        if (g_strcasecmp(name, path[i])== 0) {
          // advance menuTrav
          menuTrav= GTK_MENU_ITEM(items->data)->submenu;
          break;
        }
      }
      items= items->next;
    }
    if (items== NULL) { // not found, so add it
      GtkWidget *treeItem;
      int code= -1;
      treeItem= gtk_menu_item_new_with_label(path[i]);
      gtk_widget_show(treeItem);
      gtk_object_set_data(GTK_OBJECT(treeItem), "itemname", g_strdup(path[i]));
      if (path[i+ 1]== NULL) {
        code= actionCode;
        gtk_signal_connect(GTK_OBJECT(treeItem), "activate", GTK_SIGNAL_FUNC(WDEPopupMenuItemSelected), this);
      }
      gtk_object_set_data(GTK_OBJECT(treeItem), "itemaction", GINT_TO_POINTER(code));
      gtk_signal_connect(GTK_OBJECT(treeItem), "destroy", GTK_SIGNAL_FUNC(WDEPopupMenuDestroyItem), 0);
      // maybe add a data destroy handler, hmm?
      gtk_menu_append(GTK_MENU(menuTrav), treeItem);
      if (path[i+ 1]!= NULL) {
        menuTrav= gtk_menu_new();
        gtk_widget_show(menuTrav);
        gtk_menu_item_set_submenu(GTK_MENU_ITEM(treeItem), menuTrav);
      }
    }
  }
  g_strfreev(path);
  return NS_OK;
}

/* long ItemActionCode (in string menuPath); */
NS_IMETHODIMP WDEPopupMenu::ItemActionCode(const char *menuPath, PRInt32 *_retval) {
  char **path;
  GtkWidget *menuTrav= m_rootMenu;
  GList *items;
  int i;
  path= g_strsplit(menuPath, "\\", 0);
  for (i= 0; path[i]!= NULL; ++i) {
    // find whether the item already exists
    items= gtk_container_children(GTK_CONTAINER(menuTrav));
    while (items!= NULL) {
      if (GTK_IS_MENU_ITEM(items->data)) {
        gchar *name= (gchar *) gtk_object_get_data(GTK_OBJECT(items->data), "itemname");
        if (g_strcasecmp(name, path[i])== 0) {
          // advance menuTrav
          menuTrav= GTK_MENU_ITEM(items->data)->submenu;
          break;
        }
      }
      items= items->next;
    }
    if (items!= NULL && path[i+ 1]== NULL) {
      g_strfreev(path);
      *_retval= GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(items->data), "itemaction"));
      return NS_OK;
    }
  }
  g_strfreev(path);
  *_retval= -1;
  return NS_OK;
}

/* boolean ContainsItem (in string menuPath); */
NS_IMETHODIMP WDEPopupMenu::ContainsItem(const char *menuPath, PRBool *_retval) {
  PRInt32 ac;
  ItemActionCode(menuPath, &ac);
  *_retval= (ac != -1);
  return NS_OK;
}

/* void RemoveItem (in string menuPath); */
NS_IMETHODIMP WDEPopupMenu::RemoveItem(const char *menuPath) {
  char **path;
  GtkWidget *menuTrav= m_rootMenu;
  GList *items;
  int i;
  vector<pair<GtkWidget *, GtkWidget* > > stagePath;
  vector<bool> removeStage;
  path= g_strsplit(menuPath, "\\", 0);
  for (i= 0; path[i]!= NULL; ++i) {
    stagePath.push_back(pair<GtkWidget *, GtkWidget *>(menuTrav, NULL));
    // find whether the item already exists
    items= gtk_container_children(GTK_CONTAINER(menuTrav));
    int stageCount= 0;
    bool stageFound= false;
    while (items!= NULL) {
      if (GTK_IS_MENU_ITEM(items->data)) {
        ++stageCount;
        gchar *name= (gchar *) gtk_object_get_data(GTK_OBJECT(items->data), "itemname");
        if (g_strcasecmp(name, path[i])== 0) {
          // advance menuTrav
          stagePath.rbegin()->second= GTK_WIDGET(items->data);
          menuTrav= GTK_MENU_ITEM(items->data)->submenu;
          stageFound= true;
        }
      }
      items= items->next;
    }
    if (!stageFound) {
      g_strfreev(path);
      return NS_OK;
    }
    removeStage.push_back(stageCount== 1);
  }
  vector<bool>::reverse_iterator ri= removeStage.rbegin();
  vector<pair<GtkWidget *, GtkWidget* > >::reverse_iterator rs= stagePath.rbegin();
  while (ri< removeStage.rend()) {
    // remove item
    GtkWidget *menu= rs->first;
    GtkWidget *item= rs->second;
    gtk_container_remove(GTK_CONTAINER(menu), item);
    if (!(*ri)) break;
    ++ri;
    ++rs;
  }
  g_strfreev(path);
  return NS_OK;
}

/* void RemoveItemByCode (in long actionCode); */
NS_IMETHODIMP WDEPopupMenu::RemoveItemByCode(PRInt32 actionCode) {
  return NS_ERROR_NOT_IMPLEMENTED;
}

/* [noscript] void AttachMenu (in GtkWidgetPM holder); */
NS_IMETHODIMP WDEPopupMenu::AttachMenu(GtkWidget * holder) {
  gtk_signal_connect_object(GTK_OBJECT(holder), "button_press_event",
	  GTK_SIGNAL_FUNC(WDEPopupMenuDisplayHandler), GTK_OBJECT(m_rootMenu));
	return NS_OK;
}

/* [noscript] void DetachMenu (in GtkWidgetPM holder); */
NS_IMETHODIMP WDEPopupMenu::DetachMenu(GtkWidget * holder) {
  gtk_signal_disconnect_by_func(GTK_OBJECT(holder),
    GTK_SIGNAL_FUNC(WDEPopupMenuDisplayHandler),
    m_rootMenu);
  return NS_OK;
}

/** GTK CALLBACKS **/

extern "C" gint WDEPopupMenuDisplayHandler(GtkWidget *widget, GdkEvent *event) {
  // Code taken from GTK GtkMenu API Reference 
  GtkMenu *menu;
  GdkEventButton *event_button;

  g_return_val_if_fail(widget!= NULL, FALSE);
  g_return_val_if_fail(GTK_IS_MENU(widget), FALSE);
  g_return_val_if_fail(event!= NULL, FALSE);
  menu= GTK_MENU(widget);

  if (event->type== GDK_BUTTON_PRESS) {
    event_button= (GdkEventButton *) event;
    if (event_button->button== 3) {
	    gtk_menu_popup(menu, NULL, NULL, NULL, NULL, 
			  event_button->button, event_button->time);
	    return TRUE;
	  }
  }
  return FALSE;
}

extern "C" void WDEPopupMenuClickHandler(GtkMenuShell *menushell, gpointer user_data) {
  // Everything here happens AFTER the menu clicking
  // events have been dispatched
  // use only if really necessary
  // this handler is set up in AddItem
}  

extern "C" void WDEPopupMenuItemSelected(GtkMenuItem *item, WDEIPopupMenu *user_data) {
  // user data must be a WDEIPopupMenu
  nsCOMPtr<WDEIEventSender> snd= do_QueryInterface(user_data);
  char *itemName= (char *) gtk_object_get_data(GTK_OBJECT(item), "itemname");
  int itemActionCode= GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(item), "itemaction"));
  // check for code of -1; we don't really want to send out events
  // in that case; itemName must also be non-null
  if (itemActionCode!= -1 && itemName)
    snd->BroadcastEvent(
      WDEIEvent::PopupMenuItemSelected,
      itemName, itemActionCode, 0);
}

extern "C" void WDEPopupMenuDestroyItem(GtkObject *obj, gpointer *user_data) {
  // this handler must be connected to avoid leaking
  // the itemname string
  if (GTK_IS_MENU_ITEM(obj)) {
    // free item name string, because it was STRDUPED in AddItem
    char *itemname= (char *) gtk_object_get_data(obj, "itemname");
    if (itemname) g_free(itemname);
  }
}
