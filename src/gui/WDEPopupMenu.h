#ifndef _WDEPOPUPMENU_H
#define _WDEPOPUPMENU_H

#include <gtk/gtk.h>
#include "WDEIPopupMenu.h"
#include "WDEIGUI.h"
#include "nsCOMPtr.h"
#include "WDEEvents.h"

//de0f5ea7-3112-4d40-b753-642ff7f51d33
#define WDEPOPUPMENU_CID {0xde0f5ea7, 0x3112, 0x4d40, {0xb7, 0x53, 0x64, 0x2f, 0xf7, 0xf5, 0x1d, 0x33}}
NS_DEFINE_CID(kWDEPopupMenuCID, WDEPOPUPMENU_CID);

#define WDEPOPUPMENU_CONTRACT_ID "@gnome.org/SashWDE/WDEPopupMenu;1"

class WDEPopupMenu: public WDEIPopupMenu,
                    public WDEIEventSender {

  public:
    NS_DECL_ISUPPORTS
    NS_DECL_WDEIPOPUPMENU
    WDE_DECL_SENDER

    WDEPopupMenu();
    virtual ~WDEPopupMenu();
    /* additional members */
    
  private:
    GtkWidget *m_rootMenu;
};

/** GTK CALLBACKS USED IN WDEIPopupMenu **/
extern "C" gint WDEPopupMenuDisplayHandler(GtkWidget *widget, GdkEvent *event);
extern "C" void WDEPopupMenuClickHandler(GtkMenuShell *menushell, gpointer user_data);
extern "C" void WDEPopupMenuItemSelected(GtkMenuItem *item, WDEIPopupMenu *user_data);
extern "C" void WDEPopupMenuDestroyItem(GtkObject *obj, gpointer *user_data);

#endif
