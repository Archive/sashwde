
/***************************************************************
    SashWDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include <stdlib.h>
#include <gnome.h>
#include <glade/glade.h>

#include "WDEApp.h"
#include "WDEDevKit.h"

#include "WDETabWindow.h"
#include "WDEProjectBrowser.h"
#include "WDEITreeItem.h"
#include "WDEIExtension.h"
#include "WDEIAction.h"
#include "WDEIInstallPage.h"
#include "WDEITreeFileItem.h"
#include "WDEITreeGladeFormItem.h"
#include "nsCRT.h"
#include "GUITools.h"
#include <iostream>
#include "WDEIEditorDM.h"
#include "WDEITextEditorDM.h"
#include "UIManager.h"
#include "WDEMainWindow.h"
#include "WDEApp.h"
#include "sashILocationDKLocation.h"
#include "GUIFileCallbacks.h"
#include "WDEProjectSettings.h"
#include "WDEDesignViewDlg.h"
#include "WDEActiveFileCallbacks.h"

static const char *inst_entry2_1strings[] = {
  "Text", "License", "Custom", "PostSecurity", "Failure", "Security", "Success", "Script"
};

static const char *file_entry2_1strings[] = {
  "Weblication", "Cache", "WeblicationPath", "SharedLocation", "Server", "Extension"
};

static const char *WDEPROJECTBROWSER_GLADE_FILENAME = "projectbrowser.glade";
static const char *WDEPROJECTBROWSER_GLADE_DIALOG = "projectbrowser";
static const char *WDEPROJECTBROWSER_GLADE_NOTEBOOK = "notebook";

static const char *WDEPROJECTBROWSER_GLADE_FILES_TREE = "filestree";
static const char *WDEPROJECTBROWSER_GLADE_OPEN_FILES_TREE = "openfiles";

WDEProjectBrowser::WDEProjectBrowser() {
  m_proj_select= NULL;
  m_open_select= NULL;
  m_proj_vis= true;
  m_open_vis= false;
  m_activeTab= 0;
}

WDEProjectBrowser::~WDEProjectBrowser() {
}

bool WDEProjectBrowser::HandleEvent(WDEIEvent *event) {
	int id;
	event->GetID(&id);
	switch(id) {
	  case WDEIEvent::WDENewProject:
	    gtk_tree_remove_items(GTK_TREE(m_filesTree), GTK_TREE(m_filesTree)->children);
	    InitProject();
      ResetProjectInfo();
		  break;
	  case WDEIEvent::WDECloseProject:
	    gtk_tree_remove_items(GTK_TREE(m_filesTree), GTK_TREE(m_filesTree)->children);
	    break;
	  case WDEIEvent::ProjectNameSet:
    case WDEIEvent::ProjectTitleSet:
      ResetProjectInfo();
      break;
	  case WDEIEvent::ProjectLoaded:
	    gtk_tree_remove_items(GTK_TREE(m_filesTree), GTK_TREE(m_filesTree)->children);
	    InitProject();
      ResetProjectInfo();
      RenderFunctionList();
	    break;
	  case WDEIEvent::TextEditorDMTextModified:
	    WDEIEventable *caller;
	    WDEITextEditorDM *tdm;
	    event->GetCaller(&caller);
	    if (NS_SUCCEEDED(caller->QueryInterface(NS_GET_IID(WDEITextEditorDM), (void **) &tdm))) {
	      RefreshFunctionList(tdm);
	      RenderFunctionList();
	    }
      NS_IF_RELEASE(tdm);
	    NS_IF_RELEASE(caller);
	    break;
		default:
		  UpdateFilesTree(id, event);
	};
	return false;
}

GtkWidget *WDEProjectBrowser::AddTreeItem(GtkWidget *tree, WDEITreeItem *item, int pos, int tab) {
	int length;
	WDEITreeItem *child = NULL;
	char *name;
	GtkWidget *childW = NULL;
	GtkWidget *childTree= NULL;
  if (!tree) return NULL;
  switch (tab) {
    case 0:
      gtk_signal_connect(
        GTK_OBJECT(tree),
        (gchar *) "select-child",
        GTK_SIGNAL_FUNC(WDEProjectBrowser_ItemSelectProj), this);
      break;
    case 2:
      gtk_signal_connect(
        GTK_OBJECT(tree),
        (gchar *) "select-child",
        GTK_SIGNAL_FUNC(WDEProjectBrowser_ItemSelectOpen), this);
      break;
  }
	item->GetName(&name);
	childW= gtk_tree_item_new_with_label(name);
	nsCRT::free(name);
  gtk_signal_connect_full(
    GTK_OBJECT(childW),
    (gchar *) "button-press-event",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_ItemDoubleClick), 
    NULL, this, NULL, FALSE, GTK_RUN_LAST);
	gtk_object_set_user_data(GTK_OBJECT(childW), (gpointer) item);
	if (pos== -1)
	  gtk_tree_append(GTK_TREE(tree), childW);
	else
	  gtk_tree_insert(GTK_TREE(tree), childW, pos);
	gtk_widget_show(childW);
	item->GetLength(&length);
	if (length> 0) {
	  childTree= gtk_tree_new();
		gtk_tree_item_set_subtree(GTK_TREE_ITEM(childW), childTree);
	}
	for(int i= 0; i< length; ++i) {		
		child = NULL;
		item->GetChild(i, &child);
		if (child) AddTreeItem(childTree, child, -1, tab);
		NS_IF_RELEASE(child);
	}
	return childW;
}

void WDEProjectBrowser::ResetProjectInfo() {
  WDEIWDE *wdeDM = NULL;
	WDEIProject *projDM = NULL;
	GtkWidget *item = NULL;
	char *proj_name = NULL;
	WDEApp::GetWDE(&wdeDM);
	wdeDM->GetActiveProject(&projDM);
	projDM->GetName(&proj_name);
	item= GTK_WIDGET(GTK_TREE(m_filesTree)->children->data);
	if (*proj_name!= '\0')
	  gtk_label_set_text(GTK_LABEL(GTK_BIN(item)->child), proj_name);
	else
	  gtk_label_set_text(GTK_LABEL(GTK_BIN(item)->child), "Untitled");
	NS_IF_RELEASE(projDM);
	NS_IF_RELEASE(wdeDM);
	nsCRT::free(proj_name);
}

#define INIT_ADD_TREE_ITEM(tree, what) \
{	\
	WDEITreeItem *t = NULL; \
	projDM->Get ## what (&t); \
	if (t) AddTreeItem(tree, t, -1, 0); \
	NS_IF_RELEASE(t); \
}

void WDEProjectBrowser::InitProject() {
  WDEIWDE *wdeDM = NULL;
	WDEIProject *projDM = NULL;
	WDEITreeItem *all_files = NULL;
	GtkWidget *item, *tree = NULL;
	char *proj_name = NULL;

  m_funct_list.clear();
  m_js_visible= true;
  m_js_dirty= true;

	WDEApp::GetWDE(&wdeDM);
	assert(wdeDM);
	wdeDM->GetActiveProject(&projDM);
	if (projDM) {
		projDM->GetTitle(&proj_name);
		item= gtk_tree_item_new_with_label(proj_name);
		nsCRT::free(proj_name);
		gtk_tree_append(GTK_TREE(m_filesTree), item);
		gtk_widget_show(item);
		tree= gtk_tree_new();
		gtk_tree_item_set_subtree(GTK_TREE_ITEM(item), tree);
		WDE_ADD_NOCOM_LISTENER(projDM);
		INIT_ADD_TREE_ITEM(tree, Actions);
		INIT_ADD_TREE_ITEM(tree, GladeForms);
		INIT_ADD_TREE_ITEM(tree, Resources);
		INIT_ADD_TREE_ITEM(tree, Extensions);
		INIT_ADD_TREE_ITEM(tree, InstallPages);
		projDM->GetFiles(&all_files);
		if (all_files) {
			GlobalAddListener(all_files);
		}
	}
	NS_IF_RELEASE(all_files);
	NS_IF_RELEASE(projDM);		
	NS_IF_RELEASE(wdeDM);
	ClearMeta();
	RecursiveExpandTree(m_filesTree);
}

void WDEProjectBrowser::Initialize(GtkWidget *mainWindow) {

	InitWindow(WDEPROJECTBROWSER_GLADE_FILENAME, WDEPROJECTBROWSER_GLADE_DIALOG, WDEPROJECTBROWSER_GLADE_NOTEBOOK, mainWindow, false);
	//grab useful widgets
  gtk_signal_connect(
    GTK_OBJECT(m_notebook),
    (gchar *) "switch-page",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_ExternNotebookSwitchPage), this);

	m_filesTree= glade_xml_get_widget(m_xml, WDEPROJECTBROWSER_GLADE_FILES_TREE);
	m_openFiles= glade_xml_get_widget(m_xml, WDEPROJECTBROWSER_GLADE_OPEN_FILES_TREE);
	m_clist_fun= glade_xml_get_widget(m_xml, "func_clist");

  m_actMeta= glade_xml_get_widget(m_xml, "act_Meta");
  m_act_entry1= glade_xml_get_widget(m_xml, "act_entry1");
  gtk_signal_connect(
    GTK_OBJECT(m_act_entry1),
    (gchar *) "changed",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaNameChange), m_actMeta);
  m_act_properties= glade_xml_get_widget(m_xml, "act_properties");
  gtk_signal_connect(
    GTK_OBJECT(m_act_properties),
    (gchar *) "clicked",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaActClicked), m_actMeta);
  
  m_extMeta= glade_xml_get_widget(m_xml, "ext_Meta");
  m_ext_entry1= glade_xml_get_widget(m_xml, "ext_entry1");
  gtk_signal_connect(
    GTK_OBJECT(m_ext_entry1),
    (gchar *) "changed",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaWDFLocationChange), m_extMeta);
  
  m_fileMeta= glade_xml_get_widget(m_xml, "file_Meta");
  m_file_entry1= glade_xml_get_widget(m_xml, "file_entry1");
  gtk_signal_connect(
    GTK_OBJECT(m_file_entry1),
    (gchar *) "changed",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaFileNameChange), m_fileMeta);
  m_file_location= glade_xml_get_widget(m_xml, "file_location");
  m_file_entry2_1= glade_xml_get_widget(m_xml, "file_entry2_1");
  gtk_signal_connect(
    GTK_OBJECT(m_file_entry2_1),
    (gchar *) "changed",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaFileLocationChange), m_fileMeta);
  m_file_entry3= glade_xml_get_widget(m_xml, "file_entry3");
  gtk_signal_connect(
    GTK_OBJECT(m_file_entry3),
    (gchar *) "toggled",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaFileCacheChange), m_fileMeta);
    
  m_instMeta= glade_xml_get_widget(m_xml, "inst_Meta");
  m_inst_entry1= glade_xml_get_widget(m_xml, "inst_entry1");
  gtk_signal_connect(
    GTK_OBJECT(m_inst_entry1),
    (gchar *) "changed",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaInstTitleChange), m_instMeta);
  m_inst_entry2_1= glade_xml_get_widget(m_xml, "inst_entry2_1");
  gtk_signal_connect(
    GTK_OBJECT(m_inst_entry2_1),
    (gchar *) "changed",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaInstTypeChange), m_instMeta);
  m_inst_entry3= glade_xml_get_widget(m_xml, "inst_entry3");
  gtk_signal_connect(
    GTK_OBJECT(m_inst_entry3),
    (gchar *) "changed",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaFileNameChange), m_instMeta);
  m_inst_up= glade_xml_get_widget(m_xml, "inst_up");
  gtk_signal_connect(
    GTK_OBJECT(m_inst_up),
    (gchar *) "clicked",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaInstUp), m_instMeta);
  m_inst_down= glade_xml_get_widget(m_xml, "inst_down");
  gtk_signal_connect(
    GTK_OBJECT(m_inst_down),
    (gchar *) "clicked",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_MetaInstDown), m_instMeta);
  
  GladeXML *xml;
  string gladePathname= WDEApp::GetWDEGladeDirectory() + WDEPROJECTBROWSER_GLADE_FILENAME;
	xml= glade_xml_new(gladePathname.c_str(), "popup");
	m_popup= glade_xml_get_widget(xml, "popup");
	
  m_add= glade_xml_get_widget(xml, "add");
  gtk_signal_connect(GTK_OBJECT(m_add), (gchar *) "activate",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_Add), m_add);
  m_import= glade_xml_get_widget(xml, "import");
  gtk_signal_connect(GTK_OBJECT(m_import), (gchar *) "activate",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_Import), m_import);
  m_remove= glade_xml_get_widget(xml, "remove");
  gtk_signal_connect(GTK_OBJECT(m_remove), (gchar *) "activate",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_Remove), m_remove);
  m_save= glade_xml_get_widget(xml, "save");
  gtk_signal_connect(GTK_OBJECT(m_save), (gchar *) "activate",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_Save), m_save);
  m_close= glade_xml_get_widget(xml, "close");
  gtk_signal_connect(GTK_OBJECT(m_close), (gchar *) "activate",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_Close), m_close);
  m_open= glade_xml_get_widget(xml, "open");
  gtk_signal_connect(GTK_OBJECT(m_open), (gchar *) "activate",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_Open), m_open);
  m_properties= glade_xml_get_widget(xml, "properties");
  gtk_signal_connect(GTK_OBJECT(m_properties), (gchar *) "activate",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_Properties), m_properties);
  m_design_view= glade_xml_get_widget(xml, "design_view");
  gtk_signal_connect(GTK_OBJECT(m_design_view), (gchar *) "activate",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_DesignView), m_design_view);

	InitFilesTab();
	InitOpenFilesTab();
	InitFunctionsTab();

	WDEIWDE *wdeDM;
	WDEApp::GetWDE(&wdeDM);
	WDE_ADD_NOCOM_LISTENER(wdeDM);
	NS_IF_RELEASE(wdeDM);
	gtk_signal_connect(GTK_OBJECT(m_dialogWindow), "delete-event",  GTK_SIGNAL_FUNC(WDEProjectBrowser_ExternDeleteWindow), this);
	ClearMeta();
}

void WDEProjectBrowser::Show(int activeTab) {
  m_activeTab= activeTab;
	gtk_widget_show(GTK_WIDGET(m_dialogWindow));
	gtk_notebook_set_page(GTK_NOTEBOOK(m_notebook), activeTab);
}

void WDEProjectBrowser::InitFilesTab() {
	GtkWidget *item, *tree;
	item= gtk_tree_item_new_with_label("Project [empty]");
	gtk_tree_append(GTK_TREE(m_filesTree), item);
	gtk_widget_show(item);
	tree= gtk_tree_new();
	gtk_tree_item_set_subtree(GTK_TREE_ITEM(item), tree);
  gtk_signal_connect(
    GTK_OBJECT(m_filesTree),
    (gchar *) "select-child",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_ItemSelectProj), this);
}

void WDEProjectBrowser::InitFunctionsTab() {
  gtk_signal_connect(
    GTK_OBJECT(m_clist_fun),
    (gchar *) "select-row",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_FunctionSelect), this);
  gtk_clist_set_compare_func(GTK_CLIST(m_clist_fun), function_info_compare);
}

void WDEProjectBrowser::ShowFunctionsTab() {
	m_open_vis= false;
	m_proj_vis= false;
	ClearMeta();
	RenderFunctionList();
}

void WDEProjectBrowser::ShowFilesTab() {
	m_open_vis= false;
	m_proj_vis= true;
	ClearMeta();
  if (m_proj_select)
	  gtk_tree_select_item(GTK_TREE(m_proj_select), m_proj_pos);
}

void WDEProjectBrowser::InitOpenFilesTab() {
	WDEIWDE *wdeDM;
  gtk_signal_connect(
    GTK_OBJECT(m_openFiles),
    (gchar *) "select-child",
    GTK_SIGNAL_FUNC(WDEProjectBrowser_ItemSelectOpen), this);
	WDEITreeItem *files;
	WDEApp::GetWDE(&wdeDM);
	wdeDM->GetFiles(&files);
	AddTreeItem(m_openFiles, files, -1, 2);
	GlobalAddListener(files);
	NS_RELEASE(files);
	NS_RELEASE(wdeDM);
}

void WDEProjectBrowser::ShowOpenFilesTab() {
	m_open_vis= true;
	m_proj_vis= false;
	ClearMeta();
  if (m_open_select)
	  gtk_tree_select_item(GTK_TREE(m_open_select), m_open_pos);
}

void WDEProjectBrowser::RefreshFunctionList(WDEITextEditorDM *tdm) {
  int i;
  char *str;
  for (i= 0; i< (int) m_funct_list.size(); ++i) {
    if (m_funct_list[i].tdm== tdm) {
      tdm->GetText(&str);
      m_funct_list[i].dirty= function_lookup(str, m_funct_list[i].functs, tdm);
      nsCRT::free(str);
    }
    m_js_dirty= m_js_dirty || m_funct_list[i].dirty;
  }
}

void WDEProjectBrowser::RecreateFunctionList() {
  m_funct_list.clear();
	WDEIWDE *wdeDM;
	WDEITreeItem *files;
	WDEApp::GetWDE(&wdeDM);
	wdeDM->GetAllFiles(&files);
	GlobalAddListener(files, false);
	NS_RELEASE(files);
	NS_RELEASE(wdeDM);
  m_js_dirty= true;
}

void WDEProjectBrowser::RenderFunctionList() {
  int i, row;
  if (!m_js_dirty) return;
  if (!m_js_visible) return;
  gtk_clist_freeze(GTK_CLIST(m_clist_fun));
  gtk_clist_clear(GTK_CLIST(m_clist_fun));
  for (i= 0; i< (int) m_funct_list.size(); ++i) {
    for (int j= 0; j< (int) m_funct_list[i].functs.size(); ++j) {
      string nom= m_funct_list[i].functs[j]->name+ " ( "+  m_funct_list[i].file_name+ " )";
      gchar *one_column_only= (gchar *) nom.c_str();
      row= gtk_clist_append(GTK_CLIST(m_clist_fun), (gchar **) &one_column_only);
      gtk_clist_set_row_data(GTK_CLIST(m_clist_fun), row, m_funct_list[i].functs[j]);
    }
  }
  m_js_dirty= false;
  gtk_clist_sort(GTK_CLIST(m_clist_fun));
  gtk_clist_thaw(GTK_CLIST(m_clist_fun));
}

void WDEProjectBrowser::AddFunctionList(WDEITextEditorDM *tdm) {
  FunctionListFileEntry fl;
  char *str;
  tdm->GetName(&str);
  fl.file_name= str;
  fl.tdm= tdm; // weak refernce once more. 
  nsCRT::free(str);
  tdm->GetText(&str);
  function_lookup(str, fl.functs, tdm);
  fl.dirty= true;
  nsCRT::free(str);
  m_funct_list.push_back(fl);
}

// forReal is false when we just want to traverse the list to refresh the
// texteditoDM list in m_funct_list
void WDEProjectBrowser::GlobalAddListener(WDEITreeItem *attach, bool forReal= true) {
  int length;
  WDEITreeFileItem *fitem;
  if (!attach) return;
  if (forReal)
    WDE_ADD_NOCOM_LISTENER(attach);
  // if it's a JS file, add a listener to its DM so we know when stuff changes
  if (NS_SUCCEEDED(attach->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &fitem))) {
    WDEIEditorDM *dm;
    WDEITextEditorDM *tdm;
    PRInt32 typ;
    fitem->GetFileType(&typ);
    // try and see what happens if we use the JS function parser on HTML files
    if (typ== WDEITreeFileItem::WDETreeFileItemTypeJS || typ== WDEITreeFileItem::WDETreeFileItemTypeHTML) {
      fitem->GetEditorDM(&dm);
      if (dm && NS_SUCCEEDED(dm->QueryInterface(NS_GET_IID(WDEITextEditorDM), (void **) &tdm))) {
        if (tdm) {
          if (forReal)
            WDE_ADD_NOCOM_LISTENER(tdm);
          AddFunctionList(tdm);
        }
      }
      NS_IF_RELEASE(dm);
      NS_IF_RELEASE(tdm);
    }
    NS_IF_RELEASE(fitem);
  }
  attach->GetLength(&length);
  for (int i= 0; i< length; ++i) {
    WDEITreeItem *tmp;
    attach->GetChild(i, &tmp);
    GlobalAddListener(tmp, forReal);
    NS_IF_RELEASE(tmp);
  }
}

GtkTreeItem *WDEProjectBrowser::FindItem(GtkTree *tree, WDEITreeItem *caller) {
  GList *section;
  GtkTree *sub_tree;
  GtkWidget *tmp;
  GtkTreeItem *tree_item;
  WDEITreeItem *wdeitem;

  section= gtk_container_children(GTK_CONTAINER(tree));
  while (section!= NULL) {
    tree_item= GTK_TREE_ITEM(section->data);
    wdeitem= (WDEITreeItem *) gtk_object_get_user_data(GTK_OBJECT(tree_item));
    if (caller== wdeitem) return tree_item;
    tmp= GTK_TREE_ITEM_SUBTREE(tree_item);
    if (tmp) {
      sub_tree= GTK_TREE(tmp);
      tree_item= FindItem(sub_tree, caller);
      if (tree_item) return tree_item;
    }
    section= g_list_next(section);
  }
  return NULL;
}

void WDEProjectBrowser::RecursiveExpand(GtkWidget *item) {
  if (!item)
    return;
  if (GTK_IS_TREE_ITEM(item)) {
    GtkWidget *par_tree= GTK_WIDGET(item)->parent;
    if (GTK_IS_ROOT_TREE(par_tree))
      return;
    GtkWidget *par_item= GTK_TREE(par_tree)->tree_owner;
    gtk_tree_item_expand(GTK_TREE_ITEM(par_item));
    RecursiveExpand(par_item);
  }
}

void WDEProjectBrowser::RecursiveExpandTree(GtkWidget *tree) {
  GList *items;
  GtkWidget *sub_item, *sub_tree;
  if (!tree)
    return;
  items= gtk_container_children(GTK_CONTAINER(tree));
  while (items) {
    sub_item= GTK_WIDGET(items->data);
    gtk_tree_item_expand(GTK_TREE_ITEM(sub_item));
    sub_tree= GTK_TREE_ITEM_SUBTREE(sub_item);
    RecursiveExpandTree(sub_tree);
    items= g_list_next(items);
  }
}

void WDEProjectBrowser::RefreshItemAdded(GtkWidget *tree, WDEITreeItem *caller, WDEITreeItem *child, int pos, int tab) {
  GtkTreeItem *item;
  GtkWidget *neu;
  GtkWidget *sub_tree;
  char *name;

  if (!child) {
    return;
  }
  if (!caller || !tree) return;
  item= FindItem(GTK_TREE(tree), caller);
  if (!item) return;
  sub_tree= GTK_TREE_ITEM_SUBTREE(item);
  if (!sub_tree) {
    sub_tree= gtk_tree_new();
		gtk_tree_item_set_subtree(GTK_TREE_ITEM(item), sub_tree);
	}
	if (NS_FAILED(child->GetName(&name))) return;
  neu= AddTreeItem(sub_tree, child, pos, tab);
  GlobalAddListener(child);
	RecursiveExpand(neu);
	RecursiveExpandTree(sub_tree);
  gtk_tree_select_item(GTK_TREE(sub_tree), pos);
  if (m_activeTab== 0) {
    ClearMeta();
    ShowMeta(neu);
  }
}

void WDEProjectBrowser::RefreshItemRemoved(GtkWidget *tree, WDEITreeItem *caller, int pos, int tab) {
  GtkTreeItem *item;
  GtkTree *sub_tree;
  int p= 0;
  GList *chi;

  if (!caller || !tree) return;
  item= FindItem(GTK_TREE(tree), caller);
  if (!item) return;
  sub_tree= GTK_TREE(GTK_TREE_ITEM_SUBTREE(item));
  switch (tab) {
    case 0:
      if (m_proj_select)
        gtk_tree_unselect_item(GTK_TREE(m_proj_select), m_proj_pos);
      m_proj_select= NULL;
      if (m_proj_vis)
        ClearMeta();
      break;
    case 2:
      if (m_open_select)
        gtk_tree_unselect_item(GTK_TREE(m_open_select), m_open_pos);
      m_open_select= NULL;
      if (m_open_vis)
        ClearMeta();
      break;
  }
  chi= gtk_container_children(GTK_CONTAINER(sub_tree));
  while ((p< pos) && (chi!= NULL)) {
    ++p;
    chi= g_list_next(chi);
  }
  if (chi)
    gtk_tree_remove_item(sub_tree, GTK_WIDGET(chi->data));
}

void WDEProjectBrowser::RefreshItem(GtkWidget *tree, WDEITreeItem *caller, int tab) {
  GtkTreeItem *item;
  char *name;

  if (!caller || !tree) return;
  item= FindItem(GTK_TREE(tree), caller);
  if (!item) return;
        
  caller->GetName(&name);
	gtk_label_set_text(GTK_LABEL(GTK_BIN(item)->child), name);
  nsCRT::free(name);
}

void WDEProjectBrowser::UpdateFilesTree(int id, WDEIEvent *event) {
  int pos;
  WDEIEventable *caller= NULL;
	nsISupports *ns= NULL;
  WDEITreeItem *ev= NULL;
	WDEITreeItem *chi= NULL;

  event->GetCaller(&caller);
  event->GetIntData(&pos);
  event->GetPtrData(&ns);
	if (ns)
		ns->QueryInterface(NS_GET_IID(WDEITreeItem), (void **) &chi);
  if (!caller) return;
  // WE HANDLE EVENTS SENT BY TreeItems ONLY
  if (NS_FAILED(caller->QueryInterface(NS_GET_IID(WDEITreeItem), (void **) &ev)))
    return;
	switch (id) {
	  case WDEIEvent::TreeItemChildAdded:
	    RefreshItemAdded(m_filesTree, ev, chi, pos, 0);
	    RefreshItemAdded(m_openFiles, ev, chi, pos, 2);
	    RefreshFunctionList(NULL);
	    RenderFunctionList();
	    break;
	  case WDEIEvent::TreeItemChildRemoved:
      RecreateFunctionList();
	    RefreshItemRemoved(m_filesTree, ev, pos, 0);
	    RefreshItemRemoved(m_openFiles, ev, pos, 2);
	    RenderFunctionList();
	    break;
	  case WDEIEvent::TreeItemNameSet:
      RecreateFunctionList();
	    RefreshItem(m_filesTree, ev, 0);
	    RefreshItem(m_openFiles, ev, 2);
	    RenderFunctionList();
	    break;
	}
  NS_IF_RELEASE(ev);
}

void WDEProjectBrowser::ClearMeta() {
  if (GTK_WIDGET_VISIBLE(m_actMeta))
    gtk_widget_hide_all(m_actMeta);
  if (GTK_WIDGET_VISIBLE(m_extMeta))
    gtk_widget_hide_all(m_extMeta);
  if (GTK_WIDGET_VISIBLE(m_fileMeta))
    gtk_widget_hide_all(m_fileMeta);
  if (GTK_WIDGET_VISIBLE(m_instMeta))
    gtk_widget_hide_all(m_instMeta);
}

void WDEProjectBrowser::ShowMeta(GtkWidget *item) {
  WDEITreeItem *chi;
  WDEIAction *act;
  WDEIExtension *ext;
  WDEITreeFileItem *file;
  WDEIInstallPage *page;
  char *tmp;
  int typ;
  PRBool bubu;
  bool in_project;
  nsresult rv;

  if (!item) return;  
  chi= (WDEITreeItem *) gtk_object_get_user_data(GTK_OBJECT(item));
  if (!chi) return;
  in_project= true;
  if (FindItem(GTK_TREE(m_openFiles), chi))
    in_project= false;
	rv= chi->QueryInterface(NS_GET_IID(WDEIInstallPage), (void **) &page);
	if (NS_SUCCEEDED(rv)) {
	  gtk_object_set_user_data(GTK_OBJECT(m_instMeta), (gpointer) page);
	  gtk_widget_show_all(m_instMeta);
	  
	  chi->GetName(&tmp);
	  gtk_entry_set_text(GTK_ENTRY(m_inst_entry3), tmp);
	  g_free(tmp);
	  page->GetScreenType(&typ);
	  if (typ< 0 || typ> 7) typ= 0;
	  gtk_entry_set_text(GTK_ENTRY(m_inst_entry2_1), inst_entry2_1strings[typ]);
	  page->GetTitle(&tmp);
	  gtk_entry_set_text(GTK_ENTRY(m_inst_entry1), tmp);
	  g_free(tmp);
	  
	  NS_IF_RELEASE(page);
	  return;
	}
	rv= chi->QueryInterface(NS_GET_IID(WDEIAction), (void **) &act);
	if (NS_SUCCEEDED(rv)) {
	  gtk_object_set_user_data(GTK_OBJECT(m_actMeta), (gpointer) act);
	  gtk_object_set_user_data(GTK_OBJECT(m_extMeta), (gpointer) act);
	  gtk_widget_show_all(m_actMeta);
	  gtk_widget_show_all(m_extMeta);
	  // fill in relevant information
	  act->GetName(&tmp);
	  gtk_entry_set_text(GTK_ENTRY(m_act_entry1), tmp);
	  g_free(tmp);
	
	  act->GetDeployPath(&tmp);
	  gtk_entry_set_text(GTK_ENTRY(m_ext_entry1), tmp);
	  g_free(tmp);
    NS_IF_RELEASE(act);
    return;
	}
	rv= chi->QueryInterface(NS_GET_IID(WDEIExtension), (void **) &ext);
	if (NS_SUCCEEDED(rv)) {
	  gtk_object_set_user_data(GTK_OBJECT(m_extMeta), (gpointer) ext);
	  gtk_widget_show_all(m_extMeta);
	  // fill in relevant information
	  ext->GetDeployPath(&tmp);
	  gtk_entry_set_text(GTK_ENTRY(m_ext_entry1), tmp);
	  g_free(tmp);
    NS_IF_RELEASE(ext);
	  return;
	}
	rv= chi->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &file);
	if (NS_SUCCEEDED(rv)) {
	  gtk_object_set_user_data(GTK_OBJECT(m_fileMeta), (gpointer) file);
	  gtk_widget_show_all(m_fileMeta);
	  // fill in relevant information
	  file->GetFileName(&tmp);
	  string fpath, fname;
	  SplitFilename(tmp, fname, fpath);
	  gtk_entry_set_text(GTK_ENTRY(m_file_entry1), fname.c_str());
	  g_free(tmp);
	  file->GetLocation(&typ);
	  if (typ< 0 || typ> 5) typ= 0;
	  gtk_entry_set_text(GTK_ENTRY(m_file_entry2_1), file_entry2_1strings[typ]);
	  file->GetPreCache(&bubu);
	  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(m_file_entry3), bubu);
	  if (!in_project)
	    gtk_widget_hide(m_file_location);
	  
	  NS_IF_RELEASE(file);
	  return;
	}
	return;
}

bool WDEProjectBrowser::AllowedResource(SectionType *list, char *name) {
  for (int i= 0; list[i].type!= WDE_LIST_TERM; ++i)
    if (g_strcasecmp(name, list[i].sect_name)== 0)
      return true;
  return false;
}

void WDEProjectBrowser::HideIrrelevant(WDEITreeItem *chi) {
  WDEIAction *act;
  WDEIExtension *ext;
  WDEITreeFileItem *file;
  WDEIInstallPage *page;
  WDEITreeGladeFormItem *gf;
  bool in_project;
  nsresult rv;

  if (!chi) return;  
  gtk_widget_set_sensitive(m_add, FALSE);
  gtk_widget_set_sensitive(m_import, FALSE);
  gtk_widget_set_sensitive(m_remove, FALSE);
  gtk_widget_set_sensitive(m_save, FALSE);
  gtk_widget_set_sensitive(m_open, FALSE);
  gtk_widget_set_sensitive(m_close, FALSE);
  gtk_widget_set_sensitive(m_properties, FALSE);
  gtk_widget_set_sensitive(m_design_view, FALSE);
  
  in_project= true;
  if (FindItem(GTK_TREE(m_openFiles), chi))
    in_project= false;
	rv= chi->QueryInterface(NS_GET_IID(WDEIInstallPage), (void **) &page);
	if (NS_SUCCEEDED(rv)) {
    gtk_widget_set_sensitive(m_remove, TRUE);
    gtk_widget_set_sensitive(m_save, TRUE);
    gtk_widget_set_sensitive(m_open, TRUE);
    gtk_widget_set_sensitive(m_close, TRUE);
	  
	  NS_IF_RELEASE(page);
	  return;
	}
	rv= chi->QueryInterface(NS_GET_IID(WDEIAction), (void **) &act);
	if (NS_SUCCEEDED(rv)) {
    gtk_widget_set_sensitive(m_remove, TRUE);
    gtk_widget_set_sensitive(m_properties, TRUE);

    NS_IF_RELEASE(act);
    return;
	}
	rv= chi->QueryInterface(NS_GET_IID(WDEIExtension), (void **) &ext);
	if (NS_SUCCEEDED(rv)) {
    NS_IF_RELEASE(ext);
	  return;
	}
	rv= chi->QueryInterface(NS_GET_IID(WDEITreeGladeFormItem), (void **) &gf);
	if (NS_SUCCEEDED(rv)) {
    gtk_widget_set_sensitive(m_design_view, TRUE);
    gtk_widget_set_sensitive(m_remove, TRUE);
    NS_IF_RELEASE(gf);
	  return;
	}
	rv= chi->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &file);
	if (NS_SUCCEEDED(rv)) {
    gtk_widget_set_sensitive(m_design_view, TRUE);
    gtk_widget_set_sensitive(m_close, TRUE);
    gtk_widget_set_sensitive(m_save, TRUE);
	  if (in_project)
      gtk_widget_set_sensitive(m_open, TRUE);
	  if (in_project)
      gtk_widget_set_sensitive(m_remove, TRUE);
	  WDEITreeItem *tchi;
    chi->GetParent(&tchi);
    if (NS_SUCCEEDED(tchi->QueryInterface(NS_GET_IID(WDEIAction), (void **) &act))) {
      gtk_widget_set_sensitive(m_remove, FALSE);
      NS_IF_RELEASE(act);
    }
    if (NS_SUCCEEDED(tchi->QueryInterface(NS_GET_IID(WDEITreeGladeFormItem), (void **) &gf))) {
      gtk_widget_set_sensitive(m_remove, FALSE);
      NS_IF_RELEASE(gf);
    }
    NS_IF_RELEASE(tchi);
	  NS_IF_RELEASE(file);
	  return;
	}
	if (in_project) {
	  char *name;
	  chi->GetName(&name);
	  if (AllowedResource(NewResAllowed, name) || (g_strcasecmp(name, "Actions")== 0) || (g_strcasecmp(name, "Glade Forms")== 0))
	    gtk_widget_set_sensitive(m_add, TRUE);
    if (AllowedResource(ImportResAllowed, name))
      gtk_widget_set_sensitive(m_import, TRUE);
    nsCRT::free(name);
  }
	return;
}

extern "C" void WDEProjectBrowser_ExternNotebookSwitchPage(GtkNotebook *notebook, GtkNotebookPage *page, gint page_num, WDEProjectBrowser *dlg) {
  dlg->ShowTab(page_num);
	switch (page_num) {
	  case 0:
	    dlg->ShowFilesTab();
	    break;
	  case 1:
	    dlg->ShowFunctionsTab();
	    break;
	  case 2:
	    dlg->ShowOpenFilesTab();
	    break;
	}
}

extern "C" gboolean WDEProjectBrowser_ExternDeleteWindow(GtkWidget *widget, GdkEvent *event, WDEProjectBrowser *dlg) {
	dlg->Hide();
	WDEMainWindow* window = UIManager::GetMainWindow();
	window->SetProjectCheckbox(false);
	return TRUE;
}

extern "C" void WDEProjectBrowser_ItemSelectProj(GtkTree *tree, GtkWidget *item, gpointer extra) {
  if (!item || !extra) return;
  WDEProjectBrowser *bro;
  bro= (WDEProjectBrowser *) extra;
  bro->m_proj_select= GTK_WIDGET(tree);
  bro->m_proj_pos= gtk_tree_child_position(GTK_TREE(bro->m_proj_select), item);
  if (bro->m_proj_vis) {
    bro->ClearMeta();
    bro->ShowMeta(item);
  }
}

extern "C" void WDEProjectBrowser_ItemSelectOpen(GtkTree *tree, GtkWidget *item, gpointer extra) {
  if (!item || !extra) return;
  WDEProjectBrowser *bro;
  bro= (WDEProjectBrowser *) extra;
  bro->m_open_select= GTK_WIDGET(tree);
  bro->m_open_pos= gtk_tree_child_position(GTK_TREE(bro->m_open_select), item);
  if (bro->m_open_vis) {
    bro->ClearMeta();
    bro->ShowMeta(item);
  }
}

extern "C" void WDEProjectBrowser_MetaNameChange(GtkEditable *item, gpointer extra) {
  WDEITreeItem *ti;
  GtkWidget *meta;
  char *name;
  
  if (!item || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEITreeItem *) gtk_object_get_user_data(GTK_OBJECT(extra));
  if (!ti) return;
  name= (char *) g_strdup(gtk_entry_get_text(GTK_ENTRY(item)));
  ti->SetName(name);
  g_free(name);
}
  
extern "C" void WDEProjectBrowser_MetaWDFLocationChange(GtkEditable *item, gpointer extra) {
  WDEITreeItem *ti;
  WDEIAction *act;
  WDEIExtension *ext;
  GtkWidget *meta;
  char *name;
  
  if (!item || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEITreeItem *) gtk_object_get_user_data(GTK_OBJECT(extra));
  if (!ti) return;
  name= (char *) g_strdup(gtk_entry_get_text(GTK_ENTRY(item)));
  if (NS_SUCCEEDED(ti->QueryInterface(NS_GET_IID(WDEIAction), (void **) &act))) {
    act->SetDeployPath(name);
    NS_IF_RELEASE(act);
  }
  if (NS_SUCCEEDED(ti->QueryInterface(NS_GET_IID(WDEIExtension), (void **) &ext))) {
    ext->SetDeployPath(name);
    NS_IF_RELEASE(ext);
  }
  g_free(name);
}
  
extern "C" void WDEProjectBrowser_MetaFileNameChange(GtkEditable *item, gpointer extra) {
  WDEITreeFileItem *ti;
  GtkWidget *meta;
  char *name, *fullname;
  string fname, fpath;
  
  if (!item || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEITreeFileItem *) gtk_object_get_user_data(GTK_OBJECT(extra));
  if (!ti) return;
  name= (char *) g_strdup(gtk_entry_get_text(GTK_ENTRY(item)));
  ti->GetFileName(&fullname);
  SplitFilename(fullname, fname, fpath);
  nsCRT::free(fullname);
  fname= fpath+ string(name);
  ti->SetFileName(fname.c_str());
  g_free(name);
}
    
extern "C" void WDEProjectBrowser_MetaFileLocationChange(GtkEditable *item, gpointer extra) {
  WDEITreeFileItem *ti;
  GtkWidget *meta;
  char *name;
  int index;
  
  if (!item || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEITreeFileItem *) gtk_object_get_user_data(GTK_OBJECT(extra));
  if (!ti) return;
  
  name= (char *) g_strdup(gtk_entry_get_text(GTK_ENTRY(item)));
  for (index= 0; index< 6; ++index)
    if (g_strcasecmp(name, file_entry2_1strings[index])== 0) break;
  if (index< 6)
    ti->SetLocation(index);
  g_free(name);
}

extern "C" void WDEProjectBrowser_MetaInstTitleChange(GtkEditable *item, gpointer extra) {
  WDEIInstallPage *ti;
  GtkWidget *meta;
  char *name;
  
  if (!item || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEIInstallPage *) gtk_object_get_user_data(GTK_OBJECT(extra));
  if (!ti) return;
  name= (char *) g_strdup(gtk_entry_get_text(GTK_ENTRY(item)));
  ti->SetTitle(name);
  g_free(name);
}

extern "C" void WDEProjectBrowser_MetaInstTypeChange(GtkEditable *item, gpointer extra) {
  WDEIInstallPage *ti;
  GtkWidget *meta;
  char *name;
  int index;
  if (!item || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEIInstallPage *) gtk_object_get_user_data(GTK_OBJECT(extra));
  if (!ti) return;
  
  name= (char *) g_strdup(gtk_entry_get_text(GTK_ENTRY(item)));
  for (index= 0; index< 8; ++index)
    if (g_strcasecmp(name, inst_entry2_1strings[index])== 0) break;
  if (index< 8)
    ti->SetScreenType(index);
  g_free(name);
}

extern "C" void WDEProjectBrowser_MetaActClicked(GtkButton *button, gpointer extra) {
  WDEIAction *act;
  GtkWidget *meta;
  int idx;
  sashILocationDKLocation *dkloc;

  if (!button || !extra) return;
  meta= (GtkWidget *) extra;
  act= (WDEIAction *) gtk_object_get_user_data(GTK_OBJECT(meta));
  if (!act) return;
	act->GetIndex(&idx);
	if (WDEApp::m_locationDKs[idx]->ghost)
	  return;
	else {
	  act->GetDevkit(&dkloc);
  	dkloc->Configure();
  	NS_IF_RELEASE(dkloc);
	}
}

extern "C" void WDEProjectBrowser_MetaInstUp(GtkButton *button, gpointer extra) {
  WDEIInstallPage *ti, *tmp;
  WDEITreeItem *par, *tmp2;
  GtkWidget *meta;
  int index, len;
  if (!button || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEIInstallPage *) gtk_object_get_user_data(GTK_OBJECT(meta));
  if (!ti) return;
  ti->GetParent(&par);
  if (!par) return;
  par->GetLength(&len);
  for (index= 0; index< len; ++index) {
    par->GetChild(index, &tmp2);
    tmp2->QueryInterface(NS_GET_IID(WDEIInstallPage), (void **) &tmp);
    if ((void *) tmp== (void *) ti) {
      NS_IF_RELEASE(tmp);
      NS_IF_RELEASE(tmp2);
      break;
    }
    NS_IF_RELEASE(tmp);
    NS_IF_RELEASE(tmp2);
  }
  if (index> 0 && index< len) {
    ti->QueryInterface(NS_GET_IID(WDEITreeItem), (void **) &tmp2);
    par->RemoveChild(tmp2);
    par->AddChildWithPosition(tmp2, index- 1);
    NS_IF_RELEASE(tmp2);
  }
  NS_IF_RELEASE(par);
}

extern "C" void WDEProjectBrowser_MetaInstDown(GtkButton *button, gpointer extra) {
  WDEIInstallPage *ti, *tmp;
  WDEITreeItem *par, *tmp2;
  GtkWidget *meta;
  int index, len;
  if (!button || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEIInstallPage *) gtk_object_get_user_data(GTK_OBJECT(meta));
  if (!ti) return;
  ti->GetParent(&par);
  if (!par) return;
  par->GetLength(&len);
  for (index= 0; index< len; ++index) {
    par->GetChild(index, &tmp2);
    tmp2->QueryInterface(NS_GET_IID(WDEIInstallPage), (void **) &tmp);
    if ((void *) tmp== (void *) ti) {
      NS_IF_RELEASE(tmp);
      NS_IF_RELEASE(tmp2);
      break;
    }
    NS_IF_RELEASE(tmp);
    NS_IF_RELEASE(tmp2);
  }
  if (index>= 0 && index< len- 1) {
    ti->QueryInterface(NS_GET_IID(WDEITreeItem), (void **) &tmp2);
    par->RemoveChild(tmp2);
    par->AddChildWithPosition(tmp2, index+ 1);
    NS_IF_RELEASE(tmp2);
  }
  NS_IF_RELEASE(par);
}

extern "C" void WDEProjectBrowser_MetaFileCacheChange(GtkToggleButton *item, gpointer extra) {
  WDEITreeFileItem *ti;
  GtkWidget *meta;
  
  if (!item || !extra) return;
  meta= (GtkWidget *) extra;
  ti= (WDEITreeFileItem *) gtk_object_get_user_data(GTK_OBJECT(extra));
  if (!ti) return;
  
  ti->SetPreCache(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(item)));
}

extern "C" gboolean WDEProjectBrowser_ItemDoubleClick(GtkWidget *item, GdkEventButton *ev, gpointer extra) {
  WDEITreeItem *ti;
  WDEITreeFileItem *tfi;
	WDEProjectBrowser *proj;
  if (!item || !ev) return FALSE;
  ti= (WDEITreeItem *) gtk_object_get_user_data(GTK_OBJECT(item));
  if (!ti) return FALSE;
  if (ev->type== GDK_BUTTON_PRESS && ev->button== 3) {
    proj= (WDEProjectBrowser *) extra;
    if (!proj)
      return false;
    gtk_object_set_user_data(GTK_OBJECT(proj->m_popup), ti);
    gtk_object_set_user_data(GTK_OBJECT(proj->m_add), proj);
    gtk_object_set_user_data(GTK_OBJECT(proj->m_import), proj);
    gtk_object_set_user_data(GTK_OBJECT(proj->m_remove), proj);
    gtk_object_set_user_data(GTK_OBJECT(proj->m_save), proj);
    gtk_object_set_user_data(GTK_OBJECT(proj->m_open), proj);
    gtk_object_set_user_data(GTK_OBJECT(proj->m_close), proj);
    gtk_object_set_user_data(GTK_OBJECT(proj->m_properties), proj);
    gtk_object_set_user_data(GTK_OBJECT(proj->m_design_view), proj);
    proj->HideIrrelevant(ti);
    gtk_menu_popup(GTK_MENU(proj->m_popup), NULL, NULL, NULL, NULL, 3, ev->time);
    return true;
  }
  if (ev->type!= GDK_2BUTTON_PRESS) return FALSE;
  if (NS_FAILED(ti->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &tfi)))
    return FALSE;
  OpenEditorFromTreeFileItem(tfi);
  NS_IF_RELEASE(tfi);
  return TRUE;
}

#define POPUP_ITEM_COMMON(arg) \
  WDEProjectBrowser *proj; \
  GtkWidget *popup; \
  WDEITreeItem *item; \
  proj= (WDEProjectBrowser *) gtk_object_get_user_data(GTK_OBJECT(user_data)); \
  if (!proj) \
    return; \
  popup= GTK_WIDGET(proj->m_popup); \
  if (!popup) \
    return; \
  item= (WDEITreeItem *) gtk_object_get_user_data(GTK_OBJECT(popup)); \
  if (!item) \
    return; 

extern "C" void WDEProjectBrowser_Add(gpointer user_data) {
  POPUP_ITEM_COMMON("Add");
  char *sect_name;
  item->GetName(&sect_name);
  if (g_strcasecmp(sect_name, "Actions")== 0) {
	  UIManager::GetAddProjectItemDlg()->Show(WDEADDPROJECTITEMDLG_TAB_NEWACTION);
    return;
  }
  if (g_strcasecmp(sect_name, "Glade Forms")== 0) {
	  UIManager::GetAddProjectItemDlg()->Show(WDEADDPROJECTITEMDLG_TAB_NEWFORM);
    return;
  }
  for (int i= 0; NewResAllowed[i].type!= WDE_LIST_TERM; ++i)
    if (g_strcasecmp(sect_name, NewResAllowed[i].sect_name)== 0) {
      UIManager::GetAddProjectItemDlg()->Show(WDEADDPROJECTITEMDLG_TAB_NEWPROJECTFILE, NewResAllowed[i].type);
      return;
    }
  UIManager::GetAddProjectItemDlg()->Show(WDEADDPROJECTITEMDLG_TAB_NEWPROJECTFILE, WDE_LIST_TERM);
}

extern "C" void WDEProjectBrowser_Import(gpointer user_data) {
  POPUP_ITEM_COMMON("Import");
  char *sect_name;
  item->GetName(&sect_name);
  for (int i= 0; ImportResAllowed[i].type!= WDE_LIST_TERM; ++i)
    if (g_strcasecmp(sect_name, ImportResAllowed[i].sect_name)== 0) {
      UIManager::GetAddProjectItemDlg()->Show(WDEADDPROJECTITEMDLG_TAB_IMPORTPROJECTFILE, ImportResAllowed[i].type);
      return;
    }
  UIManager::GetAddProjectItemDlg()->Show(WDEADDPROJECTITEMDLG_TAB_IMPORTPROJECTFILE, WDE_LIST_TERM);
}

extern "C" void WDEProjectBrowser_Remove(gpointer user_data) {
  POPUP_ITEM_COMMON("Remove");
  WDEProjectBrowser_Close(user_data);
  if(item) item->Remove();
}

extern "C" void WDEProjectBrowser_Save(gpointer user_data) {
  POPUP_ITEM_COMMON("Save");
  
	WDEIWDE* wde = NULL;
  WDEITreeItem *ti= item;
  WDEITreeFileItem *tfi;
  WDEIEditorDM *edDM;
  WDEITextEditorDM *tedDM;
  
  if (NS_FAILED(ti->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &tfi))) return;  
  tfi->GetEditorDM(&edDM);
  if (!edDM) {
    NS_IF_RELEASE(tfi);
    return;
  }
  if (NS_FAILED(edDM->QueryInterface(NS_GET_IID(WDEITextEditorDM), (void **) &tedDM))) {
    NS_IF_RELEASE(tfi);
    NS_IF_RELEASE(edDM);
    return;
  }
    
	PRBool dirty;
	PRBool neverNamed;
	edDM->GetDirty(&dirty);
	edDM->GetNeverNamed(&neverNamed);
	if (dirty && !neverNamed)
    edDM->Save();
	if (dirty && neverNamed && GetNameAndNameFile(edDM))
    edDM->Save();
    
	NS_IF_RELEASE(wde);
  NS_IF_RELEASE(tfi);
  NS_IF_RELEASE(edDM);
  NS_IF_RELEASE(tedDM);
  return;
}

extern "C" void WDEProjectBrowser_Close(gpointer user_data) {
  POPUP_ITEM_COMMON("Close");
  
  WDEITreeItem *ti= item;
  WDEITreeFileItem *tfi;
  WDEIEditorDM *edDM;
  WDEITextEditorDM *tedDM;
  
  if (NS_FAILED(ti->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &tfi))) return;
	PRBool openedForEditing = false;
	tfi->GetOpenedForEditing(&openedForEditing);
	if (openedForEditing) {
	  tfi->GetEditorDM(&edDM);
  	if (!edDM) {
    	NS_IF_RELEASE(tfi);
    	return;
  	}
  	if (NS_FAILED(edDM->QueryInterface(NS_GET_IID(WDEITextEditorDM), (void **) &tedDM))) {
   		NS_IF_RELEASE(tfi);
    	NS_IF_RELEASE(edDM);
    	return;
  	}

  	CloseFileAndPromptIfNecesary(tedDM);
		NS_IF_RELEASE(edDM);
	  NS_IF_RELEASE(tedDM);
	}
  NS_IF_RELEASE(tfi);
	return;
}

extern "C" void WDEProjectBrowser_Open(gpointer user_data) {
  POPUP_ITEM_COMMON("Open");

  WDEITreeItem *ti= item;
  WDEITreeFileItem *tfi;
  if (NS_FAILED(ti->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &tfi)))
    return;  
  OpenEditorFromTreeFileItem(tfi);
  NS_IF_RELEASE(tfi);
  return;
}

extern "C" void WDEProjectBrowser_Properties(gpointer user_data) {
  POPUP_ITEM_COMMON("Properties");
  char *sect_name;
  item->GetName(&sect_name);
  nsCOMPtr<WDEIAction> act;
  int idx;
  sashILocationDKLocation *dkloc;

	act = do_QueryInterface(item);
	act->GetIndex(&idx);
	if(WDEApp::m_locationDKs[idx]->ghost) return;
	else
	{
	  act->GetDevkit(&dkloc);
  	dkloc->Configure();
  	NS_IF_RELEASE(dkloc);
	}

	act = NULL;
}

extern "C" void WDEProjectBrowser_DesignView(gpointer user_data) {
  POPUP_ITEM_COMMON("DesignView");
  
  WDEITreeFileItem *tfi;

  if (NS_FAILED(item->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &tfi)))
    return;  

  DesignViewHelper(tfi);
	NS_IF_RELEASE(tfi);
}

extern "C" void WDEProjectBrowser_FunctionSelect(
  GtkCList *clist, gint row, gint column, GdkEventButton *event, gpointer user_data) {

  WDEIWDE *wde;  
  FunctionInfo *fi;
  WDEITextEditorDM *tdm;
  if (event->type!= GDK_2BUTTON_PRESS)
    return;
  fi= (FunctionInfo *) gtk_clist_get_row_data(GTK_CLIST(clist), row);
  if (!fi)
    return;
  tdm= (WDEITextEditorDM *) fi->user;
  if (!tdm)
    return;
	WDEApp::GetWDE(&wde);
	if (wde)
	{
		WDEIEditorDM *edm = NULL;
		tdm->QueryInterface(NS_GET_IID(WDEIEditorDM), (void **) &edm);
		wde->SetActiveEditorDM(edm);
		NS_IF_RELEASE(edm);
	}
  tdm->SetCurrentLine(fi->line);
	NS_IF_RELEASE(wde);
}

extern "C" gint function_info_compare(GtkCList *src, gconstpointer p1, gconstpointer p2) {
  GtkCListRow *r1= (GtkCListRow *) p1;
  GtkCListRow *r2= (GtkCListRow *) p2;
  FunctionInfo *f1, *f2;
  char *n1, *n2, *fn1, *fn2;
  int res;
  WDEITextEditorDM *tmp;
  f1= (FunctionInfo *) r1->data;
  f2= (FunctionInfo *) r2->data;
  if (!f1 || !f2) return 0;
  tmp= (WDEITextEditorDM *) f1->user;
  tmp->GetName(&n1);
  tmp= (WDEITextEditorDM *) f2->user;
  tmp->GetName(&n2);
  if (!n1 || !n2) return 0;
  g_strup(n1);
  g_strup(n2);
  res= g_strcasecmp(n1, n2);
  if (res== 0) {// need tie-breaker by function name
    fn1= g_strdup(f1->name.c_str());
    fn2= g_strdup(f2->name.c_str());
    g_strup(fn1);
    g_strup(fn2);
    res= g_strcasecmp(fn1, fn2);
    g_free(fn1);
    g_free(fn2);
    if (res== 0) { // need tie_breaker by line number
      if (f1->line < f2->line) res= -1;
      if (f1->line > f2->line) res= 1;
    }
  }
  nsCRT::free(n1);
  nsCRT::free(n2);
  return res;
}
