
/***************************************************************
    SashWDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#ifndef _WDEPROJECTBROWSER_H
#define _WDEPROJECTBROWSER_H

// Stefan Atev, IBM Extreme Blue 2001

#include <string>
#include <vector>

#include "WDETabDialogWindow.h"
#include "WDENoCOMEventable.h"
#include "WDEParseJSFunctions.h"
#include "WDEAddProjectItemDlg.h"

#define WDEPROJECTBROWSER_TAB_FILES 0

class WDEITextEditorDM;

struct FunctionListFileEntry {
  std::string file_name;
  WDEITextEditorDM *tdm;
  bool dirty;
  std::vector<FunctionInfo *> functs;
  
  FunctionListFileEntry() {
    file_name= "";
    dirty= false;
    tdm= NULL;
  }
};

struct SectionType {
  char *sect_name;
  int type;
};

static SectionType NewResAllowed[]= {
  {"JavaScript", WDEITreeFileItem::WDETreeFileItemTypeJS},
  {"HTML", WDEITreeFileItem::WDETreeFileItemTypeHTML},
  {"Text", WDEITreeFileItem::WDETreeFileItemTypeText},
  {"Install Pages", WDE_ADD_INSTALL_PAGE},
  {"XML", WDEITreeFileItem::WDETreeFileItemTypeXML},
  {"", WDE_LIST_TERM}
};

static SectionType ImportResAllowed[]= {
  {"JavaScript", WDEITreeFileItem::WDETreeFileItemTypeJS},
  {"HTML", WDEITreeFileItem::WDETreeFileItemTypeHTML},
  {"Text", WDEITreeFileItem::WDETreeFileItemTypeText},
  {"Install Pages", WDE_ADD_INSTALL_PAGE},
  {"XML", WDEITreeFileItem::WDETreeFileItemTypeXML},
  {"Glade", WDEITreeFileItem::WDETreeFileItemTypeGlade},
  {"Images", WDEITreeFileItem::WDETreeFileItemTypeImage},
  {"Other Files", WDEITreeFileItem::WDETreeFileItemTypeData},
  {"", WDE_LIST_TERM}
};

class WDEProjectBrowser : public WDETabWindow, public WDENoCOMEventable
{
public:
	WDEProjectBrowser();
	~WDEProjectBrowser();
	
	void Show(int activeTab);
	void Initialize(GtkWidget *mainWindow);
	bool HandleEvent(WDEIEvent *event);

  void ClearMeta();
  void ShowMeta(GtkWidget *item);

  GtkWidget *m_popup;
  GtkWidget *m_add;
  GtkWidget *m_import;
  GtkWidget *m_remove;
  GtkWidget *m_save;
  GtkWidget *m_close;
  GtkWidget *m_open;
  GtkWidget *m_properties;
  GtkWidget *m_design_view;
  GtkWidget *m_proj_select;
  GtkWidget *m_open_select;
  bool m_proj_vis;
  bool m_open_vis;
  int m_proj_pos;
  int m_open_pos;
  int m_explicit_select;

  bool AllowedResource(SectionType *list, char *name);
  void HideIrrelevant(WDEITreeItem *chi);
  void TabSwitched(int activeTab);
  void ShowFilesTab();
  void ShowOpenFilesTab();
  void ShowFunctionsTab();
  
private:

  // Whole lotta funKtions you don't wanna mess with
  GtkWidget *AddTreeItem(GtkWidget *tree, WDEITreeItem *item, int pos, int tab);
  void InitProject();
  void InitFilesTab();
  void InitOpenFilesTab();
  void InitFunctionsTab();
  void RefreshFunctionList(WDEITextEditorDM *tdm);
  void RenderFunctionList();
  void RecreateFunctionList();
  void AddFunctionList(WDEITextEditorDM *tdm);
  void GlobalAddListener(WDEITreeItem *attach, bool forReal= true);
  GtkTreeItem *FindItem(GtkTree *tree, WDEITreeItem *caller);
  void RecursiveExpand(GtkWidget *item);
  void RefreshItemAdded(GtkWidget *tree, WDEITreeItem *caller, WDEITreeItem *child, int pos, int tab);
  void RefreshItemRemoved(GtkWidget *tree, WDEITreeItem *caller, int pos, int tab);
  void UpdateFilesTree(int id, WDEIEvent *event);
  void ResetProjectInfo();
  void RefreshItem(GtkWidget *tree, WDEITreeItem *caller, int tab);
  void RecursiveExpandTree(GtkWidget *tree);
  
  std::vector<FunctionListFileEntry> m_funct_list;
  
  bool m_js_visible;
  bool m_js_dirty;
  int m_activeTab;
	
	GtkWidget *m_filesTree;
	GtkWidget *m_openFiles;
	GtkWidget *m_clist_fun;
	
  GtkWidget *m_metaFrame;
  GtkWidget *m_actMeta;
  GtkWidget *m_act_entry1;
  GtkWidget *m_act_properties;
  GtkWidget *m_extMeta;
  GtkWidget *m_ext_entry1;
  GtkWidget *m_fileMeta;
  GtkWidget *m_file_entry1;
  GtkWidget *m_file_location;
  GtkWidget *m_file_entry2_1;
  GtkWidget *m_file_entry3;
  GtkWidget *m_instMeta;
  GtkWidget *m_inst_entry1;
  GtkWidget *m_inst_entry2_1;
  GtkWidget *m_inst_entry3;
  GtkWidget *m_inst_up;
  GtkWidget *m_inst_down;
};

extern "C" void WDEProjectBrowser_ExternNotebookSwitchPage(GtkNotebook *notebook, GtkNotebookPage *page, gint page_num, WDEProjectBrowser *dlg);
extern "C" gboolean WDEProjectBrowser_ExternDeleteWindow(GtkWidget *widget, GdkEvent *event, WDEProjectBrowser *dlg);
extern "C" void WDEProjectBrowser_ItemSelectProj(GtkTree *tree, GtkWidget *item, gpointer extra);
extern "C" void WDEProjectBrowser_ItemSelectOpen(GtkTree *tree, GtkWidget *item, gpointer extra);
extern "C" void WDEProjectBrowser_MetaNameChange(GtkEditable *item, gpointer extra);
extern "C" void WDEProjectBrowser_MetaFileNameChange(GtkEditable *item, gpointer extra);
extern "C" void WDEProjectBrowser_MetaFileLocationChange(GtkEditable *item, gpointer extra);
extern "C" void WDEProjectBrowser_MetaInstTypeChange(GtkEditable *item, gpointer extra);
extern "C" void WDEProjectBrowser_MetaActClicked(GtkButton *button, gpointer extra);
extern "C" void WDEProjectBrowser_MetaInstUp(GtkButton *button, gpointer extra);
extern "C" void WDEProjectBrowser_MetaInstDown(GtkButton *button, gpointer extra);
extern "C" void WDEProjectBrowser_MetaFileCacheChange(GtkToggleButton *item, gpointer extra);
extern "C" void WDEProjectBrowser_MetaWDFLocationChange(GtkEditable *item, gpointer extra);
extern "C" void WDEProjectBrowser_MetaInstTitleChange(GtkEditable *item, gpointer extra);
extern "C" gboolean WDEProjectBrowser_ItemDoubleClick(GtkWidget *item, GdkEventButton *ev, gpointer extra);

extern "C" void WDEProjectBrowser_Add(gpointer user_data);
extern "C" void WDEProjectBrowser_Import(gpointer user_data);
extern "C" void WDEProjectBrowser_Remove(gpointer user_data);
extern "C" void WDEProjectBrowser_Save(gpointer user_data);
extern "C" void WDEProjectBrowser_Close(gpointer user_data);
extern "C" void WDEProjectBrowser_Open(gpointer user_data);
extern "C" void WDEProjectBrowser_MoveUp(gpointer user_data);
extern "C" void WDEProjectBrowser_MoveDown(gpointer user_data);
extern "C" void WDEProjectBrowser_Properties(gpointer user_data);
extern "C" void WDEProjectBrowser_DesignView(gpointer user_data);

extern "C" void WDEProjectBrowser_FunctionSelect(
  GtkCList *clist, gint row, gint column, GdkEventButton *event, gpointer user_data);

extern "C" gint function_info_compare(GtkCList *src, gconstpointer p1, gconstpointer p2);

#endif
