#include "WDEProjectBrowserPane.h"
#include "WDEApp.h"
#include "xpcomtools.h"

/* Implementation file */
NS_IMPL_ISUPPORTS_INHERITED1(WDEProjectBrowserPane, WDEPane, WDEIProjectBrowserPane)

WDEProjectBrowserPane::WDEProjectBrowserPane()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
	m_projectBrowserTab = do_CreateInstance("@gnome.org/SashWDE/WDEProjectBrowserPaneTab;1");
	m_functionBrowserTab = do_CreateInstance("@gnome.org/SashWDE/WDEFunctionBrowserPaneTab;1");
	m_macroBrowserTab = do_CreateInstance("@gnome.org/SashWDE/WDEMacroBrowserPaneTab;1");
}

WDEProjectBrowserPane::~WDEProjectBrowserPane()
{
  /* destructor code */
}

/* readonly attribute WDEIProjectBrowserPaneTab projectBrowserTab; */
NS_IMETHODIMP WDEProjectBrowserPane::GetProjectBrowserTab(WDEIProjectBrowserPaneTab * *aProjectBrowserTab)
{
	XPCOM_COMPTR_GETTER(m_projectBrowserTab, aProjectBrowserTab);
}

/* readonly attribute WDEIFunctionBrowserPaneTab functionBrowserTab; */
NS_IMETHODIMP WDEProjectBrowserPane::GetFunctionBrowserTab(WDEIFunctionBrowserPaneTab * *aFunctionBrowserTab)
{
	XPCOM_COMPTR_GETTER(m_functionBrowserTab, aFunctionBrowserTab);
}

/* readonly attribute WDEIMacroBrowserPaneTab macroBrowserTab; */
NS_IMETHODIMP WDEProjectBrowserPane::GetMacroBrowserTab(WDEIMacroBrowserPaneTab * *aMacroBrowserTab)
{
	XPCOM_COMPTR_GETTER(m_macroBrowserTab, aMacroBrowserTab);
}

/* void build (); */
NS_IMETHODIMP WDEProjectBrowserPane::Build()
{
  SetName(m_name.c_str());

	m_projectBrowserTab->Build();
	m_functionBrowserTab->Build();
	m_macroBrowserTab->Build();

	nsCOMPtr<WDEIPaneTab> t;
	t = do_QueryInterface(m_projectBrowserTab);
	AppendTab(t);
	t = do_QueryInterface(m_functionBrowserTab);
	AppendTab(t);
	t = do_QueryInterface(m_macroBrowserTab);
	AppendTab(t);
	
	SetName("Project Browser");
	m_popupMenu->AddItem("Browser\\Browse", 4001);
	
	return NS_OK;
}
