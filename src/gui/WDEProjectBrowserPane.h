#ifndef _WDEPROJECTBROWSERPANE_H
#define _WDEPROJECTBROWSERPANE_H

#include <gtk/gtk.h>
#include "WDEIProjectBrowserPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//97A29179-B1D6-4CE8-990A-6BB031D2775B
#define WDEPROJECTBROWSERPANE_CID {0x97A29179, 0xB1D6, 0x4CE8, {0x99, 0x0A, 0x6B, 0xB0, 0x31, 0xD2, 0x77, 0x5B}}
NS_DEFINE_CID(kWDEProjectBrowserPaneCID, WDEPROJECTBROWSERPANE_CID);

#define WDEPROJECTBROWSERPANE_CONTRACT_ID "@gnome.org/SashWDE/WDEProjectBrowserPane;1"

class WDEProjectBrowserPane : public WDEIProjectBrowserPane, public WDEPane
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPROJECTBROWSERPANE
	NS_FORWARD_WDEIPANE(WDEPane::)

  WDEProjectBrowserPane();
  virtual ~WDEProjectBrowserPane();
  /* additional members */
	
private:
	nsCOMPtr<WDEIProjectBrowserPaneTab> m_projectBrowserTab;
	nsCOMPtr<WDEIFunctionBrowserPaneTab> m_functionBrowserTab;
	nsCOMPtr<WDEIMacroBrowserPaneTab> m_macroBrowserTab;
};

#endif
