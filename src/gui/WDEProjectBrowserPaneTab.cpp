#include "WDEProjectBrowserPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEProjectBrowserPaneTab, WDEPaneTab, WDEIProjectBrowserPaneTab);

WDEProjectBrowserPaneTab::WDEProjectBrowserPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEProjectBrowserPaneTab::~WDEProjectBrowserPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEProjectBrowserPaneTab::Build()
{
	m_name = "Project";
	return NS_OK;
}

/* void addItem (in WDEIProjectItem item); */
NS_IMETHODIMP WDEProjectBrowserPaneTab::AddItem(WDEIProjectItem *item)
{
  return NS_OK;
}
