#ifndef _WDEPROJECTBROWSERPANETAB_H
#define _WDEPROJECTBROWSERPANETAB_H

#include <gtk/gtk.h>
#include "WDEIProjectBrowserPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"
#include "WDEIProjectItem.h"

//FF8B2222-8A80-4569-8D0C-D69D206B40A4
#define WDEPROJECTBROWSERPANETAB_CID {0xFF8B2222, 0x8A80, 0x4569, {0x8D, 0x0C, 0xD6, 0x9D, 0x20, 0x6B, 0x40, 0xA4}}
NS_DEFINE_CID(kWDEProjectBrowserPaneTabCID, WDEPROJECTBROWSERPANETAB_CID);

#define WDEPROJECTBROWSERPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEProjectBrowserPaneTab;1"

class WDEProjectBrowserPaneTab : public WDEIProjectBrowserPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)
  NS_DECL_WDEIPROJECTBROWSERPANETAB

  WDEProjectBrowserPaneTab();
  virtual ~WDEProjectBrowserPaneTab();
  /* additional members */
};

#endif
