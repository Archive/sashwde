#include "WDEProjectItemPropertyPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEProjectItemPropertyPaneTab, WDEPaneTab, WDEIProjectItemPropertyPaneTab)

WDEProjectItemPropertyPaneTab::WDEProjectItemPropertyPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEProjectItemPropertyPaneTab::~WDEProjectItemPropertyPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEProjectItemPropertyPaneTab::Build()
{
	m_name = "Project Item";
	return NS_OK;
}
