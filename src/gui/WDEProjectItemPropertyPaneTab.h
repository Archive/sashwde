#ifndef _WDEPROJECTITEMPROPERTYPANETAB_H
#define _WDEPROJECTITEMPROPERTYPANETAB_H

#include <gtk/gtk.h>
#include "WDEIPropertyPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//398338D4-21A4-42C1-811B-5FB533CEE69F
#define WDEPROJECTITEMPROPERTYPANETAB_CID {0x398338D4, 0x21A4, 0x42C1, {0x81, 0x1B, 0x5F, 0xB5, 0x33, 0xCE, 0xE6, 0x9F}}
NS_DEFINE_CID(kWDEProjectItemPropertyPaneTabCID, WDEPROJECTITEMPROPERTYPANETAB_CID);

#define WDEPROJECTITEMPROPERTYPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEProjectItemPropertyPaneTab;1"

class WDEProjectItemPropertyPaneTab : public WDEIProjectItemPropertyPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPROJECTITEMPROPERTYPANETAB
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)

  WDEProjectItemPropertyPaneTab();
  virtual ~WDEProjectItemPropertyPaneTab();
  /* additional members */
};

#endif
