#include "WDEPropertyPane.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEPropertyPane, WDEPane, WDEIPropertyPane)

WDEPropertyPane::WDEPropertyPane()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
	m_projectItemPropertyTab = do_CreateInstance("@gnome.org/SashWDE/WDEProjectItemPropertyPaneTab;1");
	m_htmlWidgetTab = do_CreateInstance("@gnome.org/SashWDE/WDEHtmlWidgetPaneTab;1");
	m_gtkWidgetTab = do_CreateInstance("@gnome.org/SashWDE/WDEGtkWidgetPaneTab;1");
}

WDEPropertyPane::~WDEPropertyPane()
{
  /* destructor code */
}

/* readonly attribute WDEIProjectItemPropertyPaneTab projectItemPropertyTab; */
NS_IMETHODIMP WDEPropertyPane::GetProjectItemPropertyTab(WDEIProjectItemPropertyPaneTab * *aProjectItemPropertyTab)
{
	XPCOM_COMPTR_GETTER(m_projectItemPropertyTab, aProjectItemPropertyTab);
}

/* readonly attribute WDEIHtmlWidgetPaneTab htmlWidgetTab; */
NS_IMETHODIMP WDEPropertyPane::GetHtmlWidgetTab(WDEIHtmlWidgetPaneTab * *aHtmlWidgetTab)
{
	XPCOM_COMPTR_GETTER(m_htmlWidgetTab, aHtmlWidgetTab);
}

/* readonly attribute WDEIGtkWidgetPaneTab gtkWidgetTab; */
NS_IMETHODIMP WDEPropertyPane::GetGtkWidgetTab(WDEIGtkWidgetPaneTab * *aGtkWidgetTab)
{
	XPCOM_COMPTR_GETTER(m_gtkWidgetTab, aGtkWidgetTab);
}

/* void build (); */
NS_IMETHODIMP WDEPropertyPane::Build()
{
	m_projectItemPropertyTab->Build();
	m_htmlWidgetTab->Build();
	m_gtkWidgetTab->Build();
	
	nsCOMPtr<WDEIPaneTab> t;
	t = do_QueryInterface(m_projectItemPropertyTab);
	AppendTab(t);
	
	t = do_QueryInterface(m_htmlWidgetTab);
	AppendTab(t);
	
	t = do_QueryInterface(m_gtkWidgetTab);
	AppendTab(t);
	
	SetName("Property Pane");
	m_popupMenu->AddItem("Properties\\Something Specific", 3001);
	
	return NS_OK;
}

