#ifndef _WDEPROPERTYPANE_H
#define _WDEPROPERTYPANE_H

#include <gtk/gtk.h>
#include "WDEIPropertyPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//8AE06FD0-DD6F-48EB-9C99-59429757E242
#define WDEPROPERTYPANE_CID {0x8AE06FD0, 0xDD6F, 0x48EB, {0x9C, 0x99, 0x59, 0x42, 0x97, 0x57, 0xE2, 0x42}}
NS_DEFINE_CID(kWDEPropertyPaneCID, WDEPROPERTYPANE_CID);

#define WDEPROPERTYPANE_CONTRACT_ID "@gnome.org/SashWDE/WDEPropertyPane;1"

class WDEPropertyPane : public WDEIPropertyPane, public WDEPane
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPROPERTYPANE
	NS_FORWARD_WDEIPANE(WDEPane::)

  WDEPropertyPane();
  virtual ~WDEPropertyPane();
  /* additional members */

private:
	nsCOMPtr<WDEIProjectItemPropertyPaneTab> m_projectItemPropertyTab;
	nsCOMPtr<WDEIHtmlWidgetPaneTab> m_htmlWidgetTab;
	nsCOMPtr<WDEIGtkWidgetPaneTab> m_gtkWidgetTab;
};

#endif
