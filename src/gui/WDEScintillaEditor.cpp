#include "WDEScintillaEditor.h"
#include "xpcomtools.h"
#include "Platform.h" //scintilla's
#include "Scintilla.h"
#include "ScintillaWidget.h"
#include "SciLexer.h"

NS_IMPL_ISUPPORTS2(WDEScintillaEditor, WDEITextEditor, WDEIEventSender)

WDEScintillaEditor::WDEScintillaEditor() {
  NS_INIT_ISUPPORTS();
  WDE_INIT_SENDER
  
  // ALL SCINTILLA STUFF GOES HERE!!

  m_port= gtk_viewport_new(NULL, NULL);
  gtk_widget_show(m_port);

  // new scintilla widget
  m_scintilla= scintilla_new();
  gtk_widget_set_usize(m_scintilla, 100, 100);
  gtk_widget_show(m_scintilla);
  //connect the scintilla's notifications to our own handler,
  // so we can deal with its events
  gtk_signal_connect(
    GTK_OBJECT(m_scintilla), "notify",
    GtkSignalFunc(WDEScintillaEditorNotify), this);

  gtk_container_add(GTK_CONTAINER(m_port), m_scintilla);

  
}

WDEScintillaEditor::~WDEScintillaEditor() {
  gtk_widget_unref(m_scintilla);
}

/* attribute string text; */
NS_IMETHODIMP WDEScintillaEditor::GetText(char * *aText) {
  PRInt32 size;
  GetTextLength(&size);
  *aText= (char*) g_malloc((size+ 1)* sizeof(char));
  scintilla_send_message(
    SCINTILLA(m_scintilla), SCI_GETTEXT, size+ 1, (long) *aText);
  return NS_OK;
}

NS_IMETHODIMP WDEScintillaEditor::SetText(const char * aText) {
  int length= strlen(aText);
  scintilla_send_message(
    SCINTILLA(m_scintilla), SCI_CLEARALL, 0, 0);
  scintilla_send_message(
    SCINTILLA(m_scintilla), SCI_ADDTEXT, length, (long) aText);
  return NS_OK;
}

/* readonly attribute long textLength; */
NS_IMETHODIMP WDEScintillaEditor::GetTextLength(PRInt32 *aTextLength) {
	*aTextLength= scintilla_send_message(
	  SCINTILLA(m_scintilla), SCI_GETLENGTH, 0, 0);
  return NS_OK;
}

/* attribute boolean dirty; */
NS_IMETHODIMP WDEScintillaEditor::GetDirty(PRBool *aDirty)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEScintillaEditor::SetDirty(PRBool aDirty)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void appendText (in string text); */
NS_IMETHODIMP WDEScintillaEditor::AppendText(const char *text)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void prependText (in string text); */
NS_IMETHODIMP WDEScintillaEditor::PrependText(const char *text)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute string currentWord; */
NS_IMETHODIMP WDEScintillaEditor::GetCurrentWord(char * *aCurrentWord)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* readonly attribute string currentCharacter; */
NS_IMETHODIMP WDEScintillaEditor::GetCurrentCharacter(char * *aCurrentCharacter)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute long currentLine; */
NS_IMETHODIMP WDEScintillaEditor::GetCurrentLine(PRInt32 *aCurrentLine)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEScintillaEditor::SetCurrentLine(PRInt32 aCurrentLine)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void cut (); */
NS_IMETHODIMP WDEScintillaEditor::Cut()
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void copy (); */
NS_IMETHODIMP WDEScintillaEditor::Copy()
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void paste (); */
NS_IMETHODIMP WDEScintillaEditor::Paste()
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void clear (); */
NS_IMETHODIMP WDEScintillaEditor::Clear()
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void selectAll (); */
NS_IMETHODIMP WDEScintillaEditor::SelectAll()
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void undo (); */
NS_IMETHODIMP WDEScintillaEditor::Undo()
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void redo (); */
NS_IMETHODIMP WDEScintillaEditor::Redo()
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute long currentPosition; */
NS_IMETHODIMP WDEScintillaEditor::GetCurrentPosition(PRInt32 *aCurrentPosition)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEScintillaEditor::SetCurrentPosition(PRInt32 aCurrentPosition)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long findText (in string text, in boolean caseSensitive, in boolean startWord, in boolean wholeWord); */
NS_IMETHODIMP WDEScintillaEditor::FindText(const char *text, PRBool caseSensitive, PRBool startWord, PRBool wholeWord, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* [noscript] GtkWidget getGtkWidget (); */
NS_IMETHODIMP WDEScintillaEditor::GetGtkWidget(GtkWidget * *_retval) {
  // keep the widget windowed!
  *_retval= m_port;
  return NS_OK;
}

extern "C" void WDEScintillaEditorNotify(GtkWidget* wid, gint wParam, gpointer lParam, WDEScintillaEditor *editor) {
//  cerr << "SCI MESSAGE" << endl;
}
