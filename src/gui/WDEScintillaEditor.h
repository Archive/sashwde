#ifndef _WDESCINTILLAEDITOR_H
#define _WDESCINTILLAEDITOR_H

#include <gtk/gtk.h>
#include "WDEITextEditor.h"
#include "WDEIGUI.h"
#include "nsCOMPtr.h"
#include "WDEEvents.h"

//0cd20683-2af8-4ebf-8a67-7bc51abaebb8
#define WDESCINTILLAEDITOR_CID {0x0cd20683, 0x2af8, 0x4ebf, {0x8a, 0x67, 0x7b, 0xc5, 0x1a, 0xba, 0xeb, 0xb8}} 
NS_DEFINE_CID(kWDEScintillaEditorCID, WDESCINTILLAEDITOR_CID);

#define WDESCINTILLAEDITOR_CONTRACT_ID "@gnome.org/SashWDE/WDEScintillaEditor;1"

class WDEScintillaEditor:
  public WDEITextEditor,
  public WDEIEventSender
{
  public:
    NS_DECL_ISUPPORTS
    NS_DECL_WDEITEXTEDITOR
    WDE_DECL_SENDER

    WDEScintillaEditor();
    virtual ~WDEScintillaEditor();
    /* additional members */
    
  private:
    GtkWidget *m_scintilla;
    GtkWidget *m_port;
};

extern "C" void WDEScintillaEditorNotify(GtkWidget* wid, gint wParam, gpointer lParam, WDEScintillaEditor *editor);

#endif
