#include "WDESourcePane.h"
//#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDESourcePane, WDEPane, WDEISourcePane)

WDESourcePane::WDESourcePane()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDESourcePane::~WDESourcePane() {
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDESourcePane::Build() {
  SetName("Source Pane");
  m_popupMenu->AddItem("Source\\Properties", 2001);
  m_popupMenu->AddItem("Source\\AutoIndent", 2002);
	return NS_OK;
}

NS_IMETHODIMP WDESourcePane::CreateGenericSourceTab(WDEIGenericSourceTab * *_retval) {
  nsCOMPtr<WDEIGenericSourceTab> result= do_CreateInstance(WDEGENERICSOURCETAB_CONTRACT_ID);  
  XPCOM_COMPTR_GETTER(result, _retval);
}

