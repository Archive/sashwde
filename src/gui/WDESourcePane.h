#ifndef _WDESOURCEPANE_H
#define _WDESOURCEPANE_H

#include <gtk/gtk.h>
#include "WDEISourcePane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"
#include "WDEGenericSourceTab.h"

//C05615F8-ACC0-4437-B5CD-260D0DB1C842
#define WDESOURCEPANE_CID {0xC05615F8, 0xACC0, 0x4437, {0xB5, 0xCD, 0x26, 0x0D, 0x0D, 0xB1, 0xC8, 0x42}}
NS_DEFINE_CID(kWDESourcePaneCID, WDESOURCEPANE_CID);

#define WDESOURCEPANE_CONTRACT_ID "@gnome.org/SashWDE/WDESourcePane;1"

class WDESourcePane : public WDEISourcePane, public WDEPane
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEISOURCEPANE
	NS_FORWARD_WDEIPANE(WDEPane::)

  WDESourcePane();
  virtual ~WDESourcePane();
};

#endif
