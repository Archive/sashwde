#include "WDEWeblicationDebugPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEWeblicationDebugPaneTab, WDEPaneTab, WDEIWeblicationDebugPaneTab)

WDEWeblicationDebugPaneTab::WDEWeblicationDebugPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEWeblicationDebugPaneTab::~WDEWeblicationDebugPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEWeblicationDebugPaneTab::Build()
{
	m_name = "Debug Output";
	return NS_OK;
}
