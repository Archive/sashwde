#ifndef _WDEWEBLICATIONDEBUGPANETAB_H
#define _WDEWEBLICATIONDEBUGPANETAB_H

#include <gtk/gtk.h>
#include "WDEIDebugOutputPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//5C120556-02CC-4691-A07E-F7B9B1140F38
#define WDEWEBLICATIONDEBUGPANETAB_CID {0x5C120556, 0x02CC, 0x4691, {0xA0, 0x7E, 0xF7, 0xB9, 0xB1, 0x14, 0x0F, 0x38}}
NS_DEFINE_CID(kWDEWeblicationDebugPaneTabCID, WDEWEBLICATIONDEBUGPANETAB_CID);

#define WDEWEBLICATIONDEBUGPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEWeblicationDebugPaneTab;1"

class WDEWeblicationDebugPaneTab : public WDEIWeblicationDebugPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIWEBLICATIONDEBUGPANETAB
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)
		
  WDEWeblicationDebugPaneTab();
  virtual ~WDEWeblicationDebugPaneTab();
  /* additional members */
};

#endif
