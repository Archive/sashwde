#include "WDEWeblicationOutputPaneTab.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS_INHERITED1(WDEWeblicationOutputPaneTab, WDEPaneTab, WDEIWeblicationOutputPaneTab)

WDEWeblicationOutputPaneTab::WDEWeblicationOutputPaneTab()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEWeblicationOutputPaneTab::~WDEWeblicationOutputPaneTab()
{
  /* destructor code */
}

/* void build (); */
NS_IMETHODIMP WDEWeblicationOutputPaneTab::Build()
{
	m_name = "Run Output";
	return NS_OK;
}
