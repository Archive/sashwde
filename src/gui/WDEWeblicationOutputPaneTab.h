#ifndef _WDEWEBLICATIONOUTPUTPANETAB_H
#define _WDEWEBLICATIONOUTPUTPANETAB_H

#include <gtk/gtk.h>
#include "WDEIDebugOutputPane.h"
#include <string>
#include "nsCOMPtr.h"
#include "WDEPane.h"

//3C386DA4-A433-4BBD-B8B6-A38B001C2C2F
#define WDEWEBLICATIONOUTPUTPANETAB_CID {0x3C386DA4, 0xA433, 0x4BBD, {0xB8, 0xB6, 0xA3, 0x8B, 0x00, 0x1C, 0x2C, 0x2F}}
NS_DEFINE_CID(kWDEWeblicationOutputPaneTabCID, WDEWEBLICATIONOUTPUTPANETAB_CID);

#define WDEWEBLICATIONOUTPUTPANETAB_CONTRACT_ID "@gnome.org/SashWDE/WDEWeblicationOutputPaneTab;1"

class WDEWeblicationOutputPaneTab : public WDEIWeblicationOutputPaneTab, public WDEPaneTab
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIWEBLICATIONOUTPUTPANETAB
	NS_FORWARD_WDEIPANETAB(WDEPaneTab::)

  WDEWeblicationOutputPaneTab();
  virtual ~WDEWeblicationOutputPaneTab();
  /* additional members */
};

#endif
