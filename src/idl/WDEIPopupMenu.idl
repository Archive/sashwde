#include "nsISupports.idl"
#include "nsISupportsPrimitives.idl"

[ptr] native GtkWidgetPM(GtkWidget);


/**
  The events from a popup menu have the
  following format:
  
    ID = WDEIEvent::PopupMenuItemSelected
    attr = ActionCode
    str = Item text
    ptr = (null)
  
*/
[scriptable, uuid(faf21030-4301-490d-89c8-dc66bf4116c4)]
interface WDEIPopupMenu : nsISupports
{
  // Adds a menu item with action code
  void AddItem(in string menuPath, in long actionCode);
  // return the action code for the item, -1 if none
  long ItemActionCode(in string menuPath);
  // returns true if ItemActionCode != -1
  boolean ContainsItem(in string menuPath);
  // remove a menu item
  void RemoveItem(in string menuPath);
  // remove a menu item by its action code
  void RemoveItemByCode(in long actionCode);
  // make this menu the default menu for the holder
  [noscript] void AttachMenu(in GtkWidgetPM holder);
  // detach the menu from the holder
  [noscript] void DetachMenu(in GtkWidgetPM holder);
};
