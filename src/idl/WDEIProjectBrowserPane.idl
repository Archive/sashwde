/***************************************************************
    SashWDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

#include "nsISupports.idl"
#include "nsISupportsPrimitives.idl"
#include "WDEIGUI.idl"
#include "WDEIContentHandler.idl"
#include "WDEIDomElement.idl"

interface WDEIProjectItem;

[scriptable, uuid(78274D47-9FD5-4939-8B5E-71F8B2D7AA9D)]
interface WDEIProjectBrowserPaneTab : WDEIPaneTab
{
  void build();
  void addItem(in WDEIProjectItem item);
  
  /**
    The tab should display the current state of the project
    like a list of project files. It should report on the currently
    selected file by sending out a WDEIEvent::ProjectBrowserSelectedFile.
    The project browser content handler should display appropriate
    properties in the PropertyPane::ProjectItem tab. It should request
    a popup menu to be created for each item by the content handler
  **/
  
  //! Refresh display
  // disabled [noscript] void Refresh();
};

[scriptable, uuid(4930E598-432F-432A-B460-082320D5C359)]
interface WDEIFunctionBrowserPaneTab : WDEIPaneTab
{
  void build();
};

[scriptable, uuid(9BC8898C-4058-4949-B9C9-C876936DB228)]
interface WDEIMacroBrowserPaneTab : WDEIPaneTab
{
  void build();
};

[scriptable, uuid(D26C0556-BB3C-44AE-B16E-02B2DD3A33FD)]
interface WDEIProjectBrowserPane : WDEIPane
{
  readonly attribute WDEIProjectBrowserPaneTab projectBrowserTab;
  readonly attribute WDEIFunctionBrowserPaneTab functionBrowserTab;
  readonly attribute WDEIMacroBrowserPaneTab macroBrowserTab;
  
  void build();
};
