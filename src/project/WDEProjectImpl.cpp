/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/

// WDEProjectImpl.cpp

// Contributor(s): 

// Jordi Albornoz, June 2000
// Kevin Gibbs, June 2000

// Tyeler Quentmeyer, July 2001
// David Chisholm, July 2001
// Leonard Lee, July 2001
// Stefan Atev, July 2001

// Last update: 1 August 2001

#include "WDEProjectImpl.h"
#include "WDEApp.h"
#include "xpcomtools.h"

//#include "wdf.h"

NS_IMPL_ISUPPORTS1(WDEProject, WDEIProject)

WDEProject::WDEProject()
{
  NS_INIT_ISUPPORTS();

  m_path = "";
  m_name = "";
  m_weblicationID = "";
  m_author = "";
  m_title = "";
  m_abstract = "";
  m_extensionType = 0;
}

WDEProject::~WDEProject()
{
}

/* attribute string path; */
NS_IMETHODIMP WDEProject::GetPath(char * *aPath)
{
  XPCOM_STR_GETTER(m_path.c_str(), aPath);
}
NS_IMETHODIMP WDEProject::SetPath(const char * aPath)
{
  XPCOM_SIMPLE_SETTER(m_path, aPath);
}

/* attribute string name; */
NS_IMETHODIMP WDEProject::GetName(char * *aName)
{
  XPCOM_STR_GETTER(m_name.c_str(), aName);
}
NS_IMETHODIMP WDEProject::SetName(const char * aName)
{
  XPCOM_SIMPLE_SETTER(m_name, aName);
}

/* attribute string weblicationID; */
NS_IMETHODIMP WDEProject::GetWeblicationID(char * *aWeblicationID)
{
  XPCOM_STR_GETTER(m_weblicationID.c_str(), aWeblicationID);
}
NS_IMETHODIMP WDEProject::SetWeblicationID(const char * aWeblicationID)
{
  XPCOM_SIMPLE_SETTER(m_weblicationID, aWeblicationID);
}

/* attribute string author; */
NS_IMETHODIMP WDEProject::GetAuthor(char * *aAuthor)
{
  XPCOM_STR_GETTER(m_author.c_str(), aAuthor);
}
NS_IMETHODIMP WDEProject::SetAuthor(const char * aAuthor)
{
  XPCOM_SIMPLE_SETTER(m_author, aAuthor);
}

/* attribute string title; */
NS_IMETHODIMP WDEProject::GetTitle(char * *aTitle)
{
  XPCOM_STR_GETTER(m_title.c_str(), aTitle);
}
NS_IMETHODIMP WDEProject::SetTitle(const char * aTitle)
{
  XPCOM_SIMPLE_SETTER(m_title, aTitle);
}

/* attribute string abstract; */
NS_IMETHODIMP WDEProject::GetAbstract(char * *aAbstract)
{
  XPCOM_STR_GETTER(m_abstract.c_str(), aAbstract);
}
NS_IMETHODIMP WDEProject::SetAbstract(const char * aAbstract)
{
  XPCOM_SIMPLE_SETTER(m_abstract, aAbstract);
}

/* attribute PRInt32 extensionType; */
NS_IMETHODIMP WDEProject::GetExtensionType(PRInt32 *aExtensionType)
{
  XPCOM_SIMPLE_GETTER(m_extensionType, aExtensionType);
}
NS_IMETHODIMP WDEProject::SetExtensionType(PRInt32 aExtensionType)
{
  XPCOM_SIMPLE_SETTER(m_extensionType, aExtensionType);
}
