/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/


#ifndef _WDEPROJECT_H
#define _WDEPROJECT_H

#include "WDEIProject.h"
#include "nsCOMPtr.h"
#include <string>

//5e5856b2-b1a0-4637-94a2-6332aeb6d7b7
#define WDEPROJECT_CID { 0x5e5856b2, 0xb1a0, 0x4637, { 0x94, 0xa2, 0x63, 0x32, 0xae, 0xb6, 0xd7, 0xb7 }}
NS_DEFINE_CID(kWDEProjectCID, WDEPROJECT_CID);

#define WDEPROJECT_CONTRACT_ID "@gnome.org/SashWDE/WDEProject;1"

class WDEProject : public WDEIProject
{
 public:
  NS_DECL_ISUPPORTS;
  NS_DECL_WDEIPROJECT;
    
  WDEProject();
  virtual ~WDEProject();

  /* additional members */
 protected:
  string m_path;
  string m_name;
	
  string m_weblicationID;
  string m_author;
  string m_title;
  string m_abstract;
  int m_extensionType;	
};

#endif
