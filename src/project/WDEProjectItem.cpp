#include "WDEProjectItem.h"

NS_IMPL_ISUPPORTS_INHERITED2(WDEProjectItem, WDEDomElement, WDEIProjectItem, WDEIDomElement)

WDEProjectItem::WDEProjectItem()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
  m_project = NULL;
  m_ch = NULL;
}

WDEProjectItem::~WDEProjectItem()
{
  /* destructor code */
}

/* attribute WDEIProject project; */
NS_IMETHODIMP WDEProjectItem::GetProject(WDEIProject * *aProject)
{
  XPCOM_COMPTR_GETTER(m_project, aProject);
}
NS_IMETHODIMP WDEProjectItem::SetProject(WDEIProject * aProject)
{
  XPCOM_SIMPLE_SETTER(m_project, aProject);
}

/* attribute WDEIContentHandler contentHandler; */
NS_IMETHODIMP WDEProjectItem::GetContentHandler(WDEIContentHandler * *aContentHandler)
{
  XPCOM_COMPTR_GETTER(m_ch, aContentHandler);
}
NS_IMETHODIMP WDEProjectItem::SetContentHandler(WDEIContentHandler * aContentHandler)
{
  XPCOM_SIMPLE_SETTER(m_ch, aContentHandler);
}
