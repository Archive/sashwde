#ifndef _WDEPROJECTITEM_H
#define _WDEPROJECTITEM_H

#include "WDEDomElement.h"
#include "WDEIProject.h"
#include "WDEIProjectItem.h"
#include "WDEIContentHandler.h"

//848ACF3C-2C0C-4EA8-9053-2D6829DF0BFE
#define WDEPROJECTITEM_CID {0x848ACF3C, 0x2C0C, 0x4EA8, {0x90, 0x53, 0x2D, 0x68, 0x29, 0xDF, 0x0B, 0xFE}}
NS_DEFINE_CID(kWDEProjectItemCID, WDEPROJECTITEM_CID);

#define WDEPROJECTITEM_CONTRACT_ID "@gnome.org/SashWDE/WDEProjectItem;1"

class WDEProjectItem : public WDEIProjectItem, public WDEDomElement
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPROJECTITEM
  NS_DECL_WDEIDOMELEMENT

  WDEProjectItem();
  virtual ~WDEProjectItem();
  /* additional members */

protected:
  nsCOMPtr<WDEIProject> m_project;
  nsCOMPtr<WDEIContentHandler> m_ch;
};

#endif



