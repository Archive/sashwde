#include "WDEProjectImpl.h"
#include "WDEApp.h"

NS_GENERIC_FACTORY_CONSTRUCTOR(WDEProject)
				
static nsModuleComponentInfo components[] = {
  {
    "WDEProject module",
    WDEPROJECT_CID,
    WDEPROJECT_CONTRACT_ID,
    WDEProjectConstructor,
    NULL,
    NULL
  } 
};
	
NS_IMPL_NSGETMODULE("WDEProject module", components)
