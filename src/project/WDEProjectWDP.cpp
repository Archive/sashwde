
/***************************************************************
    SashWDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

/* Stefan Atev, July 2001 */

#include "XMLDocument.h"
#include "WDEProjectImpl.h"
#include "WDEAction.h"
#include "WDEExtension.h"
#include "WDETreeFileItem.h"
#include "WDETreeGladeFormItem.h"
#include "WDEInstallPage.h"
#include "WDEPlatform.h"
#include "wdf.h"
#include "prtypes.h"
#include "nsCRT.h"
#include "WDEApp.h"
#include "WDEDevKit.h"
#include "SecurityHash.h"

/*********************************************************************
  CONVERSION ROUTINES SCAVENGED FROM THE OLD WDF
*********************************************************************/

string IntToString(const int n) {
  static char str[100];
  sprintf(str, "%d", n);
  return string(str);
}

int StringToInt(const string &x) {
  return atoi(x.c_str());
}

string BoolToString(const bool x) {
  return (x ? string("true") : string("false"));
}

bool StringToBool(const string &s) {
  return (s == "true");
}

string FloatToString(const float n) {
  static char str[100];
  sprintf(str, "%f", n);
  return string(str);
}

float StringToFloat(const string &x) {
  return atoi(x.c_str());
}  

/*********************************************************************

  WARNING! YOU ARE ENTERING THE TWILIGHT ZONE
  
  The functions needed to save and load the WDP file are here. Change
  at you own risk.
  Regards, Stefan Atev

*********************************************************************/

void WDEProject::saveAction(XMLNode &root, WDEITreeItem *item) {
	WDEIAction *IAction;
	nsresult rv;
	char *str;
	int index;
	XMLNode action_node("action");

	rv= item->QueryInterface(NS_GET_IID(WDEIAction), (void **) &IAction);
	if (NS_FAILED(rv)) return;
  IAction->GetActionID(&str);
  action_node.setAttribute("id", str);
  nsCRT::free(str);
  IAction->GetIndex(&index);
  if (WDEApp::m_locationDKs[index]->ghost)
    action_node.setAttribute("weblicationid", WDEApp::m_locationDKs[index]->weblicationid);
  else {
    char *cid;
    WDEApp::m_locationDKs[index]->DKloc->GetTargetCID(&cid);
    action_node.setAttribute("weblicationid", cid);
    nsCRT::free(cid);
  }
  IAction->GetDeployPath(&str);
  action_node.setAttribute("deploypath", str);
  nsCRT::free(str);

  if(!WDEApp::m_locationDKs[index]->ghost) {
	  sashILocationDKLocation *dk;
	  IAction->GetDevkit(&dk);
		dk->GenerateRegistrationString();
		dk->GetRegistration(&str);
		string tempReg= string("<registration>")+ string(str)+ string("</registration>");
		action_node.appendXML(tempReg);
		nsCRT::free(str);
	}
	else {
		IAction->GetGhostRegistration(&str);
		string tempReg= string("<registration>")+ string(str)+ string("</registration>");
		action_node.appendXML(tempReg);
		nsCRT::free(str);
	}

  NS_IF_RELEASE(IAction);
  root.importChild(action_node);
}

void WDEProject::saveExtension(XMLNode &root, WDEITreeItem *item) {
	WDEIExtension *IExtension;
	nsresult rv;
	int index;
	char *str;
	XMLNode extension_node("extension");

	rv= item->QueryInterface(NS_GET_IID(WDEIExtension), (void **) &IExtension);
	if (NS_FAILED(rv)) return;
  IExtension->GetIndex(&index);
  extension_node.setAttribute("actionid", WDEApp::m_extensionDKs[index]->actionid);
  if (WDEApp::m_extensionDKs[index]->ghost)
    extension_node.setAttribute("weblicationid", WDEApp::m_extensionDKs[index]->weblicationid);
  else {
    char *cid;
    WDEApp::m_extensionDKs[index]->DKloc->GetTargetCID(&cid);
    extension_node.setAttribute("weblicationid", cid);
    nsCRT::free(cid);
  }
  IExtension->GetDeployPath(&str);
  extension_node.setAttribute("deploypath", str);
  nsCRT::free(str);
  NS_IF_RELEASE(IExtension);
  root.importChild(extension_node);
}

void WDEProject::savePlatform(XMLNode &root, WDEITreeItem *item) {
	WDEIPlatform *IPlatform;
	nsresult rv;
	int platform;
	XMLNode platform_node("platform");

	rv= item->QueryInterface(NS_GET_IID(WDEIPlatform), (void **) &IPlatform);
	if (NS_FAILED(rv)) return;
  IPlatform->GetPlatform(&platform);
  platform_node.setAttribute("id", IntToString(platform));
  NS_IF_RELEASE(IPlatform);
  root.importChild(platform_node);
}

void WDEProject::saveFileItem(XMLNode &root, WDEITreeItem *item) {
	WDEITreeFileItem *IFileItem;
	nsresult rv;
	char *str;
	int aint;
	PRBool abool;
	XMLNode fileitem_node("file");

	rv= item->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &IFileItem);
	if (NS_FAILED(rv)) return;
  IFileItem->GetFileType(&aint);
  fileitem_node.setAttribute("type", IntToString(aint));
//  IFileItem->GetFileName(&str);
  item->GetName(&str);
  fileitem_node.setAttribute("name", str);
  nsCRT::free(str);
  IFileItem->GetPreCache(&abool);
  fileitem_node.setAttribute("precache", BoolToString(abool));
  IFileItem->GetLocation(&aint);
  fileitem_node.setAttribute("location", IntToString(aint));
  NS_IF_RELEASE(IFileItem);
  root.importChild(fileitem_node);
}

void WDEProject::saveInstallPage(XMLNode &root, WDEITreeItem *item) {
	WDEIInstallPage *IPage;
	nsresult rv;
	char *str;
	int aint;
	XMLNode page_node("installpage");

	rv= item->QueryInterface(NS_GET_IID(WDEIInstallPage), (void **) &IPage);
	if (NS_FAILED(rv)) return;
  IPage->GetScreenType(&aint);
  page_node.setAttribute("type", IntToString(aint));
  IPage->GetTitle(&str);
  page_node.setAttribute("title", str);
  nsCRT::free(str);
  NS_IF_RELEASE(IPage);
  root.importChild(page_node);
}

void WDEProject::saveGladeForm(XMLNode &root, WDEITreeItem *item) {
	WDEITreeGladeFormItem *IGladeForm;
	nsresult rv;
	XMLNode gladeform_node("gladeform");

	rv= item->QueryInterface(NS_GET_IID(WDEITreeGladeFormItem), (void **) &IGladeForm);
	if (NS_FAILED(rv)) return;
  NS_IF_RELEASE(IGladeForm);
  gladeform_node.setAttribute("dummy", "make sure that we load the correct interface");
  root.importChild(gladeform_node);
}

void WDEProject::saveProjectItem(XMLNode &root, WDEITreeItem *item) {
	char *str;
	int aint;
  int length;
	PRBool abool;
	XMLNode item_node("item");
  item->GetType(&aint);
  item_node.setAttribute("type", IntToString(aint));
  item->GetName(&str);
  item_node.setAttribute("name", str);
  nsCRT::free(str);
  item->GetDescription(&str);
  item_node.setAttribute("description", str);
  nsCRT::free(str);
  item->GetAlphaSortChildren(&abool);
  item_node.setAttribute("sort", BoolToString(abool));
  
  saveAction(item_node, item);
  saveExtension(item_node, item);
  savePlatform(item_node, item);
  saveFileItem(item_node, item);
  saveInstallPage(item_node, item);
  saveGladeForm(item_node, item);

	item->GetLength(&length);
	for (int i= 0; i< length; ++i) {
	  WDEITreeItem *it, *it2= NULL;
		item->GetChild(i, &it);
		if (NS_SUCCEEDED(it->QueryInterface(NS_GET_IID(WDEITreeItem), (void **) &it2)))
      saveProjectItem(item_node, it2);
    NS_IF_RELEASE(it);
    NS_IF_RELEASE(it2);
  }
  
  root.importChild(item_node);
}

void WDEProject::saveSecurityAndRegistrySettings(XMLNode &root) {
  Registry sec_reg;
  // Taken from SecurityHash.cpp
//  g_hash_table_foreach(m_security, security_item_write_to_registry, &sec_reg);
  XMLNode happy= sec_reg.GetRegistryXMLStructure();
  XMLNode neu("security");
  if (!happy.isNull()) {
    neu.importChild(happy);
    root.importChild(neu);
  }
  Registry *def= NULL;
  if (m_registry) m_registry->GetInternalRegistry(&def);
  if (def) {
    XMLNode regN("registry");
		
		//yo stefan...
		//hopefully this works
		XMLDocument xmlDocHack;
		xmlDocHack.loadFromString(def->GetRegistryXMLStructure().getXML());
		regN.importChild(xmlDocHack.getRootNode());    
		//regN.importChild(def->GetRegistryXMLStructure());
		
    root.importChild(regN);
  }
}

bool WDEProject::saveProjectToFile(const string &file_name) {
  XMLDocument doc;
  doc.createRootNode("wdp");
  XMLNode &root= doc.getRootNode();

  root.setAttribute("weblicationid", m_weblicationID);
  root.setAttribute("author", m_author);
  root.setAttribute("title", m_title);
  root.setAttribute("extensiontype", IntToString(m_extensionType));
  XMLNode abstract("abstract");
  abstract.setText(m_abstract);
  root.importChild(abstract);
  cerr << "Saving security and registry" << endl;
  saveSecurityAndRegistrySettings(root);
  saveProjectItem(root, m_files);
  doc.saveToFile(file_name);
  return true;
}

/*********************************************************************
Loading section...
*********************************************************************/
void WDEProject::loadAction(XMLNode &root, WDEITreeItem *item) {
	WDEIAction *IAction;
	nsresult rv;
	int index;
	string weblicationid, deploy;
	XMLNode action_node= root.getFirstChildNode("action");

	rv= item->QueryInterface(NS_GET_IID(WDEIAction), (void **) &IAction);
	if (NS_FAILED(rv)) return;
  IAction->SetActionID(action_node.getAttribute("id").c_str());
  weblicationid= action_node.getAttribute("weblicationid");
  index= -1;
  for (unsigned int i= 0; i< WDEApp::m_locationDKs.size(); ++i)
    if (WDEApp::m_locationDKs[i]->ghost) {
      if (WDEApp::m_locationDKs[i]->weblicationid== weblicationid) {
        index= i;
        break;
      }
    }
    else {
      char *cid;
      WDEApp::m_locationDKs[i]->DKloc->GetTargetCID(&cid);
      if (cid== weblicationid) {
        index= i;
        nsCRT::free(cid);
        break;
      }
      nsCRT::free(cid);
    }
  IAction->SetIndex(index);
  deploy= action_node.getAttribute("deploypath");
  IAction->SetDeployPath(deploy.c_str());

  if(index!= -1)
	  if(!WDEApp::m_locationDKs[index]->ghost) {
		  IAction->ConstructDevkit();
		  string reg= "";
		  // For backwards compatibility with the old way
		  // of saving the registration string we have
		  // to handle the case of reg.str. being an attribute
		  // and not a child node
		  if (action_node.hasChildNodes("registration")> 0) {
		    reg= action_node.getFirstChildNode("registration").getXML();	
		    int p1, p2;
		    p1= string("<registration>\n").length();
		    p2= string("\n</registration>").length();
	      reg= reg.substr(p1, reg.length()- p2- p1);
		  }
		  else
        reg= action_node.getAttribute("registration");		  
//		  string reg= action_node.getAttribute("registration");
	  	sashILocationDKLocation *dk;
		  IAction->GetDevkit(&dk);
			dk->SetRegistration(reg.c_str());
			dk->LoadActionInitialization(IAction, reg.c_str());
		}
		else {
		  string reg= "";
		  if (action_node.hasChildNodes("registration")> 0) {
		    reg= action_node.getFirstChildNode("registration").getXML();	
		    int p1, p2;
		    p1= string("<registration>\n").length();
		    p2= string("\n</registration>").length();
	      reg= reg.substr(p1, reg.length()- p2- p1);
		  }
		  else
        reg= action_node.getAttribute("registration");		  
  	  IAction->SetGhostRegistration(reg.c_str());
//			IAction->SetGhostRegistration(action_node.getAttribute("registration").c_str());
    }
  NS_IF_RELEASE(IAction);
}

void WDEProject::loadExtension(XMLNode &root, WDEITreeItem *item) {
	WDEIExtension *IExtension;
	nsresult rv;
	int index;
	string weblicationid, deploy;
	XMLNode extension_node= root.getFirstChildNode("extension");

	rv= item->QueryInterface(NS_GET_IID(WDEIExtension), (void **) &IExtension);
	if (NS_FAILED(rv)) return;
	
  weblicationid= extension_node.getAttribute("weblicationid");
  index= -1;
  for (unsigned int i= 0; i< WDEApp::m_extensionDKs.size(); ++i)
    if (WDEApp::m_extensionDKs[i]->ghost) {
      if (WDEApp::m_extensionDKs[i]->weblicationid== weblicationid) {
        index= i;
        break;
      }
    }
    else {
      char *cid;
      WDEApp::m_extensionDKs[i]->DKloc->GetTargetCID(&cid);
      if (cid== weblicationid) {
        index= i;
        nsCRT::free(cid);
        break;
      }
      nsCRT::free(cid);
    }
  IExtension->SetIndex(index);
  deploy= extension_node.getAttribute("deploypath");
  IExtension->SetDeployPath(deploy.c_str());
  NS_IF_RELEASE(IExtension);
}

void WDEProject::loadPlatform(XMLNode &root, WDEITreeItem *item) {
	WDEIPlatform *IPlatform;
	nsresult rv;
	XMLNode platform_node= root.getFirstChildNode("platform");

	rv= item->QueryInterface(NS_GET_IID(WDEIPlatform), (void **) &IPlatform);
	if (NS_FAILED(rv)) return;
  IPlatform->SetPlatform(StringToInt(platform_node.getAttribute("id")));
  NS_IF_RELEASE(IPlatform);
}

void WDEProject::loadFileItem(XMLNode &root, WDEITreeItem *item) {
	WDEITreeFileItem *IFileItem;
	nsresult rv;
	XMLNode fileitem_node= root.getFirstChildNode("file");
	string hello;

	rv= item->QueryInterface(NS_GET_IID(WDEITreeFileItem), (void **) &IFileItem);
	if (NS_FAILED(rv)) return;
  IFileItem->SetFileType(StringToInt(fileitem_node.getAttribute("type")));
  hello= m_path+ fileitem_node.getAttribute("name");
  IFileItem->SetFileName(hello.c_str());
  IFileItem->SetPreCache(StringToBool(fileitem_node.getAttribute("precache")));
  IFileItem->SetLocation(StringToInt(fileitem_node.getAttribute("location")));
  NS_IF_RELEASE(IFileItem);
}

void WDEProject::loadInstallPage(XMLNode &root, WDEITreeItem *item) {
	WDEIInstallPage *IPage;
	nsresult rv;
	XMLNode page_node= root.getFirstChildNode("installpage");

	rv= item->QueryInterface(NS_GET_IID(WDEIInstallPage), (void **) &IPage);
	if (NS_FAILED(rv)) return;
  IPage->SetScreenType(StringToInt(page_node.getAttribute("type")));
  IPage->SetTitle(page_node.getAttribute("title").c_str());
  NS_IF_RELEASE(IPage);
}

void WDEProject::loadGladeForm(XMLNode &root, WDEITreeItem *item) {
	WDEITreeGladeFormItem *IGladeForm;
	nsresult rv;
	XMLNode gladeform_node= root.getFirstChildNode("gladeform");

	rv= item->QueryInterface(NS_GET_IID(WDEITreeGladeFormItem), (void **) &IGladeForm);
	if (NS_FAILED(rv)) return;

  // REMOVE the two automatically created items and replace them later with real
  // ones
  WDEITreeItem *glade_child, *js_child;
  
  item->GetChild(0, &glade_child);	
  item->GetChild(1, &js_child);	
  item->RemoveChild(glade_child);
  item->RemoveChild(js_child);
	
  NS_IF_RELEASE(IGladeForm);
}

bool WDEProject::loadNewItemDetectInterface(XMLNode &node, WDEITreeItem **retval) {
#define SEARCH_AND_CREATE(text, cid) \
  if (node.hasChildNodes(text)> 0) { \
	  nsresult rv= nsComponentManager::CreateInstance(cid, NULL, NS_GET_IID(WDEITreeItem), (void **) retval); \
	  return NS_SUCCEEDED(rv); }
  SEARCH_AND_CREATE("gladeform", kWDETreeGladeFormItemCID);  
  SEARCH_AND_CREATE("platform", kWDEPlatformCID);  
  SEARCH_AND_CREATE("action", kWDEActionCID);  
  SEARCH_AND_CREATE("extension", kWDEExtensionCID);  
  SEARCH_AND_CREATE("installpage", kWDEInstallPageCID);  
  SEARCH_AND_CREATE("file", kWDETreeFileItemCID);  
  nsresult rv= nsComponentManager::CreateInstance(kWDETreeItemCID, NULL, NS_GET_IID(WDEITreeItem), (void **) retval);
  return NS_SUCCEEDED(rv);
}

void WDEProject::loadProjectItem(XMLNode &root, WDEITreeItem **item) {
	if (!loadNewItemDetectInterface(root, item)) return;
	
  (*item)->SetType(StringToInt(root.getAttribute("type")));
  (*item)->SetName(root.getAttribute("name").c_str());
  (*item)->SetDescription(root.getAttribute("description").c_str());
  (*item)->SetAlphaSortChildren(StringToBool(root.getAttribute("sort")));

  loadAction(root, *item);
  loadExtension(root, *item);
  loadPlatform(root, *item);
  loadFileItem(root, *item);
  loadInstallPage(root, *item);
  loadGladeForm(root, *item);
  
  XMLNode traverse;
  traverse= root.getFirstChildNode("item");
  while (!traverse.isNull()) {
    WDEITreeItem *it;
    loadProjectItem(traverse, &it);
    (*item)->AddChild(it);
    NS_IF_RELEASE(it);
    traverse= traverse.getNextSibling("item");
  }
}

void WDEProject::connectResource(WDEITreeItem **dst, WDEITreeItem *src, const string &name) {
  int length;
  bool decide;
  if (!src) return;
  NS_IF_ADDREF(src);
  if (NS_FAILED(src->GetLength(&length))) return;
  for (int i= 0; i< length; ++i) {
    WDEITreeItem *item;
    char *str;
    if (NS_FAILED(src->GetChild(i, &item))) continue;
    item->GetName(&str);
    decide= (name== string(str));
    nsCRT::free(str);
    if (decide) {
      item->QueryInterface(NS_GET_IID(WDEITreeItem), (void **) dst);
      NS_RELEASE(item);
      NS_RELEASE(src);
      return;
    }
    NS_IF_RELEASE(item);
  }
  nsComponentManager::CreateInstance(kWDETreeItemCID, NULL, NS_GET_IID(WDEITreeItem), (void **) dst);
  (*dst)->SetName(name.c_str());
  NS_RELEASE(src);
}

void WDEProject::loadSecurityAndRegistrySettings(XMLNode &root) {
  XMLNode sec= root.getFirstChildNode("security");
  if (!sec.isNull()) {
    Registry sec_reg;
    sec_reg.SetRegistryXMLStructure(sec.getFirstChildNode("SashRegistry"));
    cerr << "Security stuff: " << sec_reg.GetRegistryXMLStructure() << endl;
    cerr << "This is where we fail " << endl;
    SetupSecurity(&sec_reg);
//    m_security= security_hash_new(fname, goodKeys, &sec_reg);
  }
  else
    SetupSecurity(NULL);
  XMLNode regN= root.getFirstChildNode("registry");
  if (!regN.isNull()) {
    Registry rreg;
    rreg.SetRegistryXMLStructure(regN.getFirstChildNode("SashRegistry"));
    if (m_registry) m_registry->SetInternalRegistry(&rreg);
  }
}

bool WDEProject::loadProjectFromFile(const string &file_name) {
	/**
	 * \todo remove ourselves as a listener from m_files (WDEProjectWDP.cpp)
	 */
  XMLDocument doc;
  if (!doc.loadFromFile(file_name)) {
    return false;
  }
  /* GENOCIDE!!! */
  NS_IF_RELEASE(m_HTMLFiles);
  NS_IF_RELEASE(m_jsFiles);
  NS_IF_RELEASE(m_gladeFiles);
  NS_IF_RELEASE(m_imgFiles);
  NS_IF_RELEASE(m_txtFiles);
  NS_IF_RELEASE(m_xmlFiles);
  NS_IF_RELEASE(m_otherFiles);
  NS_IF_RELEASE(m_actions);
  NS_IF_RELEASE(m_resources);
  NS_IF_RELEASE(m_extensions);
  NS_IF_RELEASE(m_installPages);
  NS_IF_RELEASE(m_gladeForms);
  NS_IF_RELEASE(m_platforms);
  NS_IF_RELEASE(m_files);
  
  XMLNode &root= doc.getRootNode();
  XMLNode files= root.getFirstChildNode("item");
  XMLNode abstract= root.getFirstChildNode("abstract");
  m_abstract= abstract.getText();
  m_weblicationID= root.getAttribute("weblicationid");
  m_author= root.getAttribute("author");
  m_title= root.getAttribute("title");
  m_extensionType= StringToInt(root.getAttribute("extensiontype"));
  loadProjectItem(files, (WDEITreeItem **) &m_files);
	WDE_ADD_COM_LISTENER(m_files);

  connectResource((WDEITreeItem **) &m_actions, m_files, "Actions");
  connectResource((WDEITreeItem **) &m_resources, m_files, "Resources");
  connectResource((WDEITreeItem **) &m_extensions, m_files, "Extensions");
  connectResource((WDEITreeItem **) &m_gladeForms, m_files, "Glade Forms");
  connectResource((WDEITreeItem **) &m_installPages, m_files, "Install Pages");
  connectResource((WDEITreeItem **) &m_platforms, m_files, "Platforms");

  connectResource((WDEITreeItem **) &m_HTMLFiles, m_resources, "HTML");
  connectResource((WDEITreeItem **) &m_jsFiles, m_resources, "JavaScript");
  connectResource((WDEITreeItem **) &m_gladeFiles, m_resources, "Glade");
  connectResource((WDEITreeItem **) &m_imgFiles, m_resources, "Images");
  connectResource((WDEITreeItem **) &m_txtFiles, m_resources, "Text");
  connectResource((WDEITreeItem **) &m_xmlFiles, m_resources, "XML");
  connectResource((WDEITreeItem **) &m_otherFiles, m_resources, "Other Files");
  
  // Do that last because otherwise m_extensions and m_actions are both
  // freed and stuff just hangs

  cerr << "Loading security and default registry" << endl;
  loadSecurityAndRegistrySettings(root);

  return true;
}
