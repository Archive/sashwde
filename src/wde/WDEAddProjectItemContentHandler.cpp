#include "WDEAddProjectItemContentHandler.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS1(WDEAddProjectItemContentHandler, WDEIAddProjectItemContentHandler)

WDEAddProjectItemContentHandler::WDEAddProjectItemContentHandler()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEAddProjectItemContentHandler::~WDEAddProjectItemContentHandler()
{
  /* destructor code */
}
