#ifndef _WDEADDPROJECTITEMCONTENTHANDLER_H
#define _WDEADDPROJECTITEMCONTENTHANDLER_H

#include "WDEIContentHandler.h"
#include "nsCOMPtr.h"
#include <string>

//2F5655E4-9A26-4640-BF92-6DC3147BADD9
#define WDEADDPROJECTITEMCONTENTHANDLER_CID {0x2F5655E4, 0x9A26, 0x4640, {0xBF, 0x92, 0x6D, 0xC3, 0x14, 0x7B, 0xAD, 0xD9}}
NS_DEFINE_CID(kWDEAddProjectItemContentHandlerCID, WDEADDPROJECTITEMCONTENTHANDLER_CID);

#define WDEADDPROJECTITEMCONTENTHANDLER_CONTRACT_ID "@gnome.org/SashWDE/WDEAddProjectItemContentHandler;1"

class WDEAddProjectItemContentHandler : public WDEIAddProjectItemContentHandler
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIADDPROJECTITEMCONTENTHANDLER

  WDEAddProjectItemContentHandler();
  virtual ~WDEAddProjectItemContentHandler();
  /* additional members */
};

#endif
