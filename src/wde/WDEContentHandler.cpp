#include "WDEContentHandler.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS1(WDEContentHandler, WDEIContentHandler)

WDEContentHandler::WDEContentHandler()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
	
	m_projectBrowserCH = NULL;
	m_addProjectItemCH = NULL;
	m_editorCH = NULL;
	m_propertyCH = NULL;
}

WDEContentHandler::~WDEContentHandler()
{
  /* destructor code */
}

/* attribute WDEIProjectBrowserContentHandler projectBrowserCH; */
NS_IMETHODIMP WDEContentHandler::GetProjectBrowserCH(WDEIProjectBrowserContentHandler * *aProjectBrowserCH)
{
	XPCOM_COMPTR_GETTER(m_projectBrowserCH, aProjectBrowserCH);
}
NS_IMETHODIMP WDEContentHandler::SetProjectBrowserCH(WDEIProjectBrowserContentHandler * aProjectBrowserCH)
{
	XPCOM_SIMPLE_SETTER(m_projectBrowserCH, aProjectBrowserCH);
}

/* attribute WDEIPropertyContentHandler propertyCH; */
NS_IMETHODIMP WDEContentHandler::GetPropertyCH(WDEIPropertyContentHandler * *aPropertyCH)
{
	XPCOM_COMPTR_GETTER(m_propertyCH, aPropertyCH);
}
NS_IMETHODIMP WDEContentHandler::SetPropertyCH(WDEIPropertyContentHandler * aPropertyCH)
{
	XPCOM_SIMPLE_SETTER(m_propertyCH, aPropertyCH);
}

/* attribute WDEIAddProjectItemContentHandler addProjectItemCH; */
NS_IMETHODIMP WDEContentHandler::GetAddProjectItemCH(WDEIAddProjectItemContentHandler * *aAddProjectItemCH)
{
	XPCOM_COMPTR_GETTER(m_addProjectItemCH, aAddProjectItemCH);
}
NS_IMETHODIMP WDEContentHandler::SetAddProjectItemCH(WDEIAddProjectItemContentHandler * aAddProjectItemCH)
{
	XPCOM_SIMPLE_SETTER(m_addProjectItemCH, aAddProjectItemCH);
}

/* attribute WDEIEditorContentHandler editorCH; */
NS_IMETHODIMP WDEContentHandler::GetEditorCH(WDEIEditorContentHandler * *aEditorCH)
{
	XPCOM_COMPTR_GETTER(m_editorCH, aEditorCH);
}
NS_IMETHODIMP WDEContentHandler::SetEditorCH(WDEIEditorContentHandler * aEditorCH)
{
	XPCOM_SIMPLE_SETTER(m_editorCH, aEditorCH);
}
