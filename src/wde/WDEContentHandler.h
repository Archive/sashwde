#ifndef _WDECONTENTHANDLER_H
#define _WDECONTENTHANDLER_H

#include "WDEIContentHandler.h"
#include "nsCOMPtr.h"
#include <string>

//F9447AE0-1ACE-4147-A552-B597DF046F7C
#define WDECONTENTHANDLER_CID {0xF9447AE0, 0x1ACE, 0x4147, {0xA5, 0x52, 0xB5, 0x97, 0xDF, 0x04, 0x6F, 0x7C}}
NS_DEFINE_CID(kWDEContentHandlerCID, WDECONTENTHANDLER_CID);

#define WDECONTENTHANDLER_CONTRACT_ID "@gnome.org/SashWDE/WDEContentHandler;1"

class WDEContentHandler : public WDEIContentHandler
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEICONTENTHANDLER

  WDEContentHandler();
  virtual ~WDEContentHandler();
  /* additional members */
	
protected:
	nsCOMPtr<WDEIProjectBrowserContentHandler> m_projectBrowserCH;
	nsCOMPtr<WDEIPropertyContentHandler> m_propertyCH;
	nsCOMPtr<WDEIAddProjectItemContentHandler> m_addProjectItemCH;
	nsCOMPtr<WDEIEditorContentHandler> m_editorCH;
};

#endif
