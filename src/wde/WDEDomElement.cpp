#include "WDEDomElement.h"

NS_IMPL_ISUPPORTS1(WDEDomElement, WDEIDomElement)

WDEDomElement::WDEDomElement()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */

  m_children.clear();
  m_alphaSortChildren = true;
	m_name= "";
	m_description= "";
}

WDEDomElement::~WDEDomElement()
{
  /* destructor code */
  int length = m_children.size();
  nsCOMPtr<WDEIDomElement>* childP;
  for(int i = 0; i < length; i++)
  {
    childP = m_children[0];
    m_children[0] = NULL;
    delete childP;
    m_children.erase(m_children.begin()+0);
  }
  
  m_children.clear();
}

/* readonly attribute PRInt32 length; */
NS_IMETHODIMP WDEDomElement::GetLength(PRInt32 *aLength)
{
  XPCOM_SIMPLE_GETTER(m_children.size(), aLength);
}

/* attribute string name; */
NS_IMETHODIMP WDEDomElement::GetName(char * *aName)
{
  XPCOM_STR_GETTER(m_name.c_str(), aName);
}
NS_IMETHODIMP WDEDomElement::SetName(const char * aName)
{
  XPCOM_SIMPLE_SETTER(m_name, aName);
}

/* attribute string description; */
NS_IMETHODIMP WDEDomElement::GetDescription(char * *aDescription)
{
  XPCOM_STR_GETTER(m_description.c_str(), aDescription);
}
NS_IMETHODIMP WDEDomElement::SetDescription(const char * aDescription)
{
  XPCOM_SIMPLE_SETTER(m_description, aDescription);
}

/* attribute WDEIDomElement parent; */
NS_IMETHODIMP WDEDomElement::GetParent(WDEIDomElement * *aParent)
{
  XPCOM_COMPTR_GETTER(m_parent, aParent);
}
NS_IMETHODIMP WDEDomElement::SetParent(WDEIDomElement * aParent)
{
  XPCOM_SIMPLE_SETTER(m_parent, aParent);
}

/* attribute boolean dirty; */
NS_IMETHODIMP WDEDomElement::GetDirty(PRBool *aDirty)
{
  XPCOM_SIMPLE_GETTER(m_dirty, aDirty);
}
NS_IMETHODIMP WDEDomElement::SetDirty(PRBool aDirty)
{
  XPCOM_SIMPLE_SETTER(m_dirty, aDirty);
}

/* attribute PRBool alphaSortChildren; */
NS_IMETHODIMP WDEDomElement::GetAlphaSortChildren(PRBool *aAlphaSortChildren)
{
  XPCOM_SIMPLE_GETTER(m_alphaSortChildren, aAlphaSortChildren);
}
NS_IMETHODIMP WDEDomElement::SetAlphaSortChildren(PRBool aAlphaSortChildren)
{
  XPCOM_SIMPLE_SETTER(m_alphaSortChildren, aAlphaSortChildren);
}

/* void AddChild (in WDEIDomElement child); */
NS_IMETHODIMP WDEDomElement::AddChild(WDEIDomElement *childP)
{
  nsCOMPtr<WDEIDomElement> child = do_QueryInterface(childP);
  nsCOMPtr<WDEIDomElement> parent = do_QueryInterface(this);
  
  child->SetParent(parent);
  
  if(!m_alphaSortChildren)
  {
    m_children.push_back(new nsCOMPtr<WDEIDomElement>(child));
  }
  else
  {
    if(m_children.empty())
    {
      m_children.push_back(new nsCOMPtr<WDEIDomElement>(child));
    }
    else
    {
      bool inserted = false;
      string childName, tempName;
      XP_GET_STRING(child->GetName, childName);
    
      vector<nsCOMPtr<WDEIDomElement>*>::iterator pos = m_children.begin();
      XP_GET_STRING((**pos)->GetName, childName);
    
      while((childName > tempName) && (pos != m_children.end()))
      {
        pos++;
        if(pos == m_children.end())
        {
          m_children.push_back(new nsCOMPtr<WDEIDomElement>(child));
          inserted = true;
        }
      }
      if(!inserted) m_children.push_back(new nsCOMPtr<WDEIDomElement>(child));
    }
  }

  return NS_OK;
}

/* void AddChildWithPosition (in WDEIDomElement child, in PRInt32 position); */
NS_IMETHODIMP WDEDomElement::AddChildWithPosition(WDEIDomElement *childP, PRInt32 position)
{
  nsCOMPtr<WDEIDomElement> child = do_QueryInterface(childP);
  nsCOMPtr<WDEIDomElement> parent = do_QueryInterface(this);
  
  child->SetParent(parent);
  m_children.insert(m_children.begin() + position, new nsCOMPtr<WDEIDomElement>(child));
  return NS_OK;
}

/* void RemoveChild (in WDEIDomElement child); */
NS_IMETHODIMP WDEDomElement::RemoveChild(WDEIDomElement *childP)
{
  nsCOMPtr<WDEIDomElement> child = do_QueryInterface(childP);
  vector<nsCOMPtr<WDEIDomElement>*>::iterator pos;
  
  for(pos = m_children.begin(); pos != m_children.end(); pos++)
  {
    if(**pos == child)
      m_children.erase(pos);
  }
 
  return NS_OK;
}

/* WDEIDomElement GetChild (in PRInt32 idx); */
NS_IMETHODIMP WDEDomElement::GetChild(PRInt32 idx, WDEIDomElement **_retval)
{
  XPCOM_COMPTR_GETTER(*(m_children[idx]), _retval);
}

/* void Remove (); */
NS_IMETHODIMP WDEDomElement::Remove()
{
  if(m_parent) m_parent->RemoveChild(this);
  return NS_OK;
}
