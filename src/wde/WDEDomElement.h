#ifndef _WDEDOMELEMENT_H
#define _WDEDOMELEMENT_H

#include "WDEIDomElement.h"
#include <string>
#include "WDEComMacros.h"
#include "xpcomtools.h"
#include <vector>
#include "nsCOMPtr.h"

//6B90B6C9-FEFD-49FB-95F1-1DA1AC8F7F6A
#define WDEDOMELEMENT_CID {0x6B90B6C9, 0xFEFD, 0x49FB, {0x95, 0xF1, 0x1D, 0xA1, 0xAC, 0x8F, 0x7F, 0x6A}}
NS_DEFINE_CID(kWDEDomElementCID, WDEDOMELEMENT_CID);

#define WDEDOMELEMENT_CONTRACT_ID "@gnome.org/SashWDE/WDEDomElement;1"

class WDEDomElement : public WDEIDomElement
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIDOMELEMENT

  WDEDomElement();
  virtual ~WDEDomElement();
  /* additional members */
protected:
  string m_name;
  string m_description;
  vector<nsCOMPtr<WDEIDomElement>*> m_children;
  nsCOMPtr<WDEIDomElement> m_parent;
  bool m_alphaSortChildren;
  bool m_dirty;
};

#endif
