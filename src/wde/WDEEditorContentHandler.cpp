#include "WDEEditorContentHandler.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS1(WDEEditorContentHandler, WDEIEditorContentHandler)

WDEEditorContentHandler::WDEEditorContentHandler()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEEditorContentHandler::~WDEEditorContentHandler()
{
  /* destructor code */
}
