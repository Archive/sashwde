#ifndef _WDEEDITORCONTENTHANDLER_H
#define _WDEEDITORCONTENTHANDLER_H

#include "WDEIContentHandler.h"
#include "nsCOMPtr.h"
#include <string>

//29B36FAD-7D50-465B-926B-20D67C479922
#define WDEEDITORCONTENTHANDLER_CID {0x29B36FAD, 0x7D50, 0x465B, {0x92, 0x6B, 0x20, 0xD6, 0x7C, 0x47, 0x99, 0x22}}
NS_DEFINE_CID(kWDEEditorContentHandlerCID, WDEEDITORCONTENTHANDLER_CID);

#define WDEEDITORCONTENTHANDLER_CONTRACT_ID "@gnome.org/SashWDE/WDEEditorContentHandler;1"

class WDEEditorContentHandler : public WDEIEditorContentHandler
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIEDITORCONTENTHANDLER

  WDEEditorContentHandler();
  virtual ~WDEEditorContentHandler();
  /* additional members */
};

#endif
