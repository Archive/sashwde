#include "WDEEditorProperties.h"

/* Implementation file */
NS_IMPL_ISUPPORTS1(WDEEditorProperties, WDEIEditorProperties)

WDEEditorProperties::WDEEditorProperties()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEEditorProperties::~WDEEditorProperties()
{
  /* destructor code */
}

/* attribute long defaultTabWidth; */
NS_IMETHODIMP WDEEditorProperties::GetDefaultTabWidth(PRInt32 *aDefaultTabWidth)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEEditorProperties::SetDefaultTabWidth(PRInt32 aDefaultTabWidth)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute long defaultFontSize; */
NS_IMETHODIMP WDEEditorProperties::GetDefaultFontSize(PRInt32 *aDefaultFontSize)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEEditorProperties::SetDefaultFontSize(PRInt32 aDefaultFontSize)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute boolean defaultUseIndentGuides; */
NS_IMETHODIMP WDEEditorProperties::GetDefaultUseIndentGuides(PRBool *aDefaultUseIndentGuides)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEEditorProperties::SetDefaultUseIndentGuides(PRBool aDefaultUseIndentGuides)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute boolean defaultIndentWithSpaces; */
NS_IMETHODIMP WDEEditorProperties::GetDefaultIndentWithSpaces(PRBool *aDefaultIndentWithSpaces)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEEditorProperties::SetDefaultIndentWithSpaces(PRBool aDefaultIndentWithSpaces)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute boolean defaultUseSyntaxHighlighting; */
NS_IMETHODIMP WDEEditorProperties::GetDefaultUseSyntaxHighlighting(PRBool *aDefaultUseSyntaxHighlighting)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEEditorProperties::SetDefaultUseSyntaxHighlighting(PRBool aDefaultUseSyntaxHighlighting)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute boolean defaultUseAutoCompletion; */
NS_IMETHODIMP WDEEditorProperties::GetDefaultUseAutoCompletion(PRBool *aDefaultUseAutoCompletion)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEEditorProperties::SetDefaultUseAutoCompletion(PRBool aDefaultUseAutoCompletion)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute boolean defaultHighlightMatchingBraces; */
NS_IMETHODIMP WDEEditorProperties::GetDefaultHighlightMatchingBraces(PRBool *aDefaultHighlightMatchingBraces)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEEditorProperties::SetDefaultHighlightMatchingBraces(PRBool aDefaultHighlightMatchingBraces)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* attribute boolean defaultHighlightBadBraces; */
NS_IMETHODIMP WDEEditorProperties::GetDefaultHighlightBadBraces(PRBool *aDefaultHighlightBadBraces)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
NS_IMETHODIMP WDEEditorProperties::SetDefaultHighlightBadBraces(PRBool aDefaultHighlightBadBraces)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}
