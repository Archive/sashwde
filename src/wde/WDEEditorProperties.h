#ifndef _WDEEDITORPROPERTIES_H
#define _WDEEDITORPROPERTIES_H

#include "WDEIEditorProperties.h"
#include <string>
#include "WDEComMacros.h"

//996CF108-73AB-41E3-ABA4-8160485D8E40
#define WDEEDITORPROPERTIES_CID {0x996CF108, 0x73AB, 0x41E3, {0xAB, 0xA4, 0x81, 0x60, 0x48, 0x5D, 0x8E, 0x40}}
NS_DEFINE_CID(kWDEEditorPropertiesCID, WDEEDITORPROPERTIES_CID);

#define WDEEDITORPROPERTIES_CONTRACT_ID "@gnome.org/SashWDE/WDEEditorProperties;1"

class WDEEditorProperties : public WDEIEditorProperties
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIEDITORPROPERTIES

  WDEEditorProperties();
  virtual ~WDEEditorProperties();
  /* additional members */
private:
};

#endif
