#include "WDEProjectBrowserContentHandler.h"
#include "WDEApp.h"
#include "xpcomtools.h"

NS_IMPL_ISUPPORTS1(WDEProjectBrowserContentHandler, WDEIProjectBrowserContentHandler)

WDEProjectBrowserContentHandler::WDEProjectBrowserContentHandler()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEProjectBrowserContentHandler::~WDEProjectBrowserContentHandler()
{
  /* destructor code */
}
