#ifndef _WDEPROJECTBROWSERCONTENTHANDLER_H
#define _WDEPROJECTBROWSERCONTENTHANDLER_H

#include "WDEIContentHandler.h"
#include "nsCOMPtr.h"
#include <string>

//46A20E88-51B0-42A5-924D-D9B4295A51AD
#define WDEPROJECTBROWSERCONTENTHANDLER_CID {0x46A20E88, 0x51B0, 0x42A5, {0x92, 0x4D, 0xD9, 0xB4, 0x29, 0x5A, 0x51, 0xAD}}
NS_DEFINE_CID(kWDEProjectBrowserContentHandlerCID, WDEPROJECTBROWSERCONTENTHANDLER_CID);

#define WDEPROJECTBROWSERCONTENTHANDLER_CONTRACT_ID "@gnome.org/SashWDE/WDEProjectBrowserContentHandler;1"

class WDEProjectBrowserContentHandler : public WDEIProjectBrowserContentHandler
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPROJECTBROWSERCONTENTHANDLER

  WDEProjectBrowserContentHandler();
  virtual ~WDEProjectBrowserContentHandler();
  /* additional members */
};

#endif
