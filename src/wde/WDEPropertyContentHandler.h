#ifndef _WDEPROPERTYCONTENTHANDLER_H
#define _WDEPROPERTYCONTENTHANDLER_H

#include "WDEIContentHandler.h"
#include "nsCOMPtr.h"
#include <string>

//7B3593EE-8322-4CAD-AB1B-C33ADB5A32FC
#define WDEPROPERTYCONTENTHANDLER_CID {0x7B3593EE, 0x8322, 0x4CAD, {0xAB, 0x1B, 0xC3, 0x3A, 0xDB, 0x5A, 0x32, 0xFC}}
NS_DEFINE_CID(kWDEPropertyContentHandlerCID, WDEPROPERTYCONTENTHANDLER_CID);

#define WDEPROPERTYCONTENTHANDLER_CONTRACT_ID "@gnome.org/SashWDE/WDEPropertyContentHandler;1"

class WDEPropertyContentHandler : public WDEIPropertyContentHandler
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIPROPERTYCONTENTHANDLER

  WDEPropertyContentHandler();
  virtual ~WDEPropertyContentHandler();
  /* additional members */
};

#endif
