/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/


#include <glib.h>

#ifdef __cplusplus
extern "C" {
#endif
#include <uuid/uuid.h>
#ifdef __cplusplus
}
#endif

#include "WDEWDEImpl.h"
#include "WDEDomElement.h"
#include "xpcomtools.h"
#include "WDEApp.h"

NS_IMPL_ISUPPORTS1(WDEWDE, WDEIWDE)

WDEWDE::WDEWDE()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */

  m_wdeProperties = do_CreateInstance("@gnome.org/SashWDE/WDEWDEProperties;1");
  m_editorProperties = do_CreateInstance("@gnome.org/SashWDE/WDEEditorProperties;1");
  m_activeProject = NULL;
}

WDEWDE::~WDEWDE()
{
}

/* readonly attribute WDEIProject activeProject; */
NS_IMETHODIMP WDEWDE::GetActiveProject(WDEIProject * *aActiveProject)
{
  XPCOM_COMPTR_GETTER(m_activeProject, aActiveProject);
}

/* readonly attribute WDEIWDEProperties wdeProperties; */
NS_IMETHODIMP WDEWDE::GetWdeProperties(WDEIWDEProperties * *aWdeProperties)
{
  XPCOM_COMPTR_GETTER(m_wdeProperties, aWdeProperties);
}

/* readonly attribute WDEIEditorProperties editorProperties; */
NS_IMETHODIMP WDEWDE::GetEditorProperties(WDEIEditorProperties * *aEditorProperties)
{
  XPCOM_COMPTR_GETTER(m_editorProperties, aEditorProperties);
}
