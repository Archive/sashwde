/***************************************************************
    SashXB WDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2000 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Contact:
    sashmo@sash.alphaworks.ibm.com    

    IBM Advanced Internet Technology Group 
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

*****************************************************************/


#ifndef _WDEWDE_H
#define _WDEWDE_H

#include "WDEIWDE.h"
#include "nsCOMPtr.h"
#include "WDEComMacros.h"
#include "WDEIProject.h"
#include "WDEIWDEProperties.h"
#include "WDEIEditorProperties.h"

//db693205-ccc0-47f0-b772-cb064207e6ec
#define WDEWDE_CID {0xdb693205, 0xccc0, 0x47f0, {0xb7, 0x72, 0xcb, 0x06, 0x42, 0x07, 0xe6, 0xec}}
NS_DEFINE_CID(kWDEWDECID, WDEWDE_CID);

#define WDEWDE_CONTRACT_ID "@gnome.org/SashWDE/WDEWDE;1"

class WDEWDE : public WDEIWDE
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIWDE

  WDEWDE();
  virtual ~WDEWDE();
  /* additional members */
	
private:
  nsCOMPtr<WDEIProject> m_activeProject;
  nsCOMPtr<WDEIWDEProperties> m_wdeProperties;
  nsCOMPtr<WDEIEditorProperties> m_editorProperties;
};

#endif
