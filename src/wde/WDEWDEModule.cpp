#include "nsIGenericFactory.h"

#include "WDEWDEImpl.h"

#include "WDEDomElement.h"
#include "WDEWDEProperties.h"
#include "WDEEditorProperties.h"

#include "WDEContentHandler.h"
#include "WDEAddProjectItemContentHandler.h"
#include "WDEEditorContentHandler.h"
#include "WDEProjectBrowserContentHandler.h"
#include "WDEPropertyContentHandler.h"

NS_GENERIC_FACTORY_CONSTRUCTOR(WDEWDE)

NS_GENERIC_FACTORY_CONSTRUCTOR(WDEDomElement)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEWDEProperties)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEEditorProperties)

NS_GENERIC_FACTORY_CONSTRUCTOR(WDEContentHandler)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEAddProjectItemContentHandler)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEEditorContentHandler)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEProjectBrowserContentHandler)
NS_GENERIC_FACTORY_CONSTRUCTOR(WDEPropertyContentHandler)

static nsModuleComponentInfo components[] = {
  {
    "WDEWDE module",
    WDEWDE_CID,
    WDEWDE_CONTRACT_ID,
    WDEWDEConstructor,
    NULL,
    NULL
  },

  {
    "WDEDomElement module",
    WDEDOMELEMENT_CID,
    WDEDOMELEMENT_CONTRACT_ID,
    WDEDomElementConstructor,
    NULL,
    NULL
  },

  {
    "WDEWDEProperties module",
    WDEWDEPROPERTIES_CID,
    WDEWDEPROPERTIES_CONTRACT_ID,
    WDEWDEPropertiesConstructor,
    NULL,
    NULL
  },

  {
    "WDEEditorProperties module",
    WDEEDITORPROPERTIES_CID,
    WDEEDITORPROPERTIES_CONTRACT_ID,
    WDEEditorPropertiesConstructor,
    NULL,
    NULL
  },
				
  {
    "WDEContentHandler",
    WDECONTENTHANDLER_CID,
    WDECONTENTHANDLER_CONTRACT_ID,
    WDEContentHandlerConstructor,
    NULL,
    NULL
  },
  {
    "WDEAddProjectItemContentHandler",
    WDEADDPROJECTITEMCONTENTHANDLER_CID,
    WDEADDPROJECTITEMCONTENTHANDLER_CONTRACT_ID,
    WDEAddProjectItemContentHandlerConstructor,
    NULL,
    NULL
  },
  {
    "WDEEditorContentHandler",
    WDEEDITORCONTENTHANDLER_CID,
    WDEEDITORCONTENTHANDLER_CONTRACT_ID,
    WDEEditorContentHandlerConstructor,
    NULL,
    NULL
  },
  {
    "WDEPropertyContentHandler",
    WDEPROPERTYCONTENTHANDLER_CID,
    WDEPROPERTYCONTENTHANDLER_CONTRACT_ID,
    WDEPropertyContentHandlerConstructor,
    NULL,
    NULL
  },
  {
    "WDEProjectBrowserContentHandler",
    WDEPROJECTBROWSERCONTENTHANDLER_CID,
    WDEPROJECTBROWSERCONTENTHANDLER_CONTRACT_ID,
    WDEProjectBrowserContentHandlerConstructor,
    NULL,
    NULL
  }
};

NS_IMPL_NSGETMODULE("WDEWDE module", components)
