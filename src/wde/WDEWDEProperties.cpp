#include "WDEWDEProperties.h"

/* Implementation file */
NS_IMPL_ISUPPORTS1(WDEWDEProperties, WDEIWDEProperties)

WDEWDEProperties::WDEWDEProperties()
{
  NS_INIT_ISUPPORTS();
  /* member initializers and constructor code */
}

WDEWDEProperties::~WDEWDEProperties()
{
  /* destructor code */
}

/* attribute string installationDirectory; */
NS_IMETHODIMP WDEWDEProperties::GetInstallationDirectory(char * *aInstallationDirectory)
{
  XPCOM_STR_GETTER(m_installationDirectory.c_str(), aInstallationDirectory);
}
NS_IMETHODIMP WDEWDEProperties::SetInstallationDirectory(const char * aInstallationDirectory)
{
  XPCOM_SIMPLE_SETTER(m_installationDirectory, aInstallationDirectory);
}

/* attribute string shareDirectory; */
NS_IMETHODIMP WDEWDEProperties::GetShareDirectory(char * *aShareDirectory)
{
  XPCOM_STR_GETTER(m_shareDirectory.c_str(), aShareDirectory);
}
NS_IMETHODIMP WDEWDEProperties::SetShareDirectory(const char * aShareDirectory)
{
  XPCOM_SIMPLE_SETTER(m_shareDirectory, aShareDirectory);
}

/* attribute string gladeDirectory; */
NS_IMETHODIMP WDEWDEProperties::GetGladeDirectory(char * *aGladeDirectory)
{
  XPCOM_STR_GETTER(m_gladeDirectory.c_str(), aGladeDirectory);
}
NS_IMETHODIMP WDEWDEProperties::SetGladeDirectory(const char * aGladeDirectory)
{
  XPCOM_SIMPLE_SETTER(m_gladeDirectory, aGladeDirectory);
}

/* attribute string pixmapDirectory; */
NS_IMETHODIMP WDEWDEProperties::GetPixmapDirectory(char * *aPixmapDirectory)
{
  XPCOM_STR_GETTER(m_pixmapDirectory.c_str(), aPixmapDirectory);
}
NS_IMETHODIMP WDEWDEProperties::SetPixmapDirectory(const char * aPixmapDirectory)
{
  XPCOM_SIMPLE_SETTER(m_pixmapDirectory, aPixmapDirectory);
}

/* attribute string defaultAuthor; */
NS_IMETHODIMP WDEWDEProperties::GetDefaultAuthor(char * *aDefaultAuthor)
{
  XPCOM_STR_GETTER(m_defaultAuthor.c_str(), aDefaultAuthor);
}
NS_IMETHODIMP WDEWDEProperties::SetDefaultAuthor(const char * aDefaultAuthor)
{
  XPCOM_SIMPLE_SETTER(m_defaultAuthor, aDefaultAuthor);
}
