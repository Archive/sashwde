#ifndef _WDEWDEPROPERTIES_H
#define _WDEWDEPROPERTIES_H

#include "WDEIWDEProperties.h"
#include "WDEComMacros.h"
#include <string>
#include "xpcomtools.h"

//C8265AC8-56A3-43CA-99D5-15B2D0EEDD9E
#define WDEWDEPROPERTIES_CID {0xC8265AC8, 0x56A3, 0x43CA, {0x99, 0xD5, 0x15, 0xB2, 0xD0, 0xEE, 0xDD, 0x9E}}
NS_DEFINE_CID(kWDEWDEPropertiesCID, WDEWDEPROPERTIES_CID);

#define WDEWDEPROPERTIES_CONTRACT_ID "@gnome.org/SashWDE/WDEWDEProperties;1"

class WDEWDEProperties : public WDEIWDEProperties
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_WDEIWDEPROPERTIES

  WDEWDEProperties();
  virtual ~WDEWDEProperties();
  /* additional members */
private:
  string m_installationDirectory;
  string m_shareDirectory;
  string m_gladeDirectory;
  string m_pixmapDirectory;
  
  string m_defaultAuthor;
};

#endif
