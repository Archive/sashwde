
/***************************************************************
    SashWDE for Linux
    The SashXB Weblication Development Environment for Linux

    Copyright (C) 2001 IBM Corporation

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA

    Contact:
    sashmo@sash.alphaworks.ibm.com

    IBM Advanced Internet Technology Group
    c/o Lotus Development Corporation
    One Rogers Street
    Cambridge, MA 02142
    USA

    *****************************************************************

Contributor(s):

*****************************************************************/

/*! \mainpage Sash Weblication Development Environment
 *
 * \section intro Introduction
 * These are the developer docs for the WDE. They are meant to give a quick intro to the WDEm and give
 * potential developers an idea of how the program works internally, and also to serve as a quick reference
 * for current developers. They are not meant as a WDE User's Guide. It assumes you have at least a general
 * understanding of how Sash/SashXB works (ie. Terminology like: Sash, Weblications, Locations, Actions,
 * Extensions, wdf should all be familiar, you should know how XPCOM, javascript, etc all work and fit
 * into SashXB at at least a basic level)
 *
 * The most basic purpose of the WDE is to allow people to write weblications. Most importantly, this involves
 * creating a directory which contains a published weblication - ie the wdf files, and necesary data files like
 * javascript files, html files, glade files, etc. To accomplish this, we need to be able to create the XML wdf
 * file from our internal representation of the weblication, and also move around files and save changes to them.
 *
 * The WDE has most of the basic functions you might expect from a text editor/IDE. Users work with
 * "projects"  as in many other IDE's - a project is basically one weblication (although you can (or will be able to)
 * use the WDE to create extensions and locations as well). There are interfaces to create actions, create or add files,
 * etc. There is a project browser, which provides a view of the project and also is a place where you can
 * change many parts of the project (thru the meta data section at the bottom of the project browser.) There is also
 * text editing, syntax highlighting, syntax autocompletion, preferences, etc.
 *
 * The WDE has a fairly strict separation of it's GUI elements and the underlying data models. The underlying data models
 * all are XPCOM components, therefore accessible to javascript through Sash itself. The GUI elements are not XPCOM, although
 * eventually may become COM... this would allow the GUI to also be accessible and extensible thru javascript.
 * The GUI responds to events which are emitted by the data models, and update themselves appropriately.
 * The GUI itself, when reponding to button clicks and otehr user commands, directly calls the methods of the data models.
 * This may seem standard, but is a large shift from the way things were done in the previous version of the WDE, so
 * it seemed worth mentioning.
 *
 * In addition to the GUI classes and the data model COM classes, there are some other assorted classes and files. These
 * include some tools, a static class or two, etc.
 *
 *
 * \section datamodels Data Models
 * The principal data model classes are WDEIWDE, WDEIProject, WDEITreeItem and WDEIEditorDM.
 *
 * WDEIWDE contains all of the information relavent to the WDE itself, including things like preferences,
 * documentation, pointers to open files and projects, etc. It also provides a registry where info can be
 * stored to disk, methods to create and close projects, etc.
 *
 * WDEIProject encapsulates the info needed for a project. The project is basically a tree, with branches
 * for things like actions, glade forms, resource files, etc. The top level tree item is the "files" attribute,
 * but we also provide access to other branches of the tree directly (instead of having to traverse the tree down
 * from "files") for ease of use. The project also contains attibutes for other parts of the project like
 * it's name, author, etc. It takes care of serializing the data from our representation in memory to a .wpf file
 * (weblication project file - for when we want to save/load a project) and a.wdf file (for when we want to save/load
 * a project or publish or install it.)
 *
 * WDEITreeItem is a general purpose tree node which we find useful in many places
 *
 * WDEEditorDM encapusulates anything which we could edit in the WDE. Most of the time this is a text editor (hence the
 * more useful WDEITextEditorDM subclass), but we generalized it in order to allow for future possibilities like embedded
 * Glade editors, image editors, etc.
 *
 * An important note on the data models is that the methods do *exactly* what they say. For instance, calling close on a
 * WDEEditorDM which has unsaved changes will close it without any warning. We leave responsibility for checking
 * GetDirty() up to the GUI. To create many new data models from disk (ie project, EditorDM) you need to create a
 * new one, set the path and filename, and then Load().
 *
 * The data models should never assume anything about the GUI or call any methods from GUI classes (occasionally a function in GUITools.h
 * needs to be used, which is fine.) They also never listen for events, but instead only broadcast them (There is one exception to this rule -
 * the WDEProject Data model listens to its files tree, so that it can update its dirty flag. This may not be the ideal way to do this,
 * but no better way presents itself...)
 *
 * The data models are a
 *
 * \section gui GUI
 * We made most of the GUI interfaces in glade, and the generally work as follows. They directly call the methods of the data model classes
 * when responding to button clicks, menu selects, etc. For instance, when a user clicks on "New File", the GUI code calls "add new file" on our
 * files tree, sets it's name to something appropriate (ie "untitled 1), and tells the WDE to make it the new active file. However, the GUI
 * elements should never update themselves in response to a button click - instead they wait for events to be broadcast from the data models. Upon
 * receiving an event, the GUI models check it's ID, and take the appropriate action.
 *
 * GUI elements should never broadcast events, only listen for them.
 *
 *
 * \section other Other Code
 * We implemented our own event system for the WDE. We created an xpcom interface, WDEIEventable, which objects implement in order to send and recieve
 * events. This is the only one they need to use - we didn't draw a distinction between objects which listen for events, and objects which
 * broadcast them. (This might have been a better way to do it, this was partially done b/c of older code and the fact we were rushed.) The events
 * themselves are WDEIEvent - they essentially are an integer ID, and some extra data whcih can be sent with this ID. We also made macros and
 * helper classes to allow non-xpcom obejects to implement this WDEIEventable xpcom interface. This was done because we didn't want to turn
 * all of our GUI code into xpcom objects, but they needed to handle events. You can checkout WDEIEvent for more information on
 * specific events.
 *
 * WDEApp is essentially a static class which provides access to the WDE data model, as well as several helper macros and functions.
 * GUITools.h provides many helper functions which are used throughout the program.
 *
 * section naming Naming Conventions, etc...
 * All of our xpcom interfaces generally begin with "WDEI...". The implementations of these interfaces are generally the same name, except we
 * remove the "I" after "WDE". Occasionally we instead append the word "Impl" to an implementation of an xpcom interface. ALl of our idl files are
 * stored in the idl directory.
 *
 * Gtk widgets require extern c callbacks, and when using glade to create our user interfaces, we generally follow the convention of prepending
 *
 * We have several files in the GUI directory which wich use solely to hold the extern callback functions for gtk widgets created in glade, these
 * are named GUIxxxCallbacks
 *
 * Most files begin with "WDE".
 *
 * \section maintodo Features, problems, etc
 * \todo Cancel buttons in all GUI dialogs...
 * \todo The wde crashes when someone tries to open an invalid project file
 * \todo Importing multiple files at once.
 * \todo Checking for whether open files have been updated on disk
 * \todo Importing wdf
 * \todo we don't always seem to call gtk_main_quit enough...
 * \todo Devkits need help
 * \todo Check for existing file of same name and prompt to overwrite when we save a new file or save file as...
 */



/**
 * @class WDEIEvent
 * The interface for an event object - our implementation is WDEEvent.
 * Events are (or should be, in a few cases...) thrown in the SashWDE whenever the data of the underlying
 * data model changes. They should never be thrown by GUI elements of the application. Each event has an ID,
 * which identifies what change the event represents. Events have a pointer to the data model which
 * threw them (also called the "caller" or "thrower". We also allow events to have 3 arbitrary pieces of data -
 * a string, an int, and a pointer to an arbitrary XPCOM object. The meaning of these pieces of data will depend
 * on what event it is (ie the ID) - see each individual ID for a summary of this information.
 * In general, event should always be thrown *after* the actual data hs changed.
 */

/**
 * @class WDEIEventable
 * We implemented our own event system for the WDE. We created an xpcom interface, WDEIEventable, which objects implement in order to send and recieve
 * events. This is the only one they need to use - we didn't draw a distinction between objects which listen for events, and objects which
 * broadcast them. (This might have been a better way to do it, this was partially done b/c of older code and the fact we were rushed.) The events
 * themselves are WDEIEvent - they essentially are an integer ID, and some extra data whcih can be sent with this ID. We also made macros and
 * helper classes to allow non-xpcom obejects to implement this WDEIEventable xpcom interface. This was done because we didn't want to turn
 * all of our GUI code into xpcom objects, but they needed to handle events.
 */

/**
 * @class WDEIEditorDM
 * @brief This interface defines the basic qualities of an object which can be edited by the WDE.
 * This is an abstract class - in reality, we are always actually using WDEITextEditorDM - however
 * this class was defined to allow future expansion for things like embedded glade or image file editing, etc...
 */

/**
 * @class WDEITextEditorDM
 * This interface for a text editor in the WDE. Portions of this class are not implemented and return NS_ERROR_NOT_IMPLEMENTED...
 */
